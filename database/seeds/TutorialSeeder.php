<?php

use Illuminate\Database\Seeder;

class TutorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Tutorial\Models\Tutorial::truncate();
        factory(\Tutorial\Models\Tutorial::class, 9)->create();
    }
}
