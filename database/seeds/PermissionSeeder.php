<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \ACL\Models\Permission::truncate();
        factory(\ACL\Models\Permission::class, 10)->create();
    }
}
