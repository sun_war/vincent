<?php

use Illuminate\Database\Seeder;

class LessonTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Tutorial\Models\LessonTest::truncate();
        factory(\Tutorial\Models\LessonTest::class, 9999)->create();
    }
}
