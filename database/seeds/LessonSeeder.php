<?php

use Illuminate\Database\Seeder;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Tutorial\Models\Lesson::truncate();
        factory(\Tutorial\Models\Lesson::class, 999)->create();
    }
}
