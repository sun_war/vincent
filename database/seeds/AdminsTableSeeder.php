<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->truncate();
        DB::table('admins')->insert([
            [
                'id' => 1,
                'name' => 'Vincent Admin',
                'email' => 'vincent@admin.vn',
                'password' => bcrypt('PMC1704bn'),
                'is_active' => 1
            ]
        ]);
    }
}
