<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \ACL\Models\Role::truncate();
        factory(\ACL\Models\Role::class, 3)->create();
    }
}
