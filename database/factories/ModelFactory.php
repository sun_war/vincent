<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('12345aA@'),
        'remember_token' => str_random(10),
        'group_id' => rand(1, 9)
    ];
});

$factory->define(\Tutorial\Models\Tutorial::class, function (Faker\Generator $faker) {
    return [
        NAME_COL => $faker->name,       
        IS_ACTIVE_COL => 1,
    ];
});

$factory->define(\Tutorial\Models\Section::class, function (Faker\Generator $faker) {
    $tutorialIds = \Tutorial\Models\Tutorial::pluck(ID_COL)->toArray();
    return [
        NAME_COL => $faker->name,
        TUTORIAL_ID_COL => $faker->randomElement($tutorialIds),
        IS_ACTIVE_COL => 1,
    ];
});

$factory->define(\Tutorial\Models\Lesson::class, function (Faker\Generator $faker) {
    $sectionIds = \Tutorial\Models\Section::pluck(ID_COL)->toArray();
    $usersIds = \App\Models\User::pluck(ID_COL)->toArray();
    return [
        'title' => $faker->name,
        INTRO_COL => $faker->name,
        CONTENT_COL => $faker->name,
        SECTION_ID_COL => $faker->randomElement($sectionIds),
        CREATED_BY_COL => $faker->randomElement($usersIds),
        UPDATED_BY_COL => $faker->randomElement($usersIds),
        IS_ACTIVE_COL => 1,
    ];
});

$factory->define(\Tutorial\Models\LessonComment::class, function (Faker\Generator $faker) {
    $lesson_ids = \Tutorial\Models\Lesson::pluck(ID_COL)->toArray();
    $usersIds = \App\Models\User::pluck(ID_COL)->toArray();
    return [
        LESSON_ID_COL => $faker->randomElement($lesson_ids),
        CONTENT_COL => $faker->text,
        CREATED_BY_COL => $faker->randomElement($usersIds),
        IS_ACTIVE_COL => 1,
    ];
});

$factory->define(\Tutorial\Models\LessonTest::class, function(Faker\Generator $faker) {
    $lesson_ids = \Tutorial\Models\Lesson::pluck(ID_COL)->toArray();
    $usersIds = \App\Models\User::pluck(ID_COL)->toArray();
    return [
        LESSON_ID_COL => $faker->randomElement($lesson_ids),
        QUESTION_COL => $faker->text,
        REPLY1_COL => $faker->name,
        REPLY2_COL => $faker->name,
        REPLY3_COL => $faker->name,
        REPLY4_COL => $faker->name,
        ANSWER_COL => rand(1, 4),
        CREATED_BY_COL => $faker->randomElement($usersIds),
        UPDATED_BY_COL => $faker->randomElement($usersIds),
        IS_ACTIVE_COL => 1,
    ];
});

