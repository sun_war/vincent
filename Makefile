ifndef u
u:=sotatek
endif

ifndef env
env:=dev
endif

OS:=$(shell uname)

docker-start:
	cp laravel-echo-server.json.example laravel-echo-server.json
	@if [ $(OS) = "Linux" ]; then\
		sed -i -e "s/localhost:8000/web:8000/g" laravel-echo-server.json; \
		sed -i -e "s/\"redis\": {}/\"redis\": {\"host\": \"redis\"}/g" laravel-echo-server.json; \
	else\
		sed -i '' -e "s/localhost:8000/web:8000/g" laravel-echo-server.json; \
		sed -i '' -e "s/\"redis\": {}/\"redis\": {\"host\": \"redis\"}/g" laravel-echo-server.json; \
	fi
	docker-compose up -d

docker-restart:
	docker-compose down
	make docker-start
	make docker-init-db-full
	make docker-link-storage

docker-connect: 
	docker exec -it vincent bash

init-app:
	cp .env.example .env
	composer install
	php artisan key:generate
	php artisan passport:keys
	php artisan migrate
	php artisan db:seed
	php artisan storage:link

docker-init:
	git submodule update --init
	docker exec -it mango make init-app
	rm -rf node_modules
	npm install

init-db-full:
	make autoload
	php artisan migrate:fresh
	make update-master
	php artisan db:seed

docker-init-db-full:
	docker exec -it mango make init-db-full

docker-link-storage:
	docker exec -it mango php artisan storage:link

init-db:
	make autoload
	php artisan migrate:fresh

start:
	php artisan serve

log:
	tail -f storage/logs/laravel.log

test-js:
	npm test

test-php:
	vendor/bin/phpcs --standard=phpcs.xml && vendor/bin/phpmd app text phpmd.xml

build:
	npm run dev

watch:
	npm run watch

docker-watch:
	docker exec -it mango make watch

autoload:
	composer dump-autoload

cache:
	php artisan cache:clear && php artisan view:clear

docker-cache:
	docker exec mango make cache

route:
	php artisan route:list

generate-master:
	php bin/generate_master.php $(lang)

update-master:
	php artisan master:update $(lang)
	make cache

deploy:
	ssh $(u)@$(h) "mkdir -p $(dir)"
	rsync -avhzL -O --delete \
				--no-perms --no-owner --no-group \
				--exclude .git \
				--exclude .idea \
				--exclude .env \
				--exclude laravel-echo-server.json \
				--exclude storage/oauth-private.key \
				--exclude storage/oauth-public.key \
				--exclude node_modules \
				--exclude /vendor \
				--exclude bootstrap/cache \
				--exclude storage/logs \
				--exclude storage/framework \
				--exclude storage/app \
				--exclude public/storage \
				. $(u)@$(h):$(dir)/


init-db-dev:
	ssh $(u)@192.168.1.20$(n) "cd /var/www/mango/ && make init-db-full"

deploy-dev:
	make deploy h=192.168.1.20$(n) dir=/var/www/mango
	ssh $(u)@192.168.1.20$(n) "cd /var/www/mango && composer dumpautoload && php artisan migrate && make cache "


compile:
	ssh $(u)@$(h) "cd $(dir) && npm install && composer install && make cache && make autoload && npm run production"

deploy-dev-full:
	make deploy h=192.168.1.20$(n) dir=/var/www/mango
	make compile h=192.168.1.20$(n) dir=/var/www/mango



