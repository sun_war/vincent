(function ($) {
    $.fn.checkAll = function (item) {
        let self = this;
        let id = '#' + self.attr('id');
        $(document).on('change', id, function () {
            if ($(id).is(":checked")) {
                $(item).each(function () {
                    $(this).prop('checked', true);
                })
            } else {
                $(item).each(function () {
                    $(this).prop('checked', false);
                })
            }
        });
        return this;
    };

    $.fn.magicFormer = function (config) {
        let inputIn = config.inputIn;
        let selectIn = config.selectIn;
        let btnIn = config.btnIn;
        let tableIn = config.tableIn;
        let remember = false;
        let review = true;

        if (config.check !== undefined) {
            remember = config.check.remember;
            let allCheck = config.check.allCheck;
            let itemCheck = config.check.itemCheck;
            review = config.check.review;
            let itemSelected = config.check.itemSelected;
            let valueSelected = config.check.valueSelected;

            function backStatus(items) {
                if (remember) {
                    $(itemCheck).each(function () {
                        let self = $(this);
                        let val = self.val();
                        if (Object.keys(items).indexOf(val) > -1) {
                            self.prop('checked', true);
                        } else {
                            self.prop('checked', false);
                        }
                    });
                }
            }

            function toggleCheck(self) {
                if (self.is(":checked")) {
                    $(itemCheck).prop('checked', true);
                } else {
                    $(itemCheck).prop('checked', false);
                }
            }

            $(document).on('change', itemCheck, function () {
                let self = $(this);
                saveStatus(self);
                reviewing();
            });

            $(document).on('change', allCheck, function () {
                let self = $(this);
                toggleCheck(self);
                $(itemCheck).each(function () {
                    let selfItem = $(this);
                    saveStatus(selfItem);
                });
                reviewing();
            });

            function reviewing() {
                if (review) {
                    let temp = '';
                    Object.keys(items).forEach(function (key) {
                        temp += '<tr>' +
                            '<input checked type="hidden" name="test_case_ids[]" value="' + key + '">' +
                            '<td>' + items[key] + '</td>' +
                            '<td class="text-center">' +
                            '<button value="' + key + '" type="button" class="btn btn-xs btn-danger ' + valueSelected + '">' +
                            '<i class="fa fa-remove"></i>' +
                            '</button>' +
                            '</td>' +
                            '</td>' +
                            '</tr>';
                    });
                    itemSelected.html(temp);
                }
            }

            $(document).on('click', '.' + valueSelected, function () {
                let self = $(this);
                let value = self.val();
                self.parents('tr').remove();
                delete items[value];
                backStatus(items);
            });

            function saveStatus(self) {
                if (remember) {
                    let val = self.val();
                    let check = self.is(':checked');
                    if (check) {
                        items[val] = self.attr('nick');
                    } else {
                        delete items[val];
                    }
                }
            }
        }
        if (config.myItem !== undefined) {
            let tableItem = config.myItem.table;
            $(document).on('click', '#' + tableItem.attr('id') + ' li a', function (e) {
                e.preventDefault();
                let url = $(this).attr('href');
                let data = {};
                paginate(url, data, tableItem);
            });
        }
        let selfForm = this;
        let items = {};

        function filterAjax(url, data) {
            $.ajax({
                url: url,
                method: 'GET',
                data: data,
                beforeSend: function () {
                    //table.html('Loading ...')
                },
                success: function (data) {
                    tableIn.html(data);
                    $('[data-toggle="popover"]').popover();
                    $('.btnPopover').hover(function () {
                        $(this).popover('show');
                    });
                    backStatus(items)
                },
                error: function () {
                    tableIn.html('Error..')
                }
            })
        }

        function paginate(url, data, table) {
            $.ajax({
                url: url,
                method: 'GET',
                data: data,
                beforeSend: function () {
                    //table.html('Loading ...')
                },
                success: function (data) {
                    table.html(data);
                    //backStatus(items)
                },
                error: function () {
                    table.html('Error..')
                }
            })
        }

        if (inputIn) {
            inputIn.on("keydown", function (event) {
                if (event.which === 13) {
                    event.preventDefault();
                    let data = selfForm.serialize();
                    let url = selfForm.attr('action');
                    filterAjax(url, data);
                }
            });
        }
        if (selectIn) {
            selectIn.change(function () {
                let data = selfForm.serialize();
                let url = selfForm.attr('action');
                filterAjax(url, data);
            });
        }
        if (btnIn) {
            btnIn.click(function () {
                let data = selfForm.serialize();
                let url = selfForm.attr('action');
                filterAjax(url, data);
            });
        }
        /**
         * Paginate
         */
        $(document).on('click', '#' + tableIn.attr('id') + ' li a', function (e) {
            e.preventDefault();
            let url = $(this).attr('href');
            let data = selfForm.serialize();
            filterAjax(url, data);
        });

        return this;
    };

    let confirmSure = $('.confirmSure');
    let confirmYes = $('#confirmYes');
    let formDestroy;
    confirmSure.click(function () {
        formDestroy = $(this).parent('form');
    });
    confirmYes.click(function () {
        formDestroy.submit();
    });

    $.fn.magicFormatNumber = function (config) {
        let magicNumber = this;
        magicNumber.keyup(function (event) {
            let selection = window.getSelection().toString();
            if (selection !== '') {
                return;
            }
            if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
                return;
            }
            let $this = $(this);
            let input = $this.val();
            input = input.replace(/[\D\s\._\-]+/g, "");
            input = input ? parseInt(input, 10) : 0;
            $(config).text(function () {
                return (input === 0) ? "" : input.toLocaleString("en-US");
            });
        });
        return this;
    }
}($));

const format = {
    number: (input , config) => {
        input += '';
        input = input.replace(/[\D\s\._\-]+/g, "");
        input = input ? parseInt(input, 10) : 0;
        $(config).text(function () {
            return (input === 0) ? "" : input.toLocaleString("en-US");
        });
    }
};

function getChecked(input) {
    return input.map(function () {
        return $(this).val();
    }).get();
}


/*
 let formFilter = $('#formFilter');
 let inputFilter = $('.inputFilter');
 let btnFilter = $('#btnFilter');
 let table = $('#table');
 let selectFilter = $('.selectFilter');
 let itemSelected = $('#itemSelected');
 let checkAllCheck = '.check_all';
 let checkItemCheck = '.check_item';
 let exportData = $('#exportData');

 let config = {
 inputIn: inputFilter,
 selectIn: selectFilter,
 btnIn: btnFilter,
 tableIn: table,
 check: {
 remember: false,
 allCheck: checkAllCheck,
 itemCheck: checkItemCheck,
 itemSelected: itemSelected,
 review: false
 }
 };
 formFilter.magicFormer(config);
 */



