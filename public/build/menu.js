const menu = {
    ACLMenu : [ 'permissionMenu', 'roleMenu', 'userMenu' ],
    componentMenu: [ 'slideMenu', 'quotationMenu', 'aboutMenu', 'contactMenu'],
    ECommerceMenu: [ 'categoryMenu', 'branchMenu', 'typeMenu', 'productMenu', 'orderMenu', 'orderDetailMenu'],
    englishMenu: ['pronunciationMenu', 'mistakeMenu', 'fillInTheBlankMenu', 'similarityMenu', 'vocabularyMenu'],
    loggerMenu: [ 'coinLogMenu' ],
    mediaMenu: [ 'imageMenu', 'albumMenu'],
    modularizationMenu: ['crudMenu'],
    testMenu:[ 'multiChoiceMenu' , 'testResultMenu', 'questionResultMenu'],
    eLearnMenu: [ 'tutorialMenu', 'tutorialTestMenu', 'tutorialResultMenu', 'sectionMenu', 'sectionTestMenu',
        'sectionResultMenu', 'lessonMenu', 'lessonTestMenu', 'lessonTestMenu', 'lessonResultMenu', 'lessonCommentMenu',
    'lessonFeedBackMenu']
};

function Menu(parent, child) {
    $(parent).addClass('active').addClass('opened');
    $(child).addClass('active');
}