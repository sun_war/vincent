/**
 * Created by vincent on 5/18/17.
 */
let formFilter = $('#formFilter');
let inputFilter = $('.inputFilter');
let table = $('#table');
let selectFilter = $('.selectFilter');

$(document).on('change', '.check_all', function () {
    if ($(this).is(":checked")) {
        $('.check_item').prop('checked', true);
    } else {
        $('.check_item').prop('checked', false);
    }
});

(function ($) {
    $.fn.checkAll = function (item) {
        let self = this;
        $(document).on('change', this, function () {
            if (self.is(":checked")) {
                $(item).prop('checked', true);
            } else {
                $(item).prop('checked', false);
            }
        });
        return this;
    };

    function filterAjax(url, data, table) {
        $.ajax({
            url: url,
            method: 'GET',
            data: data,
            beforeSend: function () {
                //table.html('Loading ...')
            },
            success: function (data) {
                table.html(data);
            },
            error: function () {
                table.html('Error..')
            }
        })
    }

    $.fn.formFilter = function (inputFilter, table) {
        let self = this;
        inputFilter.on( "keydown", function( event ) {
            if (event.which === 13) {
                event.preventDefault();
                console.log('filter begin');
                let data = self.serialize();
                let url = self.attr('action');
                filterAjax(url, data, table);
            }
            // else {
            //     inputFilter.change(function () {
            //         console.log('filter begin');
            //         let data = self.serialize();
            //         let url = self.attr('action');
            //         filterAjax(url, data, table);
            //     });
            // }
        });
        selectFilter.change(function () {
            console.log('filter begin');
            let data = self.serialize();
            let url = self.attr('action');
            filterAjax(url, data, table);
        });
        return this;
    };

    $(document).on('click', '#linkPaginate li a', function (e) {
        e.preventDefault();
        let url = $(this).attr('href');
        let data = formFilter.serialize();
        filterAjax(url, data, table);
    });

    $(document).on('click', '#searchVocabularyTable li a', function (e) {
        e.preventDefault();
        let url = $(this).attr('href');
        let data = $('#formSearchVocabulary').serialize();
        filterAjax(url, data, $('#searchVocabularyTable'));
    });

    let subject_id = $('#subject_id');

    $(document).on('change', '#importFile', function () {
        let files = event.target.files;
        event.stopPropagation(); // Stop stuff happening
        event.preventDefault(); // Totally stop stuff happening
        let data = new FormData();
        $.each(files, function (key, value) {
            data.append(key, value);
        });
        data.append('subject_id', subject_id.val());
        let url = $(this).attr('route');
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data, textStatus, jqXHR) {
                alert('Import success');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('ERRORS: ' + textStatus);
            }
        });
        $(this).prop('disable', true);
    });

    let confirmSure = $('.confirmSure');
    let confirmYes = $('#confirmYes');
    let formDestroy;

    confirmSure.click(function () {
        formDestroy = $(this).parent('form');
        console.log(formDestroy);
    });

    confirmYes.click(function () {
        formDestroy.submit();
    });

}($));

formFilter.formFilter(inputFilter, table);

let exportData = $('#exportData');
exportData.click(function () {
    let route = $(this).attr('route');
    let action = formFilter.attr('action');
    formFilter.attr('action', route);
    formFilter.submit();
    formFilter.attr('action', action);
});
