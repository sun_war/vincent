$(function () {
    var statDate = $('#start_date');
    var endDate = $('#end_date');
    statDate.datetimepicker({
        format: 'YYYY-MM-DD'
    });
    endDate.datetimepicker({
        format: 'YYYY-MM-DD'
    });
    checkDateFirst();
    function checkDateFirst() {
        var varStart = statDate.val();
        var varEnd = statDate.val();
        if(varStart > varEnd)
        {
            statDate.val('');
            varEnd.val('');
            //alert('Start date must smaller than end date');
        }
    }
});
$(".assignedSelect").select2();
/**
 * ManDay Process
 * @type {jQuery|HTMLElement}
 */
var addAssignedBtn = $('#addAssignedBtn');
var addAssignedInput = $('#addAssignedInput');
var removeAssignBtn = $('.removeAssignBtn');
var createProjectManDayRoute = $('#createProjectManDayRoute');
var projectIdInput = $('#projectIdInput');
var projectId = projectIdInput.val();
var url = createProjectManDayRoute.val();
addAssignedBtn.click(function () {
    var addAssignedSelect = $('#addAssignedSelect option:selected');
    var day = addAssignedInput.val();
    if(day > 1000 || day <=0)
    {
        alert('Max manday must > 0 and <1000');
        addAssignedInput.val('0');
    } else {
        var valueAssign = addAssignedSelect.val();
        var textAssign = addAssignedSelect.text();
        var data = {project_id: projectId, user_id: valueAssign, day: day};
        var self = $(this);
        $.ajax({
            url: url,
            method: 'POST',
            data: data,
            success: function (data) {
                console.log(data);
                if (data) {
                    var assignedTd = '<tr>\n' +
                        '                        <td>\n' +
                        '                            <select class="form-control input-sm" name="assigneds[]" id="" readonly="">\n' +
                        '                                <option value="' + valueAssign + '">' + textAssign + '</option>\n' +
                        '                            </select>\n' +
                        '                        </td>\n' +
                        '                        <td>\n' +
                        '                            <div class="input-group">\n' +
                        '                                <input type="number" required name="days[]" value="' + day + '" class="form-control input-sm" >\n' +
                        '                                <span class="input-group-btn">\n' +
                        '                                    <button type="button" data="' + data.id + '" class="btn btn-sm btn-danger pull-right removeAssignBtn"><i class="fa fa-remove"></i></button>\n' +
                        '                                </span>\n' +
                        '                            </div><!-- /input-group -->\n' +
                        '\n' +
                        '                        </td>\n' +
                        '                    </tr>';
                    self.parents('tr').after(assignedTd);
                    addAssignedInput.val('');
                } else {
                    alert('Create fail, please try again');
                }
            },
            error: function () {
                alert('Error');
            }
        });
    }
});

$(document).on('click', '.removeAssignBtn', function () {
    var self = $(this);
    var ok = confirm('Are you sure ?');
    if (ok) {
        var id = self.attr('data');
        $.ajax({
            url: url + '/' + id,
            method: 'DELETE',
            success: function (data) {
                self.parents('tr').remove();
            },
            error: function () {
                alert('Error');
            }
        });
    }
});

/**
 *Statistic Severity
 * @type {jQuery|HTMLElement}
 */

var changePlatFormRoute = $('#changePlatFormRoute');
var platformSelect = $('#platform_id');
var statisticCriteriaContent = $('#statisticCriteriaContent');
platformSelect.change(function () {
    var url = changePlatFormRoute.val();
    var platform_id = $(this).val();
    $.ajax({
        url: url,
        method: 'GET',
        data: {project_id: projectId, platform_id: platform_id},
        success: function (data) {
            console.log(data);
            statisticCriteriaContent.html(data);
        },
        error: function () {
            alert('Error');
        }
    });
});

var updateManDay = $('.updateManDayBtn');
var changeManDayRoute = $('#changeManDayRoute');
updateManDay.click(function () {
    var self = $(this);
    var day = self.parents('div').children('input').val();
    if(day < 0 && day > 1000)
    {
        alert('Day must > 0 and < 1000');
    }
    var url = changeManDayRoute.val();
    var tester_id = self.attr('tester_id');
    var project_id = self.attr('project_id');
    var id = self.attr('mam_day_id');
    var data = {};
    if(id) {
        data = {user_id: tester_id, day: day, id: id, project_id: project_id};
    } else {
        data = {user_id: tester_id, day: day, project_id: project_id};
    }
    // console.log(tester_id);
    // console.log(id);
    console.log(day);
    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: function (data) {
           alert('Update Success');
        },
        error: function () {
            alert('ERROR');
        }
    })
});