var pushNotifyProject = $('.pushNotifyProject');
var projectIdNotifyInput = $('#projectIdNotify');
var searchNotifyUserBtn = $('#searchNotifyUser');
var url = formFilter.attr('action');

function filterNotify() {
    $.ajax({
        url: url,
        data: formFilter.serialize(),
        method: 'GET',
        beforeSend: function () {
            //table.html('Loading ...')
        },
        success: function (data) {
            //console.log(data);
            table.html(data);
        },
        error: function () {
            table.html('Error..')
        }
    });
}

pushNotifyProject.click(function () {
    var self = $(this);
    var id = self.attr('data');
    projectIdNotifyInput.val(id);
    filterNotify();
});

searchNotifyUserBtn.click(function () {
    filterNotify();
});


$(document).on('change', '.check_all', function () {
    var self = $(this);
    if (self.is(":checked")) {
        $('.check_item').prop('checked', true);
    } else {
        $('check_item').prop('checked', false);
    }
});
