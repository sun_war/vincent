/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./modules/ACL/resources/assets/js/app.js":
/*!************************************************!*\
  !*** ./modules/ACL/resources/assets/js/app.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./profile */ "./modules/ACL/resources/assets/js/profile.js");

/***/ }),

/***/ "./modules/ACL/resources/assets/js/profile.js":
/*!****************************************************!*\
  !*** ./modules/ACL/resources/assets/js/profile.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$("#birthday").datetimepicker({
  format: 'YYYY-MM-DD'
});
var rateUserBtn = '.rateUserBtn';
var rateHidden = '#rateHidden';
$(document).on('click', rateUserBtn, function () {
  var self = $(this);
  var start = self.attr('data-start');
  $(rateHidden).val(start);
});
$('#coin').magicFormatNumber('#number');

function errorMessage(errors) {
  var errorMessages = '';

  for (var errorName in errors) {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = errors[errorName][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var error = _step.value;
        errorMessages += error + "<br/>";
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return != null) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  }

  return errorMessages;
}

var backupBtn = '#backupBtn';
var privateKeyRoute = '#privateKeyRoute';
$(backupBtn).click(function () {
  var url = $(privateKeyRoute).val();
  $.ajax({
    url: url,
    method: 'GET',
    success: function success(privateKey) {
      var mnemonic = decryptMnemonic(privateKey);
      $('#mnemonicTxt').val(mnemonic);
      $('#qrCoeRecovery').html('').qrcode({
        size: '200',
        render: 'image',
        text: mnemonic
      });
    }
  });
});
var Enable2stepBtn = '#Enable2stepBtn';
var TwoStepTxt = '#TwoStepTxt';
var TowStepQrImage = '#TowStepQrImage';
var createOtpRoute = '#createOtpRoute';
$(Enable2stepBtn).click(function () {
  var url = $(createOtpRoute).val();
  $.ajax({
    url: url,
    method: 'POST',
    success: function success(data) {
      console.log(data);
      $(TwoStepTxt).val(data.key);
      $(TowStepQrImage).html("<img src=\"".concat(data.url, "\" style=\"margin: auto\" alt=\"code qr\" id=\"TowStepQrImage\" class=\"img-responsive\">"));
    },
    error: function error(_error) {
      console.log(_error);
      toastr.error(_error.responseJSON.message);
    }
  });
});
var twoStep = '#twoStep';
var twoStepBtn = '#twoStepBtn';
var twoStepEnable = '#twoStepEnable';
var twoStepEnableForm = '#twoStepEnableForm';
var twoStepDisableForm = '#twoStepDisableForm';
$(twoStepEnableForm).submit(function (e) {
  e.preventDefault();

  if ($(twoStepEnableForm).valid()) {
    var self = $(this);
    var data = self.serialize();
    var url = self.attr('action');
    $.ajax({
      url: url,
      method: 'POST',
      data: data,
      success: function success(data) {
        toastr.success('Enable 2-step success', '', 10000);
        $(twoStepEnableForm).modal('show');
        location.reload();
      },
      error: function error(_error2) {
        var errors = _error2.responseJSON.errors;
        var errorMessages = errorMessage(errors);
        toastr.error(errorMessages, 'Errors', 10000);
      }
    });
  }
});
$(twoStepBtn).click(function () {
  $(twoStep).modal('hide');
  setTimeout(function () {
    $(twoStepEnable).modal('show');
  }, 500);
}); // $(twoStepEnableForm).validate({
//     rules: {
//         otp: {
//             required: true,
//         },
//     }
// });
//
// $(twoStepDisableForm).validate({
//     rules: {
//         otp: {
//             required: true,
//         },
//     }
// });

$(twoStepDisableForm).submit(function (e) {
  e.preventDefault();

  if ($(twoStepDisableForm).valid()) {
    var self = $(this);
    var data = self.serialize();
    var url = self.attr('action');
    $.ajax({
      url: url,
      method: 'POST',
      data: data,
      success: function success(data) {
        toastr.success('Disable 2-step success', '', 10000);
        $(twoStepDisableForm).modal('show');
        location.reload();
      },
      error: function error(_error3) {
        var errors = _error3.responseJSON.errors;
        var errorMessages = errorMessage(errors);
        toastr.error(errorMessages, 'Errors', 10000);
      }
    });
  }
});

function copyToClipboard(element) {
  var copyText = document.getElementById(element);
  copyText.select();
  document.execCommand("copy");
  toastr.success('Copied', '', 10000);
}

/***/ }),

/***/ 7:
/*!******************************************************!*\
  !*** multi ./modules/ACL/resources/assets/js/app.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/diamond/Documents/vincent/modules/ACL/resources/assets/js/app.js */"./modules/ACL/resources/assets/js/app.js");


/***/ })

/******/ });