/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

import Echo from "laravel-echo"

const token = $('meta[name=csrf-token]').attr('content');

const echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001',
    auth: {
        headers: {
            Authorization: "Bearer " + token
        }
    },
});

echo.channel('chan-demo')
    .listen('.post', (e) => {
        console.log(e);
    });

$('#demo').click((e) => {
    alert('SEND');
    $.get('/post');
});

const groupId = $('meta[name=group-id]').attr('content');
window.demo = echo.join(`group.${groupId}`)
    .here((users) => {
        console.log('here', users)
    })
    .joining((user) => {
        toastr.success( user.name + ' joined');
        console.log('joining', user)
    })
    .leaving((user) => {
        console.log('leaving', user)
    })
    .listen('.group-wizz', (e) => {
        console.log('group-wizz', e);
    }).listenForWhisper('test', (e) => {
        console.log(e);
    });

echo.channel('auth')
    .listen('.logout', (e) => {
        alert('logout');
    })
    .listen('.login', (e) => {
        alert('login');
    });

// echo.channel('chat')
//     .listen('.message', (res) => {
//         if ($('#' + res.id).length === 0) {
//             $('#res').append('<p><strong>' + res.author + '</strong>: ' + res.content + '</p>')
//         }
//         else {
//             toastr.success( 'Notify');
//         }
//         console.log(res);
//     });
//
// const userId = $('meta[name=user-id]').attr('content');
//
// echo.private(`App.User.${userId}`)
//     .notification(function (notification) {
//         alert('Notify');
//         console.log(notification);
//         toastr.success( 'Notify');
// });
