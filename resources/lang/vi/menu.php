<?php

return [

    'acl' => 'Phân quyền',
    'build_module' => 'Xây dựng module code',
    'configs' => 'Config',
    'landing_page' => 'Landing page',
    'keyword' => 'Keyword',
    'component' => 'Thành phần trang',
    'crud' => 'CRUD',
    'e-commerce' => 'Thương mãi điện tử',
    'english' => 'English',
    'media' => 'Ảnh',
    'multi_choice' => 'Trắc nghiệm',

    'log' => 'Nhật ký',
    'transaction' => 'Giao dịch',
    'tutorial' => 'Quản lý học tập',

];
