<?php
/**
 * Created by PhpStorm.
 * User: FightLightDiamond
 * Date: 4/20/2016
 * Time: 10:07 AM
 */
return [
    'auth' => 'Chứng thực',
    'class' => 'Lớp',
    'course' => 'Khóa học',
    'contact' => 'Liên hệ',
    'home' => 'Trang chủ',
    'lang' => 'Ngôn ngữ',
    'exam' => 'Kiểm tra',
    'test' => 'Bài kiểm tra',
    'mini_test' => 'Mini Test',
    'user' => 'User',
    'security' => 'Security',
    'role' => 'vai trò',
    'permission' => 'Giấy phép',
    'professional' => 'Chức nghiệp',
    'role_user' => 'Vai trò cuả người dùng',
    'permission_user' => 'Quyền của người dùng',
    'knowledge' => 'Kiến thức',
    'tutorial' => 'Tutorial',

    'news' => 'News',
    'media' => 'Media'
];