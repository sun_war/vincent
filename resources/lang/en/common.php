<?php

return [
    'all' => 'All',
    'forgot_password_message' => 'Forget password',
    'home' => 'Home',
    'knowledge' => 'knowledge',
    'level' => 'Level',
    'profile' => 'Profile',
    'professional' => 'Professional',
    'remember_me' => 'Remember me',
    'sign_in_message' => 'Đăng nhập để sử dụng',
    'sign_out' => 'Sign out',
];
