<?php
/**
 * Created by PhpStorm.
 * User: FightLightDiamond
 * Date: 4/20/2016
 * Time: 10:07 AM
 */
return [
    'course' => 'Course',
    'news' => 'News',
    'class' => 'Class',
    'contact' => 'Contact',
    'exam' => 'Examination',
    'home' => 'Home',
    'lang' => 'Language',
    'test' => 'Test',
    'mini_test' => 'Mini Test',
    'auth' => 'Auth',
    'user' => 'User',
    'security' => 'Security',
    'role' => 'Role',
    'permission' => 'Permission',
    'role_user' => 'Role for user',
    'permission_user' => 'Permission for user',
    'knowledge' => 'Knowledge',
    'tutorial' => 'Tutorial',
    'media' => 'Media'
];