<?php
/**
 * Created by PhpStorm.
 * User: CPM
 * Date: 5/12/2018
 * Time: 1:43 AM
 */

return [
    'acl' => 'Access Control Level',
    'build_module' => 'Build module code',
    'configs' => 'Config',
    'landing_page' => 'Landing page',
    'keyword' => 'Keyword',
    'tutorial' => 'Tutorial',
    'dev-ops' => 'DevOps',
    'component' => 'Component',
    'crud' => 'CRUD',
    'e-commerce' => 'E-commerce',
    'english' => 'English',
    'media' => 'Media',
    'multi_choice' => 'Multi Choice',
    'log' => 'Log',
    'studio' => 'Studio',
    'model' => 'Model',

    'transaction' => 'Transaction',
];
