<?php
/**
 * Created by PhpStorm.
 * User: JK
 * Date: 4/2/2018
 * Time: 2:44 PM
 */
return [
    'abouts' => 'Abouts',
    'albums' => 'Albums',
    'billionaires' => 'Billionaires',
    'brands' => 'Brands',
    'categories' => 'Categories',
    'coin_logs' => 'Coin Logs',
    'contacts' => 'Contacts',
    'contracts' => 'Contracts',
    'crazies' => 'Crazies',
    'crazy_courses' => 'Crazy Courses',
    'crazy_details' => 'Crazy Details',
    'crazy_histories' => 'Crazy Histories',
    'defend_trees' => 'Defend Trees',
    'dns_test_reports' => 'Dns Test Reports',
    'drop_box_test_reports' => 'Drop Box Test Reports',
    'fast_com_test_reports' => 'Fast Com Test Reports',
    'fill_in_the_blanks' => 'Fill In The Blanks',
    'groups' => 'Group',
    'html5_test_sg_reports' => 'Html5 Test Sg Reports',
    'html5_test_us_reports' => 'Html5 Test Us Reports',
    'http_web_page_load_test_reports' => 'Http Web Page Load Test Reports',
    'i_perf_test_reports' => 'I Perf Test Reports',
    'icmp_test_reports' => 'Icmp Test Reports',
    'images' => 'Images',
    'jobs' => 'Jobs',
    'keyword_tags' => 'Keyword Tags',
    'keywords' => 'Keywords',
    'lesson_comments' => 'Lesson Comments',
    'lesson_feed_backs' => 'Lesson Feed Backs',
    'lesson_results' => 'Lesson Results',
    'lesson_sub_comments' => 'Lesson Sub Comments',
    'lesson_tests' => 'Lesson Tests',
    'lessons' => 'Lessons',
    'migrations' => 'Migrations',
    'mistakes' => 'Mistakes',
    'multi_choices' => 'Multi Choices',
    'news' => 'News',
    'news_tags' => 'News Tags',
    'oauth_access_tokens' => 'Oauth Access Tokens',
    'oauth_auth_codes' => 'Oauth Auth Codes',
    'oauth_clients' => 'Oauth Clients',
    'oauth_personal_access_clients' => 'Oauth Personal Access Clients',
    'oauth_refresh_tokens' => 'Oauth Refresh Tokens',
    'order_details' => 'Order Details',
    'orders' => 'Orders',
    'parameters' => 'Parameters',
    'password_resets' => 'Password Resets',
    'permissions' => 'Permissions',
    'plan_trees' => 'Plan Trees',
    'price_rents' => 'Price Rents',
    'probe_group_parameters' => 'Probe Group Parameters',
    'probe_groups' => 'Probe Group',
    'probe_logs' => 'Probe Logs',
    'probes' => 'Probes',
    'products' => 'Products',
    'pronunciations' => 'Pronunciations',
    'quotations' => 'Quotations',
    'report_details' => 'Report Details',
    'response_choices' => 'Response Choices',
    'results' => 'Results',
    'rg_answers' => 'Rg Answers',
    'rg_questions' => 'Rg Questions',
    'rg_replies' => 'Rg Replies',
    'rg_results' => 'Rg Results',
    'rg_tests' => 'Rg Tests',
    'role_permission' => 'Role Permission',
    'role_user' => 'Role User',
    'roles' => 'Roles',
    'schedules' => 'Schedules',
    'section_results' => 'Section Results',
    'section_tests' => 'Section Tests',
    'sections' => 'Sections',
    'sequences' => 'Sequences',
    'similarities' => 'Similarities',
    'slides' => 'Slides',
    'social_accounts' => 'Social Accounts',
    'speed_test_sg_reports' => 'Speed Test Sg Reports',
    'speed_test_us_reports' => 'Speed Test Us Reports',
    'subjects' => 'Subjects',
    'tags' => 'Tags',
    'tcp_test_reports' => 'Tcp Test Reports',
    'test_details' => 'Test Details',
    'test_results' => 'Test Results',
    'tests' => 'Tests',
    'tutorial_results' => 'Tutorial Results',
    'tutorial_tests' => 'Tutorial Tests',
    'tutorials' => 'Tutorials',
    'types' => 'Types',
    'users' => 'Users',
    'vocabularies' => 'Vocabularies',
    'youtube_test_reports' => 'Youtube Test Reports',

    'a' => 'A',
    'access_id' => 'Access',
    'access_token_id' => 'Access Token',
    'active' => 'Active',
    'active_time' => 'Active Time',
    'address' => 'Address',
    'album_id' => 'Album',
    'answer' => 'Answer',
    'attempts' => 'Attempts',
    'audio' => 'Audio',
    'author' => 'Author',
    'auto_pay' => 'Auto Pay',
    'available_at' => 'Available At',
    'avatar' => 'Avatar',
    'average_download' => 'Average Download',
    'b' => 'B',
    'batch' => 'Batch',
    'birthday' => 'Birthday',
    'borrower' => 'Borrower',
    'borrower_confirm' => 'Borrower Confirm',
    'brand_id' => 'Brand',
    'c' => 'C',
    'category' => 'Category',
    'category_id' => 'Category',
    'change' => 'Change',
    'client_id' => 'Client',
    'code' => 'Code',
    'coin' => 'Coin',
    'comment' => 'Comment',
    'content' => 'Content',
    'crazy_course_id' => 'Crazy Course',
    'crazy_id' => 'Crazy',
    'create_by' => 'Create By',
    'created_at' => 'Created At',
    'created_by' => 'Created By',
    'current_speed' => 'Current Speed',
    'd' => 'D',
    'deal' => 'Deal',
    'deleted_at' => 'Deleted At',
    'description' => 'Description',
    'details' => 'Details',
    'discount' => 'Discount',
    'display_name' => 'Display Name',
    'dns_resolution_time' => 'Dns Resolution Time',
    'download' => 'Download',
    'download_rate' => 'Download Rate',
    'email' => 'Email',
    'end_time' => 'End Time',
    'expires_at' => 'Expires At',
    'file' => 'File',
    'first_name' => 'First Name',
    'google' => 'Google',
    'health' => 'Health',
    'hot' => 'Hot',
    'id' => 'Id',
    'identify' => 'Identify',
    'identity' => 'Identity',
    'image' => 'Image',
    'img' => 'Img',
    'info' => 'Info',
    'integer' => 'Integer',
    'interest' => 'Interest',
    'intro' => 'Intro',
    'ip' => 'Ip',
    'is_active' => 'Is Active',
    'is_auto_pay' => 'Is Auto Pay',
    'is_reboot' => 'Is Reboot',
    'is_transaction' => 'Is Transaction',
    'job' => 'Job',
    'keyword' => 'Keyword',
    'keyword_id' => 'Keyword',
    'knowledge' => 'Knowledge',
    'ladder' => 'Ladder',
    'last_login' => 'Last Login',
    'last_logout' => 'Last Logout',
    'last_name' => 'Last Name',
    'last_offline' => 'Last Offline',
    'last_online' => 'Last Online',
    'last_view' => 'Last View',
    'latency' => 'Latency',
    'lender' => 'Lender',
    'lender_confirm' => 'Lender Confirm',
    'lesson_comment' => 'Lesson Comment',
    'lesson_id' => 'Lesson',
    'level' => 'Level',
    'link' => 'Link',
    'load_time' => 'Load Time',
    'location' => 'Location',
    'log' => 'Log',
    'map' => 'Map',
    'max' => 'Max',
    'meaning' => 'Meaning',
    'message' => 'Message',
    'migration' => 'Migration',
    'min' => 'Min',
    'module_id' => 'Module',
    'money' => 'Money',
    'name' => 'Name',
    'news_id' => 'News',
    'no' => 'No',
    'note' => 'Note',
    'order_id' => 'Order',
    'package_loss' => 'Package Loss',
    'page' => 'Page',
    'password' => 'Password',
    'password_client' => 'Password Client',
    'payload' => 'Payload',
    'percent' => 'Percent',
    'period' => 'Period',
    'permission_id' => 'Permission',
    'personal_access_client' => 'Personal Access Client',
    'phone_number' => 'Phone Number',
    'picture' => 'Picture',
    'ping' => 'Ping',
    'plan_tree_id' => 'Plan Tree',
    'price' => 'Price',
    'priority' => 'Priority',
    'probe_group_id' => 'Probe Group',
    'probe_id' => 'Probe',
    'probe_name' => 'Probe Name',
    'product_id' => 'Product',
    'professional' => 'Professional',
    'pronounce' => 'Pronounce',
    'pronunciation_a' => 'Pronunciation A',
    'pronunciation_b' => 'Pronunciation B',
    'pronunciation_c' => 'Pronunciation C',
    'pronunciation_d' => 'Pronunciation D',
    'provider' => 'Provider',
    'provider_user_id' => 'Provider User',
    'quantity' => 'Quantity',
    'question' => 'Question',
    'question_id' => 'Question',
    'questions' => 'Questions',
    'queue' => 'Queue',
    'reason' => 'Reason',
    'recommended' => 'Recommended',
    'redirect' => 'Redirect',
    'remember_token' => 'Remember Token',
    'repair' => 'Repair',
    'replacement' => 'Replacement',
    'reply' => 'Reply',
    'reply1' => 'Reply1',
    'reply2' => 'Reply2',
    'reply3' => 'Reply3',
    'reply4' => 'Reply4',
    'reply5' => 'Reply5',
    'report_id' => 'Report',
    'reserved_at' => 'Reserved At',
    'response_time' => 'Response Time',
    'result' => 'Result',
    'revoked' => 'Revoked',
    'rg_question_id' => 'Rg Question',
    'rg_question_ids' => 'Rg Question',
    'rg_results_id' => 'Rg Results',
    'rg_test_id' => 'Rg Test',
    'role_id' => 'Role',
    'round_trip_time_avg' => 'Round Trip Time Avg',
    'route' => 'Route',
    'scheduleable_id' => 'Scheduleable',
    'scheduleable_type' => 'Scheduleable Type',
    'scopes' => 'Scopes',
    'score' => 'Score',
    'secret' => 'Secret',
    'section_id' => 'Section',
    'sentence' => 'Sentence',
    'serial_number' => 'Serial Number',
    'sex' => 'Sex',
    'sing_net_100' => 'Sing Net 100',
    'sing_net_83' => 'Sing Net 83',
    'slack_webhook_url' => 'Slack Webhook Url',
    'source' => 'Source',
    'standard_deviation' => 'Standard Deviation',
    'status' => 'Status',
    'subject_id' => 'Subject',
    'tag_id' => 'Tag',
    'target' => 'Target',
    'test_case_id' => 'Test Case',
    'test_id' => 'Test',
    'test_name' => 'Test Name',
    'time' => 'Time',
    'title' => 'Title',
    'token' => 'Token',
    'total' => 'Total',
    'tutorial_id' => 'Tutorial',
    'type' => 'Type',
    'type_id' => 'Type',
    'updated_at' => 'Updated At',
    'updated_by' => 'Updated By',
    'upload' => 'Upload',
    'url' => 'Url',
    'user_id' => 'User',
    'version' => 'Version',
    'video' => 'Video',
    'views' => 'Views',
    'word' => 'Word',
];