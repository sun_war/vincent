<?php
define('OK', 200);
define('CREATED', 201);
define('BAD_REQUEST', 400);
define('UNAUTHORIZED', 401);
define('FORBIDDEN', 403);
define('NOT_FOUND', 404);
define('NOT_ACEPTABLE', 406);
define('SERVER_ERROR', 500);

const LOCALES = [
    'en' => 'English',
    'jp' => 'Japanese',
    'vi' => 'Vietnamese',
];

const QUESTIONS_COL = 'questions';

const TECHNIQUE = 1;
const TECHNIQUE_NAME = 'Chuyên môn';
const MAJOR = 2;
const MAJOR_NAME = 'Nghiệp vụ';
const PROFESSIONAL = [TECHNIQUE => TECHNIQUE_NAME, MAJOR => MAJOR_NAME];
const PROFESSIONAL_KEY = [TECHNIQUE, MAJOR];

const KNOWLEDGE_OF_DESIGN_PATTERN = 1;
const KNOWLEDGE_OF_DESIGN_PATTERN_NAME = 'Kiến thức về Design Pattern';
const SOFTWARE_DEVELOPMENT_PROCESS = 2;
const SOFTWARE_DEVELOPMENT_PROCESS_NAME = 'Quy trình phát triển phần mềm';
const KNOWLEDGE_OF_PHP_PROGRAMING_LANGUAGE = 3;
const KNOWLEDGE_OF_PHP_PROGRAMING_LANGUAGE_NAME = 'Kiến thức về ngôn ngữ Lập trình PHP';
const BASIC_IT_KNOWLEDGE = 4;
const BASIC_IT_KNOWLEDGE_NAME = 'Kiến thức CNTT cơ bản';
const KNOWLEDGE_OF_DESIGN_TOOLS = 5;
const KNOWLEDGE_OF_DESIGN_TOOLS_NAME = 'Kiến thức về Công cụ thiết kế';
const KNOWLEDGE_OF_PROGRAMING_TUTORIALS = 6;
const KNOWLEDGE_OF_PROGRAMING_TUTORIALS_NAME = 'Kiến thức về Các hướng dẫn Lập trình';
const KNOWLEDGE_OF_PROGRAMMING_TOOLS = 7;
const KNOWLEDGE_OF_PROGRAMMING_TOOLS_NAME = 'Kiến thức về Công cụ Lập trình';
const KNOWLEDGE_OF_PHP_DEVELOPER_CERTIFICATION_BY_W3SCHOOLS = 8;
const KNOWLEDGE_OF_PHP_DEVELOPER_CERTIFICATION_BY_W3SCHOOLS_NAME = 'Kiến thức theo chuẩn chứng chỉ PHP Developer (của w3schools)';
const LARAVEL = 9;
const LARAVEL_NAME = 'Framework PHP Laravel';

const KNOWLEDGE = [
    KNOWLEDGE_OF_DESIGN_PATTERN => KNOWLEDGE_OF_DESIGN_PATTERN_NAME,
    SOFTWARE_DEVELOPMENT_PROCESS => SOFTWARE_DEVELOPMENT_PROCESS_NAME,
    KNOWLEDGE_OF_PHP_PROGRAMING_LANGUAGE => KNOWLEDGE_OF_PHP_PROGRAMING_LANGUAGE_NAME,
    BASIC_IT_KNOWLEDGE => BASIC_IT_KNOWLEDGE_NAME,
    KNOWLEDGE_OF_DESIGN_TOOLS => KNOWLEDGE_OF_DESIGN_TOOLS_NAME,
    KNOWLEDGE_OF_PROGRAMING_TUTORIALS => KNOWLEDGE_OF_PROGRAMING_TUTORIALS_NAME,
    KNOWLEDGE_OF_PROGRAMMING_TOOLS => KNOWLEDGE_OF_PROGRAMMING_TOOLS_NAME,
    KNOWLEDGE_OF_PHP_DEVELOPER_CERTIFICATION_BY_W3SCHOOLS => KNOWLEDGE_OF_PHP_DEVELOPER_CERTIFICATION_BY_W3SCHOOLS_NAME,
    9 => 'Lập trình iOS',
    10 =>'Phân tích thiết kế, Design pattern',
    11 => 'Các công cụ phát triển, công cụ quản lý',
    12 => 'Hướng dẫn lập trình, tối ưu hiệu năng, ATTT',
    13 => 'Thiết kế hệ thống phần mềm, hạ tầng phần cứng',
];

const KNOWLEDGE_KEY = [
    KNOWLEDGE_OF_DESIGN_PATTERN,
    SOFTWARE_DEVELOPMENT_PROCESS,
    KNOWLEDGE_OF_PHP_PROGRAMING_LANGUAGE,
    BASIC_IT_KNOWLEDGE,
    KNOWLEDGE_OF_DESIGN_TOOLS,
    KNOWLEDGE_OF_PROGRAMING_TUTORIALS,
    KNOWLEDGE_OF_PROGRAMMING_TOOLS,
    KNOWLEDGE_OF_PHP_DEVELOPER_CERTIFICATION_BY_W3SCHOOLS,
    LARAVEL,
];
const LEVEL = [1, 2, 3, 4, 5, 6, 7];

/**
 * ALC
 */

const VIEW_ACCESS = 1;
const EXECUTE_ACCESS = 2;
const UPDATE_ACCESS = 3;
const DELETE_ACCESS = 4;
const ACTIVE_ACCESS = 6;
const CREATE_ACCESS = 8;

const VIEW_ACCESS_NAME = 'View';
const CREATE_ACCESS_NAME = 'Create';
const UPDATE_ACCESS_NAME = 'Update';
const DELETE_ACCESS_NAME = 'Delete';
const EXECUTE_ACCESS_NAME = 'Active';
const ACTIVE_ACCESS_NAME = 'Execute';
const EXPORT_ACCESS_NAME = 'Export';

const ACCESSES = [
    VIEW_ACCESS => VIEW_ACCESS_NAME,
    EXECUTE_ACCESS => EXECUTE_ACCESS_NAME,
    UPDATE_ACCESS => UPDATE_ACCESS_NAME,
    DELETE_ACCESS => DELETE_ACCESS_NAME,
    ACTIVE_ACCESS => ACTIVE_ACCESS_NAME,
    CREATE_ACCESS => CREATE_ACCESS_NAME
];

const VIEWER_LEVEL = 1;
const TESTER_LEVEL = 2;
const DEVELOPER_LEVEL = 3;
const MANAGER_LEVEL = 4;
const ADMIN_LEVEL = 5;

const LEVELS = [VIEWER_LEVEL, TESTER_LEVEL, DEVELOPER_LEVEL, MANAGER_LEVEL, ADMIN_LEVEL];

const ACCESS_LEVEL = [
    1 => [VIEW_ACCESS],
    2 => [VIEW_ACCESS, EXECUTE_ACCESS],
    3 => [VIEW_ACCESS, EXECUTE_ACCESS, UPDATE_ACCESS],
    4 => [VIEW_ACCESS, EXECUTE_ACCESS, UPDATE_ACCESS, DELETE_ACCESS, CREATE_ACCESS],
    5 => [VIEW_ACCESS, EXECUTE_ACCESS, UPDATE_ACCESS, DELETE_ACCESS, ACTIVE_ACCESS, CREATE_ACCESS],
];

const ACCESS_LEVEL_NAME = [
    1 => [VIEW_ACCESS => VIEW_ACCESS_NAME],
    2 => [VIEW_ACCESS => VIEW_ACCESS_NAME, EXECUTE_ACCESS => EXECUTE_ACCESS_NAME],
    3 => [VIEW_ACCESS => VIEW_ACCESS_NAME, EXECUTE_ACCESS => EXECUTE_ACCESS_NAME, UPDATE_ACCESS => UPDATE_ACCESS_NAME],
    4 => [VIEW_ACCESS => VIEW_ACCESS_NAME, EXECUTE_ACCESS => EXECUTE_ACCESS_NAME, UPDATE_ACCESS => UPDATE_ACCESS_NAME,
        CREATE_ACCESS => CREATE_ACCESS_NAME, DELETE_ACCESS => DELETE_ACCESS_NAME],
    5 => [CREATE_ACCESS => CREATE_ACCESS_NAME, VIEW_ACCESS => VIEW_ACCESS_NAME, EXECUTE_ACCESS => EXECUTE_ACCESS_NAME,
        UPDATE_ACCESS => UPDATE_ACCESS_NAME, ACTIVE_ACCESS => ACTIVE_ACCESS_NAME,
         DELETE_ACCESS => DELETE_ACCESS_NAME],
];

const ROLE_LEVEL = [
    1 => ACCESS_LEVEL[1],
    2 => ACCESS_LEVEL[2],
    3 => ACCESS_LEVEL[4],
    4 => ACCESS_LEVEL[4],
    5 => ACCESS_LEVEL[5],
];

const TEST_CASE_STATUS_MODULE = 1;
const TEST_CASE_DASHBOARD_MODULE = 2;
const TEST_CASE_MANAGEMENT_MODULE = 3;
const PROBE_MANAGEMENT_MODULE = 4;
const UAT_DASHBOARD_MODULE = 5;
const PROJECT_DASHBOARD_MODULE = 6;
const PROBE_DASHBOARD_MODULE = 7;
const RESULT_HISTORY_MODULE = 8;
const USER_MANAGEMENT_MODULE = 9;
const EQUIPMENT_MANAGEMENT_MODULE = 10;

const TEST_CASE_STATUS_MODULE_NAME = 'Test case status';
const TEST_CASE_DASHBOARD_MODULE_NAME = 'Test case dashboard';
const TEST_CASE_MANAGEMENT_MODULE_NAME = 'Test case management';
const PROBE_MANAGEMENT_MODULE_NAME = 'Probe management';
const UAT_DASHBOARD_MODULE_NAME = 'UAT dashboard';
const PROJECT_DASHBOARD_MODULE_NAME = 'Project dashboard';
const PROBE_DASHBOARD_MODULE_NAME = 'Probe dashboard';
const RESULT_HISTORY_MODULE_NAME = 'Result history';
const USER_MANAGEMENT_MODULE_NAME = 'User management';
const EQUIPMENT_MANAGEMENT_MODULE_NAME = 'Equipment management';

const ACCESS_MODULE = [
    TEST_CASE_STATUS_MODULE => TEST_CASE_STATUS_MODULE_NAME,
    TEST_CASE_DASHBOARD_MODULE => TEST_CASE_DASHBOARD_MODULE_NAME,
    TEST_CASE_MANAGEMENT_MODULE => TEST_CASE_MANAGEMENT_MODULE_NAME,
    PROBE_MANAGEMENT_MODULE => PROBE_MANAGEMENT_MODULE_NAME,
    UAT_DASHBOARD_MODULE => UAT_DASHBOARD_MODULE_NAME,
    PROJECT_DASHBOARD_MODULE => PROJECT_DASHBOARD_MODULE_NAME,
    PROBE_DASHBOARD_MODULE => PROBE_DASHBOARD_MODULE_NAME,
    RESULT_HISTORY_MODULE => RESULT_HISTORY_MODULE_NAME,
    USER_MANAGEMENT_MODULE => USER_MANAGEMENT_MODULE_NAME,
    EQUIPMENT_MANAGEMENT_MODULE => EQUIPMENT_MANAGEMENT_MODULE_NAME,
];

const ROLE_MODULE = [
    1 => [TEST_CASE_STATUS_MODULE, UAT_DASHBOARD_MODULE, PROJECT_DASHBOARD_MODULE, PROBE_DASHBOARD_MODULE],
    2 => [TEST_CASE_STATUS_MODULE, TEST_CASE_DASHBOARD_MODULE, TEST_CASE_MANAGEMENT_MODULE, PROBE_MANAGEMENT_MODULE,
        UAT_DASHBOARD_MODULE, PROJECT_DASHBOARD_MODULE, PROBE_DASHBOARD_MODULE, RESULT_HISTORY_MODULE],
    3 => [TEST_CASE_STATUS_MODULE, TEST_CASE_DASHBOARD_MODULE, TEST_CASE_MANAGEMENT_MODULE, PROBE_MANAGEMENT_MODULE,
        UAT_DASHBOARD_MODULE, PROJECT_DASHBOARD_MODULE, PROBE_DASHBOARD_MODULE, RESULT_HISTORY_MODULE],
    4 => [TEST_CASE_STATUS_MODULE, TEST_CASE_DASHBOARD_MODULE, TEST_CASE_MANAGEMENT_MODULE, PROBE_MANAGEMENT_MODULE,
        UAT_DASHBOARD_MODULE, PROJECT_DASHBOARD_MODULE, PROBE_DASHBOARD_MODULE, RESULT_HISTORY_MODULE, USER_MANAGEMENT_MODULE, EQUIPMENT_MANAGEMENT_MODULE],
    5 => [TEST_CASE_STATUS_MODULE, TEST_CASE_DASHBOARD_MODULE, TEST_CASE_MANAGEMENT_MODULE, PROBE_MANAGEMENT_MODULE,
        UAT_DASHBOARD_MODULE, PROJECT_DASHBOARD_MODULE, PROBE_DASHBOARD_MODULE, RESULT_HISTORY_MODULE, USER_MANAGEMENT_MODULE, EQUIPMENT_MANAGEMENT_MODULE],
];

const ROLES = ['Viewer', 'Tester', 'Developer', 'Manager', 'Admin'];

/**
 * Test
 */

const REP_LIST = [1 => 'reply1', 2 => 'reply2', 3 => 'reply3', 4 => 'reply4', 5 => 'reply5'];
const REP_LESSONS = [1 => 'reply1', 2 => 'reply2', 3 => 'reply3', 4 => 'reply4'];
const REPLIES = ['a', 'b', 'c', 'd'];
const PRONUNCIATIONS = ['pronunciation_a', 'pronunciation_b', 'pronunciation_c', 'pronunciation_d'];

const BETTING = 'betting';
const COIN = 'coin';
const ERROR = 'error';
const SUCCESS = 'success';
const PER_PAGE = 'per_page';

const NULL_FILTER = '#NULL#';
const NOT_NULL_FILTER = '#NOT_NULL#';

const ENGLISH_PER_PAGE = 20;

const FIRST_TEST = 1;
const AGAIN_TEST = 2;

const SOCIALS = [
    'facebook',
    'youtube',
    'skype',
    'telegram'
];

const SOCIAL_ATTRIBUTES = [
    'name' => 'text',
    'icon' => 'file',
    'icon_hover' => 'text',
    'link' => 'text'
];