<?php
/**
 * Created by PhpStorm.
 * User: soi
 * Date: 7/5/17
 * Time: 8:22 PM
 */

return [
    'professional' => [
        'Chuyên môn' => 1,
        'Nghiệp vụ' => 2,
        'CM' => 1,
        'NV' => 2,
    ],
    'knowledge' => [
        'Kiến thức về Design Pattern.' => 1,
        'Quy trình phát triển phần mềm' => 2,
        'Kiến thức về ngôn ngữ Lập trình PHP' => 3,
        'Kiến thức CNTT cơ bản' => 4,
        'Kiến thức về Công cụ thiết kế' => 5,
        'Kiến thức về Các hướng dẫn Lập trình' => 6,
        'Kiến thức về Công cụ Lập trình' => 7,
        'Kiến thức theo chuẩn chứng chỉ PHP Developer (của w3schools)' => 8,
        'Lập trình iOS' => 9,
        'Phân tích thiết kế, Design pattern' => 10,
        'Các công cụ phát triển, công cụ quản lý' => 11,
        'Hướng dẫn lập trình, tối ưu hiệu năng, ATTT' => 12,
        'Thiết kế hệ thống phần mềm, hạ tầng phần cứng' => 13,
    ],
    'paginate' => [
        'table' => 10,
        'test' => 20
    ]
];