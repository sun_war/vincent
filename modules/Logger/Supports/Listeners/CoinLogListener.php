<?php

namespace Logger\Supports\Listeners;


use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Logger\Models\CoinLog;
use Logger\Supports\Events\CoinLogEvent;

class CoinLogListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CoinLogEvent  $event
     * @return void
     */
    public function handle(CoinLogEvent $event)
    {
        app(CoinLog::class)->create($event->data);
    }
}
