<table class="table">
    <thead>
        <tr>
            <th>{{trans('label.coin')}}</th>
<th>{{trans('label.deal')}}</th>
<th>{{trans('label.created_by')}}</th>
<th>{{trans('label.type')}}</th>

            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($coinLogs as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td>{{$row->coin}}</td>
<td>{{$row->deal}}</td>
<td>{{$row->created_by}}</td>
<td>{{$row->type}}</td>

            <td class="text-right">
                <form method="POST" action="{{route('coin-log.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('coin-log.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('coin-log.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$coinLogs->links()}}
</div>
