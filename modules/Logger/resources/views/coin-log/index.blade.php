@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('coin-log.index')}}">{{trans('table.coin_logs')}}</a>
        </li>
        <li class="active">
            <strong>Table</strong>
        </li>
    </ol>
    <form class="form-group row" id="formFilter" action="{{route('coin-log.index')}}" method="POST">
        <div class="col-sm-2 form-group">
            <label for="per_page">{{__('label.per_page')}}</label>
            <select name="per_page" class="form-control selectFilter" id="per_page">
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="40">40</option>
                <option value="50">50</option>
            </select>
        </div>
        <div class="col-sm-4 form-group">
            <label for="name">{{__('label.start_time')}}</label>
            <input name="start_time" class="form-control inputFilter" id="start_time">
        </div>
        <div class="col-sm-4 form-group">
            <label for="is_active">{{__('label.end_time')}}</label>
            <input name="end_time" class="form-control inputFilter" id="end_time">
        </div>
        <div class="col-sm-1 form-group">
            <label>{{__('label.action')}}</label>
            <a class="btn btn-primary btn-block" href="{{route('coin-log.create')}}"><i class="fa fa-plus"></i></a>
        </div>
    </form>
    <div id="chart8">

    </div>
    <div id="chart7">

    </div>
    <div class="form-group" id="table">
        @include('log::coin-log.table')
    </div>
@endsection
@push('css')
    <link rel="stylesheet"
          href="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}"/>
@endpush
@push('js')
    <script type="text/javascript" src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript">
        $("#start_time").datetimepicker({format: 'YYYY-MM-DD'});
        $("#end_time").datetimepicker({format: 'YYYY-MM-DD'});
    </script>

    <script src="{{asset('build/form-filter.js')}}"></script>
    <script src="{{asset('')}}assets/js/morris.min.js"></script>
    <script>
        $.ajax({
            url: "{{route('coin-log.charts')}}",
            method: 'GET',
            success: function (data) {
                console.log(data);
                Morris.Line({
                    element: 'chart8',
                    data: data,
                    xkey: 'created_at',
                    ykeys: ['deal'],
                    labels: 'COIN',
                    parseTime: false,
                    lineColors: ['#242d3c']
                });
                Morris.Line({
                    element: 'chart7',
                    data: data,
                    xkey: 'created_at',
                    ykeys: ['coin'],
                    labels: 'COIN',
                    parseTime: false,
                    lineColors: ['#942d3c']
                });
            },
            error: function () {
                alert('error')
            }
        })
    </script>
@endpush