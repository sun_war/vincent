@extends('layouts.app')
@section('content')
<div class="row">
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('coin-log.index')}}">{{trans('table.coin_logs')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.update')}}</strong>
        </li>
    </ol>
    <form action="{{route('coin-log.update', $coinLog->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-6">
    <label for="coin">{{trans('label.coin')}}</label>
    <input type="number" class="form-control" name="coin"  id="coin" value="{{$coinLog->coin}}">
</div>
<div class="form-group col-lg-6">
    <label for="deal">{{trans('label.deal')}}</label>
    <input type="number" class="form-control" name="deal"  id="deal" value="{{$coinLog->deal}}">
</div>
<div class="form-group col-lg-6">
    <label for="created_by">{{trans('label.created_by')}}</label>
    <input type="number" class="form-control" name="created_by"  id="created_by" value="{{$coinLog->created_by}}">
</div>
<div class="form-group col-lg-6">
    <label for="type">{{trans('label.type')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" name="type"  id="type" value="{{$coinLog->type}}">
        </label>
    </div>
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection