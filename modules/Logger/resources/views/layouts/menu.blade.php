<li class="has-sub root-level" id="loggerMenu">
    <a>
        <i class="entypo-gauge"></i>
        <span class="title">{{__('menu.log')}}</span>
    </a>
    <ul>
        <li class="{{ request()->is('coin-log*') ? 'active' : ''}}" id="coinLogMenu">
            <a href="{{route('coin-log.index')}}">
                <span class="title">{{__('table.coin_logs')}}</span>
            </a>
        </li>
    </ul>
</li>