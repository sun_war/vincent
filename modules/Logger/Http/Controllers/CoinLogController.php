<?php

namespace Logger\Http\Controllers;

use App\Http\Controllers\Controller;
use Logger\Http\Requests\CoinLogCreateRequest;
use Logger\Http\Requests\CoinLogUpdateRequest;
use Logger\Repositories\CoinLogRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class CoinLogController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(CoinLogRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['coinLogs'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('log::coin-log.table', $data)->render();
        }
        return view('log::coin-log.index', $data);
    }

    public function create()
    {
        return view('log::coin-log.create');
    }

    public function store(CoinLogCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('coin-log.index');
    }

    public function show($id)
    {
        $coinLog = $this->repository->find($id);
        if(empty($coinLog))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('log::coin-log.show', compact('coinLog'));
    }

    public function edit($id)
    {
        $coinLog = $this->repository->find($id);
        if(empty($coinLog))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('log::coin-log.update', compact('coinLog'));
    }

    public function update(CoinLogUpdateRequest $request, $id)
    {
        $input = $request->all();
        $coinLog = $this->repository->find($id);
        if(empty($coinLog))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $coinLog);
        session()->flash('success', 'update success');
        return redirect()->route('coin-log.index');
    }

    public function destroy($id)
    {
        $coinLog = $this->repository->find($id);
        if(empty($coinLog))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }

    public function charts(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->makeModel()
            ->orderBy(ID_COL, 'DESC')
            ->filter($input)
            ->select(CREATED_AT_COL, COIN_COL, DEAL_COL)
            ->limit(150)
            ->get()->toArray();
        $data = array_reverse ($data);
        return response()->json($data);
    }
}
