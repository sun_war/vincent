<?php
/**
 * Created by PhpStorm.
 * User: JK
 * Date: 3/25/2018
 * Time: 11:33 PM
 */

Route::group(
    [
        'middleware' => ['web'],
        'namespace' => 'Logger\Http\Controllers',
        'prefix' => 'log'
    ], function () {

    Route::group(['auth:admin', 'role:admin'], function () {
        Route::resource('coin-log' , 'CoinLogController');
    });
    Route::get('coin-log-charts' , 'CoinLogController@charts')->name('coin-log.charts');
});




