<?php

namespace Logger\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class CoinLog extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'coin_logs';
    public $fillable = [COIN_COL, DEAL_COL, CREATED_BY_COL, TYPE_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input[COIN_COL])) {
            $query->where(COIN_COL, $input[COIN_COL]);
        }
        if (isset($input[DEAL_COL])) {
            $query->where(DEAL_COL, $input[DEAL_COL]);
        }
        if (isset($input[CREATED_BY_COL])) {
            $query->where(CREATED_BY_COL, $input[CREATED_BY_COL]);
        }
        if (isset($input[TYPE_COL])) {
            $query->where(TYPE_COL, $input[TYPE_COL]);
        }
        if(isset($input['start_time'])) {
            $query->where(CREATED_AT_COL, '>=', $input['start_time'] . ' 00:00:00');
        }
        if(isset($input['end_time'])) {
            $query->where(CREATED_AT_COL, '<=', $input['end_time'] . ' 23:59:59');
        }
        return $query;
    }

}

