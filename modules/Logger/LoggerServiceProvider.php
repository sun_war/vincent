<?php

namespace Logger\Providers;

use Illuminate\Support\ServiceProvider;
use Logger\Repositories\CoinLogRepository;
use Logger\Repositories\CoinLogRepositoryEloquent;

class LoggerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrates');
        $this->loadRoutesFrom(__DIR__ . '/router.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'log');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CoinLogRepository::class, CoinLogRepositoryEloquent::class);
    }
}
