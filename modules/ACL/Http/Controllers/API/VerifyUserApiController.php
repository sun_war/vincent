<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace ACL\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use ACL\Http\Requests\VerifyUserCreateRequest;
use ACL\Http\Requests\VerifyUserUpdateRequest;
use ACL\Http\Resources\VerifyUserResource;
use ACL\Http\Repositories\VerifyUserRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class VerifyUserApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(VerifyUserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new VerifyUserResource($data);
    }

    public function create()
    {
        return view('acl::verify-user.create');
    }

    public function store(VerifyUserCreateRequest $request)
    {
        $input = $request->all();
        $verifyUser = $this->repository->store($input);
        return new VerifyUserResource($verifyUser);
    }

    public function show($id)
    {
        $verifyUser = $this->repository->find($id);
        if (empty($verifyUser)) {
            return new VerifyUserResource([$verifyUser]);
        }
        return new VerifyUserResource($verifyUser);
    }

    public function edit($id)
    {
        $verifyUser = $this->repository->find($id);
        if (empty($verifyUser)) {
            return new VerifyUserResource([$verifyUser]);
        }
        return new VerifyUserResource($verifyUser);
    }

    public function update(VerifyUserUpdateRequest $request, $id)
    {
        $input = $request->all();
        $verifyUser = $this->repository->find($id);
        if (empty($verifyUser)) {
            return new VerifyUserResource([$verifyUser]);
        }
        $data = $this->repository->change($input, $verifyUser);
        return new VerifyUserResource($data);
    }

    public function destroy($id)
    {
        $verifyUser = $this->repository->find($id);
        if (empty($verifyUser)) {
            return new VerifyUserResource($verifyUser);
        }
        $data = $this->repository->delete($id);
        return new VerifyUserResource([$data]);
    }
}