<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 2/15/19
 * Time: 11:11 AM
 */

Route::name('api.')
    ->namespace('ACL\Http\Controllers\API\Auth')
    ->prefix('api')
    ->middleware(['api'])
    ->group( function () {
        Route::post('forgot-password' , 'ForgotPasswordController@sendResetLinkEmail');
        Route::post('register' , 'RegisterController@register');
        Route::post('reset' , 'ResetPasswordController@reset');
    });

Route::name('api.')
    ->namespace('ACL\Http\Controllers\API')
    ->prefix('api')
    ->middleware(['api'])
    ->group( function () {
        Route::resource('verify-users' , 'VerifyUserApiController');
    });
