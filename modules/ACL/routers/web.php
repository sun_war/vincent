<?php
/**
 * Created by PhpStorm.
 * User: JK
 * Date: 3/8/2018
 * Time: 7:45 PM
 */
Route::group(['namespace' => 'ACL\Http\Controllers', 'middleware' => ['web', 'locale.db']], function () {
    Auth::routes();
    Route::get('/spy/{id}', 'Auth\LoginController@spy')->name('spy');
});

Route::name('admin.')
    ->namespace('ACL\Http\Controllers\Admin')
    ->middleware(['web', 'locale.db'])
    ->prefix('admin')
    ->group(function () {
        Auth::routes();
    });

Route::group(['namespace' => 'ACL\Http\Controllers', 'middleware' => ['web', 'locale.db']], function () {
    Route::get('/admin', 'HomeController@index')->name('admin');
    Route::get('/locale-session/{locale}', 'LocaleController@session')->name('locale.session');

    Route::group(['middleware' => 'auth', 'prefix' => 'acl'], function () {
        Route::get('profile', 'UserController@profile')->name('acl.profile');
        Route::put('profile/{id}', 'UserController@updateProfile')->name('profile.update');
        Route::put('change-password', 'UserController@changePassword')->name('change-password');
        Route::put('transaction', 'UserController@transaction')->name('transaction');
        Route::post('google-otp', 'ProfileController@createGoogleOtp')->name('otp.google');
        Route::post('google-otp-enable', 'ProfileController@enableOtp')->name('otp.google.enable');
        Route::post('google-otp-disable', 'ProfileController@disableOtp')->name('otp.google.disable');
        Route::get('/locale-db', 'LocaleController@db')->name('locale.db');
    });
    Route::group(['middleware' => 'auth:admin', 'prefix' => 'acl'], function () {
        Route::resource('admins', 'AdminController');
        Route::group(['middleware' => 'role:admin'], function () {
            Route::resource('permissions', 'PermissionController');
            Route::resource('roles', 'RoleController');
            Route::resource('users', 'UserController');
            Route::put('profile/{id}', 'UserController@updateProfile')->name('profile.update');
            Route::put('renew-password/{id}', 'UserController@renewPassword')->name('renew-password');
            Route::put('ban/{id}', 'UserController@ban')->name('users.ban');
            Route::resource('verify-users', 'VerifyUserController');
        });
    });

    Route::get('verify-email/{code}', 'VerifyUserController@email')->name('verify.email');
});

