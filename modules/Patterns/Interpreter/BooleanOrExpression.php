<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/6/18
 * Time: 3:03 PM
 */

namespace Patterns\Interpreter;


class BooleanOrExpression extends OperatorExpression
{
    protected function doInterpret(InterpreterContext $context, $result_l, $result_r)
    {
        $context->replace($this, $result_l || $result_r);
    }
}