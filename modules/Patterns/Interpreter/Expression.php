<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/6/18
 * Time: 11:22 AM
 */

namespace Patterns\Interpreter;


abstract class Expression
{
    private static $keycount = 0;
    private $key;

    abstract function interpret(InterpreterContext $context);

    function getKey()
    {
        if (!isset($this->key)) {
            self::$keycount++;
            $this->key = self::$keycount;
        }
        return $this->key;
    }
}