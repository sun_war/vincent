<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/6/18
 * Time: 11:24 AM
 */

namespace Patterns\Interpreter;


class LiteralExpression extends Expression
{
    private $value;
    function __construct( $value ) {
        $this->value = $value;
    }
    function interpret( InterpreterContext $context ) {
        $context->replace( $this, $this->value );
    }
}