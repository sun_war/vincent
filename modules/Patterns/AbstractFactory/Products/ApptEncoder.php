<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 10:02 AM
 */

namespace Patterns\AbstractFactory\Products;


abstract class ApptEncoder
{
    abstract function encode();
}
