<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 10:20 AM
 */

namespace Patterns\AbstractFactory\Products;


abstract class CommsManager
{
    abstract function getHeaderText();
    abstract function getApptEncoder();
    abstract function getTtdEncoder();
    abstract function getContactEncoder();
    abstract function getFooterText();
}