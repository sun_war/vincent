<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 10:23 AM
 */

namespace Patterns\AbstractFactory\Products;


class BloggsCommsManager extends CommsManager
{
    function getHeaderText()
    {
        return "BloggsCal header\n";
    }

    function getApptEncoder()
    {
        return new BloggsApptEncoder();
    }

    function getTtdEncoder()
    {
//        return new BloggsTtdEncoder();
    }

    function getContactEncoder()
    {
//        return new BloggsContactEncoder();
    }

    function getFooterText()
    {
        return "BloggsCal footer\n";
    }
}