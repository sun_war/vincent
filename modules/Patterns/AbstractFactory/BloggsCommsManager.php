<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 10:53 AM
 */

namespace Patterns\AbstractFactory;


use Patterns\AbstractFactory\Products\BloggsApptEncoder;

class BloggsCommsManager extends CommsManager
{
    function getHeaderText()
    {
        return "BloggsCal header\n";
    }

    function make($flag_int)
    {
        switch ($flag_int) {
            case self::APPT:
                return new BloggsApptEncoder();
            case self::CONTACT:
                return 'contact';
//                return new BloggsContactEncoder();
            case self::TTD:
                return 'ttd';
//                return new BloggsTtdEncoder();
        }
    }

    function getFooterText()
    {
        return "BloggsCal footer\n";
    }
}