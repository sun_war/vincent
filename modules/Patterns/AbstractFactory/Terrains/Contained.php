<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 11:44 AM
 */

namespace Patterns\AbstractFactory\Terrains;


class Contained
{
    public $contained;
    function __construct() {
        $this->contained = new Contained();
    }
    function __clone() {
        // Ensure that cloned object holds a
        // clone of self::$contained and not
        // a reference to it
        $this->contained = clone $this->contained;
    }
}