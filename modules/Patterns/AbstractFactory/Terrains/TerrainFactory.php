<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 11:38 AM
 */

namespace Patterns\AbstractFactory\Terrains;


use Patterns\AbstractFactory\Terrains\Landing\Forest;
use Patterns\AbstractFactory\Terrains\Landing\Plains;
use Patterns\AbstractFactory\Terrains\Component\Sea;

class TerrainFactory
{
    private $sea;
    private $forest;
    private $plains;

    function __construct(Sea $sea, Plains $plains, Forest $forest)
    {
        $this->sea = $sea;
        $this->plains = $plains;
        $this->forest = $forest;
    }

    function getSea()
    {
        return clone $this->sea;
    }

    function getPlains()
    {
        return clone $this->plains;
    }

    function getForest()
    {
        return clone $this->forest;
    }
}