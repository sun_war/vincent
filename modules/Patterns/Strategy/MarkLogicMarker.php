<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/7/18
 * Time: 2:24 PM
 */

namespace Patterns\Strategy;


class MarkLogicMarker extends Marker
{
    private $engine;

    function __construct($test)
    {
        parent::__construct($test);
        // $this->engine = new MarkParse( $test );
    }

    function mark($response)
    {
        // return $this->engine->evaluate( $response );
        // dummy return value
        return true;
    }
}