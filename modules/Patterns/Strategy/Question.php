<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/7/18
 * Time: 2:22 PM
 */

namespace Patterns\Strategy;


abstract class Question
{
    protected $prompt;
    protected $marker;

    function __construct($prompt, Marker $marker)
    {
        $this->marker = $marker;
        $this->prompt = $prompt;
    }

    function mark($response)
    {
        return $this->marker->mark($response);
    }
}