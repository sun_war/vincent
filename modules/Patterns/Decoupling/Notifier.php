<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 11/23/18
 * Time: 10:16 AM
 */

namespace Patterns\Decoupling;


abstract class Notifier
{
    static function getNotifier()
    {
        // acquire concrete class according to
        // configuration or other logic
        if (rand(1, 2) === 1) {
            return new MailNotifier();
        } else {
            return new TextNotifier();
        }
    }

    abstract function inform($message);
}