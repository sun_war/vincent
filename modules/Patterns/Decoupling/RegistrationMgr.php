<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 11/23/18
 * Time: 10:14 AM
 */

namespace Patterns\Decoupling;


use Patterns\Composition\Lesson;

class RegistrationMgr
{
    function register(Lesson $lesson)
    {
        // do something with this Lesson
        // now tell someone
        $notifier = Notifier::getNotifier();
        $notifier->inform("new lesson: cost ({$lesson->cost()}) <br/>");
    }
}