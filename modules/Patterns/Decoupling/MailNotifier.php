<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 11/23/18
 * Time: 10:17 AM
 */

namespace Patterns\Decoupling;


class MailNotifier extends Notifier
{
    function inform( $message ) {
        print "MAIL notification: {$message}\n";
    }
}