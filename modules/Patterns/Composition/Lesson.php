<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 11/22/18
 * Time: 2:35 PM
 */

namespace Patterns\Composition;


abstract class Lesson
{
    private $duration;
    private $costStrategy;

    function __construct($duration, CostStrategy $strategy)
    {
        $this->duration = $duration;
        $this->costStrategy = $strategy;
    }

    function cost()
    {
        return $this->costStrategy->cost($this);
    }

    function chargeType()
    {
        return $this->costStrategy->chargeType();
    }

    function getDuration()
    {
        return $this->duration;
    }

}