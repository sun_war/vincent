<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 9:35 AM
 */

namespace Patterns\Composition\Civilization;


class Archer extends Unit
{
    function bombardStrength() {
        return 4;
    }
}