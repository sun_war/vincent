<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 10:49 AM
 */

namespace Patterns\Composition\Civilization;


class TroopCarrier
{
    function addUnit(Unit $unit)
    {
        if ($unit instanceof Cavalry) {
            throw new UnitException("Can't get a horse on the vehicle");
        }
        super::addUnit($unit);
    }

    function bombardStrength()
    {
        return 0;
    }
}