<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 9:34 AM
 */

namespace Patterns\Composition\Civilization;


abstract class Unit
{
    function getComposite() {
        return null;
    }
    function addUnit( Unit $unit ) {
        throw new UnitException( get_class($this)." is a leaf" );
    }
    function removeUnit( Unit $unit ) {
        throw new UnitException( get_class($this)." is a leaf" );
    }
    abstract function bombardStrength();
}