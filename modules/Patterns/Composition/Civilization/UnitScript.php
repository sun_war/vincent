<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 10:47 AM
 */

namespace Patterns\Composition\Civilization;


class UnitScript
{
    static function joinExisting(Unit $newUnit,
                                 Unit $occupyingUnit)
    {
        $comp = '';
        if (!is_null($comp = $occupyingUnit->getComposite())) {
            $comp->addUnit($newUnit);
        } else {
            $comp = new Army();
            $comp->addUnit($occupyingUnit);
            $comp->addUnit($newUnit);
        }
        return $comp;
    }
}