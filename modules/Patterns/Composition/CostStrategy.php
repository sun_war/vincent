<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 11/22/18
 * Time: 3:00 PM
 */

namespace Patterns\Composition;


abstract class CostStrategy
{
    abstract function cost(Lesson $lesson);

    abstract function chargeType();
}