<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 11/22/18
 * Time: 3:26 PM
 */

namespace Patterns\Composition;


class TimedCostStrategy extends CostStrategy
{
    function cost(Lesson $lesson)
    {
        return ($lesson->getDuration() * 500);
    }

    function chargeType()
    {
        return "hourly rate";
    }
}