<table class="table table-hover">
    <thead>
    <tr>
        
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($patterns as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        
        <td class="text-right">
            <form method="POST" action="{{route('patterns.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('patterns.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('patterns.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$patterns->links()}}
</div>
