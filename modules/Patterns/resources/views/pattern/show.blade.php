@extends('layouts.app')
@section('content')
<ol class="breadcrumb">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('patterns.index')}}">{{trans('table.patterns')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#Menu', '#patternMenu')
</script>
@endpush
