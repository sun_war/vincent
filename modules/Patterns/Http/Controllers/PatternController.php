<?php

namespace Patterns\Http\Controllers;

use App\Http\Controllers\Controller;
use Patterns\Http\Requests\PatternCreateRequest;
use Patterns\Http\Requests\PatternUpdateRequest;
use Patterns\Repositories\PatternRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class PatternController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(PatternRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['patterns'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('pat::pattern.table', $data)->render();
        }
        return view('pat::pattern.index', $data);
    }

    public function create()
    {
        return view('pat::pattern.create');
    }

    public function store(PatternCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('patterns.index');
    }

    public function show($id)
    {
        $pattern = $this->repository->find($id);
        if (empty($pattern)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('pat::pattern.show', compact('pattern'));
    }

    public function edit($id)
    {
        $pattern = $this->repository->find($id);
        if (empty($pattern)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('pat::pattern.update', compact('pattern'));
    }

    public function update(PatternUpdateRequest $request, $id)
    {
        $input = $request->all();
        $pattern = $this->repository->find($id);
        if (empty($pattern)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $pattern);
        session()->flash('success', 'update success');
        return redirect()->route('patterns.index');
    }

    public function destroy($id)
    {
        $pattern = $this->repository->find($id);
        if (empty($pattern)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
