<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 1:51 PM
 */

namespace Patterns\Decorator\Auth;


abstract class DecorateProcess extends ProcessRequest
{
    protected $processrequest;
    function __construct( ProcessRequest $pr ) {
        $this->processrequest = $pr;
    }
}