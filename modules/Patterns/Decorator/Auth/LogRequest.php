<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 1:52 PM
 */

namespace Patterns\Decorator\Auth;


class LogRequest extends DecorateProcess
{
    function process(RequestHelper $req)
    {
        print __CLASS__ . ": logging request<br/>";
        $this->processrequest->process($req);
    }
}