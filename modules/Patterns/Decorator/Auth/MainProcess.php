<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 11:57 AM
 */

namespace Patterns\Decorator\Auth;


class MainProcess extends ProcessRequest
{
    function process(RequestHelper $req)
    {
        print __CLASS__ . ": doing something useful with request\n";
    }
}