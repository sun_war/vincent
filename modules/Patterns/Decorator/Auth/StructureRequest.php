<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 1:54 PM
 */

namespace Patterns\Decorator\Auth;


class StructureRequest extends DecorateProcess
{
    function process(RequestHelper $req)
    {
        print __CLASS__ . ": structuring request data<br/>";
        $this->processrequest->process($req);
    }
}