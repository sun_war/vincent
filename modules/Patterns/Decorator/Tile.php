<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 10:57 AM
 */

namespace Patterns\Decorator;


abstract class Tile
{
    abstract function getWealthFactor();
}