<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 11:12 AM
 */

namespace Patterns\Decorator;


abstract class TileDecorator extends Tile
{
    protected $tile;
    function __construct( Tile $tile ) {
        $this->tile = $tile;
    }
}