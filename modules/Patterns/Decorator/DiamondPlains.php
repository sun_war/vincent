<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 10:59 AM
 */

namespace Patterns\Decorator;


class DiamondPlains extends Plains
{
    function getWealthFactor() {
        return parent::getWealthFactor() + 2;
    }
}