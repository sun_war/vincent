<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.email')}}</th>
<th>{{trans('label.name')}}</th>
<th>{{trans('label.phone_number')}}</th>
<th>{{trans('label.facebook')}}</th>
<th>{{trans('label.website')}}</th>
<th>{{trans('label.image')}}</th>
<th>{{trans('label.slogan')}}</th>
<th>{{trans('label.city')}}</th>
<th>{{trans('label.address')}}</th>
<th>{{trans('label.status')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($studios as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->email}}</td>
<td>{{$row->name}}</td>
<td>{{$row->phone_number}}</td>
<td>{{$row->facebook}}</td>
<td>{{$row->website}}</td>
<td>{{$row->image}}</td>
<td>{{$row->slogan}}</td>
<td>{{$row->city}}</td>
<td>{{$row->address}}</td>
<td>{{$row->status}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('studios.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('studios.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('studios.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$studios->links()}}
</div>
