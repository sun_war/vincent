@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('studio-rates.index')}}">{{trans('table.studio_rates')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.studio_id')}}</th>
<td>{!! $studio_rate->studio_id !!}</td>
</tr><tr><th>{{__('label.created_by')}}</th>
<td>{!! $studio_rate->created_by !!}</td>
</tr><tr><th>{{__('label.start')}}</th>
<td>{!! $studio_rate->start !!}</td>
</tr><tr><th>{{__('label.description')}}</th>
<td>{!! $studio_rate->description !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#StudioMenu', '#studioRateMenu')
</script>
@endpush
