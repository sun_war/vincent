@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('studio-reports.index')}}">{{trans('table.studio_reports')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.update')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('studio-reports.update', $studioReport->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-12">
    <label for="studio_id">{{trans('label.studio_id')}}</label>
    <input required type="number" class="form-control" name="studio_id" id="studio_id" value="{{$studioReport->studio_id}}">
</div>
<div class="form-group col-lg-12">
    <label for="created_by">{{trans('label.created_by')}}</label>
    <input required type="number" class="form-control" name="created_by" id="created_by" value="{{$studioReport->created_by}}">
</div>
<div class="form-group col-lg-12">
    <label for="content">{{trans('label.content')}}</label>
    <textarea class="form-control" name="content" id="content">{{$studioReport->content}}</textarea>
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        Menu('#StudioMenu', '#studioReportMenu')
    </script>
@endpush
