<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.studio_id')}}</th>
<th>{{trans('label.created_by')}}</th>
<th>{{trans('label.content')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($studioReports as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->studio_id}}</td>
<td>{{$row->created_by}}</td>
<td>{{$row->content}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('studio-reports.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('studio-reports.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('studio-reports.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$studioReports->links()}}
</div>
