@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('studio-follows.index')}}">{{trans('table.studio_follows')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.studio_id')}}</th>
<td>{!! $studio_follow->studio_id !!}</td>
</tr><tr><th>{{__('label.created_by')}}</th>
<td>{!! $studio_follow->created_by !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#StudioMenu', '#studioFollowMenu')
</script>
@endpush
