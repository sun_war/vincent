<li class="has-sub root-level" id="StudioMenu">
    <a>
        <i class="fa fa-file"></i>
        <span class="title">{{__('menu.studio')}}</span>
    </a>
    <ul>
        <li id="studioFollowsMenu">
            <a href="{{route('studio-follows.index')}}">
                <span class="title">{{__('table.studio_follows')}}</span>
            </a>
        </li>
        <li id="studioRatesMenu">
            <a href="{{route('studio-rates.index')}}">
                <span class="title">{{__('table.studio_rates')}}</span>
            </a>
        </li>
        <li id="studioReportsMenu">
            <a href="{{route('studio-reports.index')}}">
                <span class="title">{{__('table.studio_reports')}}</span>
            </a>
        </li>
        <li id="studiosMenu">
            <a href="{{route('studios.index')}}">
                <span class="title">{{__('table.studios')}}</span>
            </a>
        </li>
    </ul>
</li>