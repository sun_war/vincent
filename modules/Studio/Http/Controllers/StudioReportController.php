<?php

namespace Studio\Http\Controllers;

use App\Http\Controllers\Controller;
use Studio\Http\Requests\StudioReportCreateRequest;
use Studio\Http\Requests\StudioReportUpdateRequest;
use Studio\Http\Repositories\StudioReportRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class StudioReportController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(StudioReportRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['studioReports'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('studio::studio-report.table', $data)->render();
        }
        return view('studio::studio-report.index', $data);
    }

    public function create()
    {
        return view('studio::studio-report.create');
    }

    public function store(StudioReportCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('studio-reports.index');
    }

    public function show($id)
    {
        $studioReport = $this->repository->find($id);
        if (empty($studioReport)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('studio::studio-report.show', compact('studioReport'));
    }

    public function edit($id)
    {
        $studioReport = $this->repository->find($id);
        if (empty($studioReport)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('studio::studio-report.update', compact('studioReport'));
    }

    public function update(StudioReportUpdateRequest $request, $id)
    {
        $input = $request->all();
        $studioReport = $this->repository->find($id);
        if (empty($studioReport)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $studioReport);
        session()->flash('success', 'update success');
        return redirect()->route('studio-reports.index');
    }

    public function destroy($id)
    {
        $studioReport = $this->repository->find($id);
        if (empty($studioReport)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
