<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Studio\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Studio\Http\Requests\StudioFollowCreateRequest;
use Studio\Http\Requests\StudioFollowUpdateRequest;
use Studio\Http\Resources\StudioFollowResource;
use Studio\Http\Repositories\StudioFollowRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class StudioFollowApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(StudioFollowRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new StudioFollowResource($data);
    }

    public function create()
    {
        return view('studio::studio-follow.create');
    }

    public function store(StudioFollowCreateRequest $request)
    {
        $input = $request->all();
        $studioFollow = $this->repository->store($input);
        return new StudioFollowResource($studioFollow);
    }

    public function show($id)
    {
        $studioFollow = $this->repository->find($id);
        if (empty($studioFollow)) {
            return new StudioFollowResource([$studioFollow]);
        }
        return new StudioFollowResource($studioFollow);
    }

    public function edit($id)
    {
        $studioFollow = $this->repository->find($id);
        if (empty($studioFollow)) {
            return new StudioFollowResource([$studioFollow]);
        }
        return new StudioFollowResource($studioFollow);
    }

    public function update(StudioFollowUpdateRequest $request, $id)
    {
        $input = $request->all();
        $studioFollow = $this->repository->find($id);
        if (empty($studioFollow)) {
            return new StudioFollowResource([$studioFollow]);
        }
        $data = $this->repository->change($input, $studioFollow);
        return new StudioFollowResource($data);
    }

    public function destroy($id)
    {
        $studioFollow = $this->repository->find($id);
        if (empty($studioFollow)) {
            return new StudioFollowResource($studioFollow);
        }
        $data = $this->repository->delete($id);
        return new StudioFollowResource([$data]);
    }
}