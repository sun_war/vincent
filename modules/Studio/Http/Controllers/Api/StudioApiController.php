<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Studio\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Studio\Http\Requests\StudioCreateRequest;
use Studio\Http\Requests\StudioUpdateRequest;
use Studio\Http\Resources\StudioResource;
use Studio\Http\Repositories\StudioRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class StudioApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(StudioRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new StudioResource($data);
    }

    public function create()
    {
        return view('studio::studio.create');
    }

    public function store(StudioCreateRequest $request)
    {
        $input = $request->all();
        $studio = $this->repository->store($input);
        return new StudioResource($studio);
    }

    public function show($id)
    {
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            return new StudioResource([$studio]);
        }
        return new StudioResource($studio);
    }

    public function edit($id)
    {
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            return new StudioResource([$studio]);
        }
        return new StudioResource($studio);
    }

    public function update(StudioUpdateRequest $request, $id)
    {
        $input = $request->all();
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            return new StudioResource([$studio]);
        }
        $data = $this->repository->change($input, $studio);
        return new StudioResource($data);
    }

    public function destroy($id)
    {
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            return new StudioResource($studio);
        }
        $data = $this->repository->delete($id);
        return new StudioResource([$data]);
    }
}