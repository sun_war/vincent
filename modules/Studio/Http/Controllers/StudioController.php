<?php

namespace Studio\Http\Controllers;

use App\Http\Controllers\Controller;
use Studio\Http\Requests\StudioCreateRequest;
use Studio\Http\Requests\StudioUpdateRequest;
use Studio\Http\Repositories\StudioRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class StudioController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(StudioRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['studios'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('studio::studio.table', $data)->render();
        }
        return view('studio::studio.index', $data);
    }

    public function create()
    {
        return view('studio::studio.create');
    }

    public function store(StudioCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('studios.index');
    }

    public function show($id)
    {
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('studio::studio.show', compact('studio'));
    }

    public function edit($id)
    {
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('studio::studio.update', compact('studio'));
    }

    public function update(StudioUpdateRequest $request, $id)
    {
        $input = $request->all();
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $studio);
        session()->flash('success', 'update success');
        return redirect()->route('studios.index');
    }

    public function destroy($id)
    {
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
