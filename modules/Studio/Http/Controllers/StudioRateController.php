<?php

namespace Studio\Http\Controllers;

use App\Http\Controllers\Controller;
use Studio\Http\Requests\StudioRateCreateRequest;
use Studio\Http\Requests\StudioRateUpdateRequest;
use Studio\Http\Repositories\StudioRateRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class StudioRateController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(StudioRateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['studioRates'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('studio::studio-rate.table', $data)->render();
        }
        return view('studio::studio-rate.index', $data);
    }

    public function create()
    {
        return view('studio::studio-rate.create');
    }

    public function store(StudioRateCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('studio-rates.index');
    }

    public function show($id)
    {
        $studioRate = $this->repository->find($id);
        if (empty($studioRate)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('studio::studio-rate.show', compact('studioRate'));
    }

    public function edit($id)
    {
        $studioRate = $this->repository->find($id);
        if (empty($studioRate)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('studio::studio-rate.update', compact('studioRate'));
    }

    public function update(StudioRateUpdateRequest $request, $id)
    {
        $input = $request->all();
        $studioRate = $this->repository->find($id);
        if (empty($studioRate)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $studioRate);
        session()->flash('success', 'update success');
        return redirect()->route('studio-rates.index');
    }

    public function destroy($id)
    {
        $studioRate = $this->repository->find($id);
        if (empty($studioRate)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
