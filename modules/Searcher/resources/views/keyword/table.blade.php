<table class="table">
    <thead>
    <tr>
        <th>{{trans('label.keyword')}}</th>
{{--        <th>{{trans('label.description')}}</th>--}}
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($keywords as $row)
        <tr>
            <td>{{$row->keyword}}</td>
{{--            <td>{{$row->description}}</td>--}}
            <td class="text-right">
                <form method="POST" action="{{route('keywords.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('keywords.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('keywords.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$keywords->links()}}
</div>
