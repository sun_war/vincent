@extends('layouts.app')
@section('content')
<ol class="breadcrumb">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('keywords.index')}}">{{trans('table.keywords')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.keyword')}}</th>
<td>{!! $keyword->keyword !!}</td>
</tr><tr><th>{{__('label.description')}}</th>
<td>{!! $keyword->description !!}</td>
</tr>
        </tbody>
    </table>
</div>

@endsection