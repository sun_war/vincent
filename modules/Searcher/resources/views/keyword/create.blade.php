@extends('layouts.app')
@section('content')
    <ol class="breadcrumb">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('keywords.index')}}">{{trans('table.keywords')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.create')}}</strong>
        </li>
    </ol>
    <div class="row">
        <form action="{{route('keywords.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group col-lg-12">
                <label for="keyword">{{trans('label.keyword')}}</label>
                <input required class="form-control" name="keyword" id="keyword">
            </div>

            <div class="form-group col-lg-12">
                <label for="description">{{trans('label.description')}}</label>
                <textarea class="form-control" name="description" id="description"></textarea>
            </div>

            <div class="form-group col-lg-12">
                <label for="description">{{trans('label.description')}}</label>
                <select class="form-control select2" name="tags[]" id="description" multiple>

                </select>
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection

@push('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet"/>
    <style>
        .select2 {
            visibility: visible;
        }
    </style>
@endpush

@push('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2({
                placeholder: 'This is my placeholder',
                allowClear: true,
                tags: true
            });
        });
    </script>
@endpush