@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('keyword-tags.index')}}">{{trans('table.keyword_tags')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.keyword_id')}}</th>
<td>{!! $keyword_tag->keyword_id !!}</td>
</tr><tr><th>{{__('label.tag_id')}}</th>
<td>{!! $keyword_tag->tag_id !!}</td>
</tr>
        </tbody>
    </table>
</div>

@endsection