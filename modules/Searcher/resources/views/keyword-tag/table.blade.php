<table class="table">
    <thead>
    <tr>
        <th>{{trans('label.keyword_id')}}</th>
<th>{{trans('label.tag_id')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($keywordTags as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->keyword_id}}</td>
<td>{{$row->tag_id}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('keyword-tags.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('keyword-tags.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('keyword-tags.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$keywordTags->links()}}
</div>
