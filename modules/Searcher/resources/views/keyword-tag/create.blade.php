@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('keyword-tags.index')}}">{{trans('table.keyword_tags')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.create')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('keyword-tags.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group col-lg-6">
    <label for="keyword_id">{{trans('label.keyword_id')}}</label>
    <input type="number" required class="form-control" name="keyword_id" id="keyword_id">
</div>
<div class="form-group col-lg-6">
    <label for="tag_id">{{trans('label.tag_id')}}</label>
    <input type="number" required class="form-control" name="tag_id" id="tag_id">
</div>

        <div class="col-lg-12">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection