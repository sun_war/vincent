<li class="has-sub root-level" id="searchMenu">
    <a>
        <i class="fa fa-search-plus"></i>
        <span class="title">{{__('menu.keyword')}}</span>
    </a>
    <ul>
        <li  id="pronunciationMenu">
            <a href="{{route('keywords.index')}}">
                <span class="title">{{__('table.keywords')}}</span>
            </a>
        </li>
    </ul>
</li>