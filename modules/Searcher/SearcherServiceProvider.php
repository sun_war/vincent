<?php

namespace Searcher\Providers;

use Illuminate\Support\ServiceProvider;
use Searcher\Repositories\KeywordRepository;
use Searcher\Repositories\KeywordRepositoryEloquent;
use Searcher\Repositories\KeywordTagRepository;
use Searcher\Repositories\KeywordTagRepositoryEloquent;

class SearcherServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/router.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'se');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(KeywordRepository::class, KeywordRepositoryEloquent::class);
        $this->app->bind(KeywordTagRepository::class, KeywordTagRepositoryEloquent::class);
    }
}
