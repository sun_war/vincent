<?php

namespace Searcher\Http\Controllers;

use App\Http\Controllers\Controller;
use Searcher\Http\Requests\KeywordTagCreateRequest;
use Searcher\Http\Requests\KeywordTagUpdateRequest;
use Searcher\Repositories\KeywordTagRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class KeywordTagController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(KeywordTagRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['keywordTags'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('se::keyword-tag.table', $data)->render();
        }
        return view('se::keyword-tag.index', $data);
    }

    public function create()
    {
        return view('se::keyword-tag.create');
    }

    public function store(KeywordTagCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('keyword-tags.index');
    }

    public function show($id)
    {
        $keywordTag = $this->repository->find($id);
        if (empty($keywordTag)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('se::keyword-tag.show', compact('keywordTag'));
    }

    public function edit($id)
    {
        $keywordTag = $this->repository->find($id);
        if (empty($keywordTag)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('se::keyword-tag.update', compact('keywordTag'));
    }

    public function update(KeywordTagUpdateRequest $request, $id)
    {
        $input = $request->all();
        $keywordTag = $this->repository->find($id);
        if (empty($keywordTag)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $keywordTag);
        session()->flash('success', 'update success');
        return redirect()->route('keyword-tags.index');
    }

    public function destroy($id)
    {
        $keywordTag = $this->repository->find($id);
        if (empty($keywordTag)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
