<?php

namespace Searcher\Http\Controllers;

use App\Http\Controllers\Controller;
use Searcher\Http\Requests\KeywordCreateRequest;
use Searcher\Http\Requests\KeywordUpdateRequest;
use Searcher\Repositories\KeywordRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class KeywordController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(KeywordRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['keywords'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('se::keyword.table', $data)->render();
        }
        return view('se::keyword.index', $data);
    }

    public function create()
    {
        return view('se::keyword.create');
    }

    public function store(KeywordCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('keywords.index');
    }

    public function show($id)
    {
        $keyword = $this->repository->find($id);
        if (empty($keyword)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('se::keyword.show', compact('keyword'));
    }

    public function edit($id)
    {
        $keyword = $this->repository->find($id);
        if (empty($keyword)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('se::keyword.update', compact('keyword'));
    }

    public function update(KeywordUpdateRequest $request, $id)
    {
        $input = $request->all();
        $keyword = $this->repository->find($id);
        if (empty($keyword)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $keyword);
        session()->flash('success', 'update success');
        return redirect()->route('keywords.index');
    }

    public function destroy($id)
    {
        $keyword = $this->repository->find($id);
        if (empty($keyword)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
