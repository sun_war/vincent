<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:06 AM
 */

Route::middleware(['web'])
    ->namespace('Searcher\Http\Controllers')
    ->prefix('search')
    ->group(function () {
        Route::resources([
            'keywords' => 'KeywordController',
            'keyword-tags' => 'KeywordTagController'
        ]);
    });
