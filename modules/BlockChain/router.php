<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/6/18
 * Time: 9:48 AM
 */

Route::group(
    [
        'namespace' => 'BlockChain\Http\Controllers',
        'middleware' => ['web'],
    ],
    function () {
        Route::post('bc-login', 'Auth\LoginController@login')->name('eth.login');
        Route::post('bc-logout', 'Auth\LoginController@logout')->name('eth.logout');
    });

Route::group(
    [
        'namespace' => 'BlockChain\Http\Controllers',
        'middleware' => ['web', 'auth'],
    ],
    function () {
        Route::get('bc', 'WalletController@index')->name('wallet');
        Route::post('bc-key', 'WalletController@getKey')->name('eth.key');

        Route::post('bc-register', 'Auth\RegisterController@register')->name('eth.register');
    });

Route::group(
    [
        'namespace' => 'BlockChain\Http\Controllers',
        'middleware' => ['web', 'auth:admin'],
    ],
    function () {
        Route::resource('accounts', 'AccountController');
        Route::resource('cryptos', 'CryptoController');
    });

//Route::group(
//    [
//        'namespace' => 'BlockChain\Http\Controllers\API',
//        'middleware' => ['api'],
//        'prefix' => 'api'
//    ],
//    function () {
//        Route::resource('accounts', 'AccountApiController');
//
//        Route::get('web3-client-version', 'Web3Controller@clientVersion')->name('api.web3.client.version');
//        Route::get('web3-sha3', 'Web3Controller@sha3')->name('api.web3.sha3');
//
//        Route::get('net-version', 'NetController@getVersion')->name('api.net.version');
//        Route::get('net-listening', 'NetController@listen')->name('api.net.listen');
//        Route::get('net-peer-count', 'NetController@countPeer')->name('api.net.count.peer');
//
//        Route::get('eth-protocol-version', 'EthController@getProtocolVersion')->name('api.eth.protocol.version');
//        Route::get('eth-coin-base', 'EthController@getCoinBase')->name('api.eth.coin.base');
//        Route::get('eth-mining', 'EthController@getMining')->name('api.eth.mining');
//        Route::get('eth-gas-price', 'EthController@getGasPrice')->name('api.eth.gas.price');
//        Route::get('eth-accounts', 'EthController@getAccounts')->name('api.eth.account');
//        Route::get('eth-block-number', 'EthController@getBlockNumber')->name('api.eth.block.number');
//        Route::get('eth-balance', 'EthController@getBalance')->name('api.eth.balance');
//        Route::get('eth-storage-at', 'EthController@getStorageAt')->name('api.eth.storage.at');
//        Route::get('eth-transaction-count', 'EthController@getTransactionCount')->name('api.eth.transaction.count');
//        Route::get('eth-block-transaction-count-by-hash', 'EthController@getBlockTransactionCountByHash')
//            ->name('api.eth.block.transaction.count.by.hash');
//        Route::get('eth-block-transaction-count-by-number', 'EthController@getBlockTransactionCountByNumber')
//            ->name('api.eth.block.transaction.count.by.number');
//        Route::get('eth-code', 'EthController@getCode')
//            ->name('api.eth.code');
//        Route::get('eth-send-transaction', 'EthController@sendTransaction')
//            ->name('api.send.transaction');
//        Route::get('eth-call', 'EthController@call')
//            ->name('api.call');
//        Route::get('eth-get-block-by-hash', 'EthController@getBlockByHash')
//            ->name('api.get.block.by.hash');
//        Route::get('eth-get-block-by-number', 'EthController@getBlockByNumber')
//            ->name('api.get.block.by.number');
//        Route::get('eth-get-transaction-by-hash', 'EthController@getTransactionByHash')
//            ->name('api.get.transaction-by-hash');
//        Route::get('eth-get-transaction-by-block-and-index', 'EthController@getTransactionByBlockAndIndex')
//            ->name('api.get.transaction-by-block-and-index');
//        Route::get('eth-get-transaction-by-block-number-and-index', 'EthController@getTransactionByBlockNumberAndIndex')
//            ->name('api.get.transaction-by-block-number-and-index');
//        Route::get('eth-get-uncle-by-block-hash-and-index', 'EthController@getUncleByBlockHashAndIndex')
//            ->name('api.get.uncle.by.block.hash.and.index');
//        Route::get('eth-get-uncle-by-block-number-and-index', 'EthController@getUncleByBlockNumberAndIndex')
//            ->name('api.get.uncle.by.block.number.and.index');
//        Route::get('eth-get-compile', 'EthController@getCompilers')
//            ->name('api.get.comple');
//        Route::get('eth-get-compile-solidity', 'EthController@getCompileSolidity')
//            ->name('api.get.compile-solidity');
//        Route::get('eth-compile-lll', 'EthController@compileLLL')
//            ->name('api.compile-lll');
//        Route::get('eth-compile-serpent', 'EthController@compileSerpent')
//            ->name('api.compile-serpent');
//        Route::get('eth-new-filter', 'EthController@newFilter')
//            ->name('api.new-filter');
//        Route::get('eth-new-block-filter', 'EthController@newBlockFilter')
//            ->name('api.new-block-filter');
//        Route::get('eth-get-filter-change', 'EthController@getFilterChanges')
//            ->name('api-get-filter-change');
//        Route::get('eth-get-filter-log', 'EthController@getFilterLogs')
//            ->name('api-get-filter-log');
//        Route::get('eth-get-filter-log', 'EthController@getFilterLogs')
//            ->name('api.get.filter.log');
//        Route::get('eth-get-work', 'EthController@getWork')
//            ->name('api.get.work');
//        Route::get('eth-submit-work', 'EthController@submitWork')
//            ->name('api.submit.work');
//
//        Route::get('db-put-string', 'DbController@putString')
//            ->name('api.db.put.string');
//        Route::get('db-put-hex', 'DbController@putHex')
//            ->name('api.db.put.hex');
//        Route::get('db-get-hex', 'DbController@getHex')
//            ->name('api.db.get.hex');
//
//        Route::get('ssh-version', 'ShhController@version')
//            ->name('api.ssh.version');
//        Route::get('ssh-post', 'ShhController@post')
//            ->name('api.ssh.post');
//        Route::get('ssh-new-identity', 'ShhController@newIdentinty')
//            ->name('api.ssh.new.identity');
//        Route::get('ssh-has-identity', 'ShhController@hasIdentity')
//            ->name('api.ssh.has.identity');
//        Route::get('ssh-new-group', 'ShhController@newGroup')
//            ->name('api.ssh.new.group');
//        Route::get('ssh-add-group', 'ShhController@addToGroup')
//            ->name('api.ssh.add.group');
//        Route::get('ssh-new-filter', 'ShhController@newFilter')
//            ->name('api.ssh.new.filter');
//        Route::get('ssh-uninstall-filter', 'ShhController@uninstallFilter')
//            ->name('api.ssh.uninstall.filter');
//        Route::get('ssh-filter-change', 'ShhController@getFilterChanges')
//            ->name('api.ssh.filter.change');
//
//        Route::get('get-transactions', 'AccountController@getTransactions')
//            ->name('api.get.transaction');
//        Route::get('get-balance', 'AccountController@getBalance')
//            ->name('api.get.balance');
//        Route::get('get-list', 'AccountController@getList')
//            ->name('api.get.list');
//        Route::get('get-price', 'AccountController@getPrice')
//            ->name('api.get.price');
//    }
//);