<?php

namespace BlockChain\Http\Controllers;

use App\Http\Controllers\Controller;
use BlockChain\Http\Requests\AccountCreateRequest;
use BlockChain\Http\Requests\AccountUpdateRequest;
use BlockChain\Http\Repositories\AccountRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class AccountController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(AccountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['accounts'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('bc::account.table', $data)->render();
        }
        return view('bc::account.index', $data);
    }

    public function create()
    {
        return view('bc::account.create');
    }

    public function store(AccountCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('accounts.index');
    }

    public function show($id)
    {
        $account = $this->repository->find($id);
        if (empty($account)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('bc::account.show', compact('account'));
    }

    public function edit($id)
    {
        $account = $this->repository->find($id);
        if (empty($account)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('bc::account.update', compact('account'));
    }

    public function update(AccountUpdateRequest $request, $id)
    {
        $input = $request->all();
        $account = $this->repository->find($id);
        if (empty($account)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $account);
        session()->flash('success', 'update success');
        return redirect()->route('accounts.index');
    }

    public function destroy($id)
    {
        $account = $this->repository->find($id);
        if (empty($account)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
