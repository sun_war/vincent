<?php

namespace BlockChain\Http\Controllers\Auth;

use ACL\Models\Admin;
use App\Http\Controllers\Controller;
use BlockChain\Models\Crypto;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    use Authenticatable;

    protected $redirectTo = '/bc';
    protected $afterLogout = '/bc';
    protected $guard = 'eth';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function login(Request $request)
    {
        $credentials = $request->only( 'password');
        $credentials[ADDRESS_COL] = \auth()->user()->account->address;

        if (Auth::guard('eth')->attempt($credentials)) {
            $account = auth('eth')->user();
            $publicKey = Crypto::where('user_id', $account->user_id)->pluck('public_key');
            $keyStore = $account->keystore;
            $data = [
                'public_key' => $publicKey,
                'keystore' => $keyStore
            ];

            return response()->json($data);
        }
    }

    public function username()
    {
        return 'address';
    }

    protected function guard()
    {
        return Auth::guard($this->guard);
    }

    /**
     * @return bool
     */
    protected function isAnyGuardLoggedIn()
    {
        $configGuards = config('auth.guards');
        foreach ($configGuards as $guard => $config) {
            if (Auth::guard($guard)->check()) {
                return true;
            }
        }

        return false;
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return redirect()->intended($this->redirectPath());
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        if (!$this->isAnyGuardLoggedIn()) {
            $request->session()->invalidate();
        }
        return redirect($this->afterLogout);
    }
}