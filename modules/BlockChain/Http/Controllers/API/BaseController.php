<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/6/18
 * Time: 9:56 AM
 */

namespace BlockChain\Http\Controllers\API;


use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class BaseController extends Controller
{
    protected $uri = 'http://127.0.0.1:8545';
    protected $contentType = 'application/json';
    protected $client;
    protected $account = '0x187f9169d474bc354Bf8E5B443DB74518EdB6Fe0';
    protected $account1 = '4c376c10a722b4a2e96cf6afd885e92012858850';
    protected $account2 = '0985346e93b8a4a54aec429e8dad8046d8af53a1';

    public function __construct()
    {
        $this->client = new Client([
            'base_url' => [$this->uri],
            'defaults' => [
                'headers' => [
                    'content-type' => $this->contentType,
                    'Accept' => $this->contentType
                ],
            ],
        ]);
    }

    protected function send($data)
    {
        $res = $this->client->post($this->uri, [
            RequestOptions::JSON => $data
        ]);
        $body = json_decode($res->getBody(), true);
        return response()->json($body, $res->getStatusCode());
    }
}