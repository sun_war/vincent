<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/6/18
 * Time: 9:54 AM
 */

namespace BlockChain\Http\Controllers\API;


use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class Web3Controller extends BaseController
{
    public function clientVersion()
    {
        $data = [
            "jsonrpc" => "2.0",
            "method" => "web3_clientVersion",
            "params" => ['0x68656c6c6f20776f726c64'],
            "id" => 2
        ];

        $res = $this->client->post($this->uri, [
            RequestOptions::JSON => $data
        ]);
        $body = json_decode($res->getBody(), true);
        return response()->json($body, $res->getStatusCode());
    }

    public function sha3()
    {
        $data = [
            "jsonrpc" => "2.0",
            "method" => "web3_sha3",
            "params" => ['0x68656c6c6f20776f726c64'],
            "id" => 64
        ];

        $res = $this->client->post($this->uri, [
            RequestOptions::JSON => $data
        ]);
        $body = json_decode($res->getBody(), true);
        return response()->json($body, $res->getStatusCode());
    }

}