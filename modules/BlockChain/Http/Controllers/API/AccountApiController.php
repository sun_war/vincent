<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace BlockChain\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use BlockChain\Http\Requests\AccountCreateRequest;
use BlockChain\Http\Requests\AccountUpdateRequest;
use BlockChain\Http\Resources\AccountResource;
use BlockChain\Http\Repositories\AccountRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class AccountApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(AccountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new AccountResource($data);
    }

    public function create()
    {
        return view('bc::account.create');
    }

    public function store(AccountCreateRequest $request)
    {
        $input = $request->all();
        $account = $this->repository->store($input);
        return new AccountResource($account);
    }

    public function show($id)
    {
        $account = $this->repository->find($id);
        if (empty($account)) {
            return new AccountResource([$account]);
        }
        return new AccountResource($account);
    }

    public function edit($id)
    {
        $account = $this->repository->find($id);
        if (empty($account)) {
            return new AccountResource([$account]);
        }
        return new AccountResource($account);
    }

    public function update(AccountUpdateRequest $request, $id)
    {
        $input = $request->all();
        $account = $this->repository->find($id);
        if (empty($account)) {
            return new AccountResource([$account]);
        }
        $data = $this->repository->change($input, $account);
        return new AccountResource($data);
    }

    public function destroy($id)
    {
        $account = $this->repository->find($id);
        if (empty($account)) {
            return new AccountResource($account);
        }
        $data = $this->repository->delete($id);
        return new AccountResource([$data]);
    }
}