<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/6/18
 * Time: 9:55 AM
 */

namespace BlockChain\Http\Controllers\API;


class EthController extends BaseController
{
    public function getProtocolVersion()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_protocolVersion",
            "params" => [],
            "id" => 67
        ];
        return $this->send($data);
    }

    public function getCoinBase()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_coinbase",
            "params" => [],
            "id" => 1
        ];

        return $this->send($data);
    }

    public function getMining()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_mining",
            "params" => [],
            "id" => 1
        ];

        return $this->send($data);
    }

    public function getGasPrice()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_gasPrice",
            "params" => [],
            "id" => 1
        ];

        return $this->send($data);
    }

    public function getAccounts()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_accounts",
            "params" => [],
            "id" => 1
        ];

        return $this->send($data);
    }

    public function getBlockNumber()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_blockNumber",
            "params" => [],
            "id" => 1
        ];

        return $this->send($data);
    }

    public function getBalance()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getBalance",
            "params" => [$this->account, "latest"],
            "id" => 1
        ];

        return $this->send($data);
    }

    public function getStorageAt()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getStorageAt",
            "params" => [$this->account, "0x0", "0x2"],
            "id" => 1
        ];

        return $this->send($data);
    }

    public function getTransactionCount()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getTransactionCount",
            "params" => [$this->account, "latest"],
            "id" => 1
        ];

        return $this->send($data);
    }

    public function getBlockTransactionCountByHash()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getBlockTransactionCountByHash",
            "params" => ["0xb903239f8543d04b5dc1ba6579132b143087c68db1b2168786408fcbce568238"],
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getBlockTransactionCountByNumber()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getBlockTransactionCountByNumber",
            "params" => ["0xe8"],
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getCode()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getCode",
            "params" => [$this->account, "0x2"],
            "id" => 1
        ];
        return $this->send($data);
    }

    public function sendTransaction()
    {
        $params = [
            "from" => $this->account,
            "to" => $this->account,
            "gas" => "0x76c0", // 30400,
            "gasPrice" => "0x9184e72a000", // 10000000000000
            "value" => "0x9184e72a", // 2441406250
            "data" => "0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_sendTransaction",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function call()
    {
        $params = [
            "from" => $this->account,
            "to" => $this->account,
            "gas" => "0x76c0", // 30400,
            "gasPrice" => "0x9184e72a000", // 10000000000000
            "value" => "0x9184e72a", // 2441406250
            "data" => "0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_call",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getBlockByHash()
    {
        $params = [
            '0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331',
            true
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getBlockByHash",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getBlockByNumber()
    {
        $params = [
            '0x1b4', true
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getBlockByNumber",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getTransactionByHash()
    {
        $params = [
            '0xb903239f8543d04b5dc1ba6579132b143087c68db1b2168786408fcbce568238'
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getTransactionByHash",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getTransactionByBlockAndIndex()
    {
        $params = [
            '0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331',
            '0x0'
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getTransactionByHash",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getTransactionByBlockNumberAndIndex()
    {
        $params = [
            '0x29c',
            '0x0'
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getTransactionByBlockNumberAndIndex",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getUncleByBlockHashAndIndex()
    {
        $params = [
            '0xc6ef2fc5426d6ad6fd9e2a26abeab0aa2411b7ab17f30a99d3cb96aed1d1055b',
            '0x0'
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getUncleByBlockHashAndIndex",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getUncleByBlockNumberAndIndex()
    {
        $params = [
            '0x29c',
            '0x0'
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getUncleByBlockNumberAndIndex",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getCompilers()
    {
        $params = [];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getCompilers",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getCompileSolidity()
    {
        $params = [
            "contract test { function multiply(uint a) returns(uint d) {   return a * 7;   } }",
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_compileSolidity",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function compileLLL()
    {
        $params = ["(returnlll (suicide (caller)))",];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_compileSolidity",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function compileSerpent()
    {
        $params = ["/* some serpent */",];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_compileSolidity",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function newFilter()
    {
        $params = [
            "fromBlock" => "0x1",
            "toBlock" => "0x2",
            "address" => "0x8888f1f195afa192cfee860698584c030f4c9db1",
            "topics" => ["0x000000000000000000000000a94f5374fce5edbc8e2a8697c15331677e6ebf0b"]
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_newFilter",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function newBlockFilter()
    {
        $params = [
            "pending"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_newBlockFilter",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function uninstallFilter()
    {
        $params = [
            "0xb"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_newBlockFilter",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getFilterChanges()
    {
        $params = [
            "0x16"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getFilterChanges",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getFilterLogs()
    {
        $params = [
            "0x16"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getFilterLogs",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getLogs()
    {
        $params = [
            "0x16"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getFilterLogs",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getWork()
    {
        $params = [];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_getWork",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function submitWork()
    {
        $params = [
            "0x0000000000000001",
            "0x1234567890abcdef1234567890abcdef",
            "0xD1FE5700000000000000000000000000"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "eth_submitWork",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }
}
