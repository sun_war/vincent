<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/6/18
 * Time: 9:56 AM
 */

namespace BlockChain\Http\Controllers\API;


class DbController extends BaseController
{
    public function putString()
    {
        $params = [
            "testDB",
            "myKey",
            "myString"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "db_putString",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function putHex()
    {
        $params = [
            "testDB",
            "myKey",
            "myString"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "db_putString",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getHex()
    {
        $params = [
            "testDB",
            "myKey",
            "myString"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "db_getHex",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }
}