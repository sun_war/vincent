<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/18/18
 * Time: 9:17 AM
 */

namespace BlockChain\Http\Services;

use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrService
{
    public function render($str)
    {
        return base64_encode(QrCode::format('png')->size(100)->generate($str));
    }
}