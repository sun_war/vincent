<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/17/18
 * Time: 10:06 AM
 */

namespace BlockChain\Http\Services;

use Modularization\Facades\OpensslFun;

class AccountService extends BaseService
{
    public function getLocalCurrency()
    {
        $user = auth()->user();
        return $user->getCurrency();
    }

    public function getTransactions($coin = 'MGC', $address = '0x5c7738b67a3403f349782244e59e776ddb3581c3', $page = 1, $perPage = 20)
    {
        $contractAddress = $this->Erc20Service['tokens'][$coin]['address'];
        $data = [
            'module' => "account",
            "action" => "tokentx",
            "contractaddress" => $contractAddress,
            "address" => $address,
            "sort" => 'DESC',
            "apikey" => 'YourApiKeyToken',
            "page" => $page,
            "offset" => $perPage,
        ];
        return $this->send($data);
    }

    public function getCoinAmount($coin, $address)
    {
        try {
            if ($coin === "ETH") {
                $data = [
                    'module' => "account",
                    'action' => 'balance',
                    'tag' => 'latest',
                    'address' => $address,
                    'apikey' => 'YourApiKeyToken'
                ];
            } else {
                $coin = 'MGC4';
                $contractAddress = $this->Erc20Service['tokens'][$coin]['address'];
                $data = [
                    'module' => "account",
                    "action" => "tokenbalance",
                    "contractaddress" => $contractAddress,
                    "address" => $address,
                    "tag" => 'latest',
                    "apikey" => 'YourApiKeyToken',
                ];
            }
            $result = $this->send($data);
            if ($result['status'] == 1) {
                return $this->convertCoin($result['result']);
            };
            return 0;
        } catch (\Exception $exception) {
            return 0;
        }
    }

    public function getPrice($coin = 'MGC')
    {
        try {
            $result = file_get_contents('https://min-api.cryptocompare.com/data/pricemultifull?fsyms=' . $coin .'&tsyms=USD,JPY,PHP');
            return json_decode($result, true);
        } catch (\Exception $ex) {
            return response()->json([
                'message' => $ex->getMessage(),
            ]);
        }
    }

    private function convertCoin($value)
    {
        return (float)$value / pow(10, 18);
    }

    public function calculateBalances($typeCoins, $address, $currency)
    {
        $wallet = [];
        $total = 0;
        foreach ($typeCoins as $coin) {
            $amount = $wallet[$coin]['amount'] = $this->getCoinAmount($coin, $address);
            $price = $this->getPrice($coin)['RAW'][$coin][$currency]['PRICE'];
            $wallet[$coin]['title'] = $coin;
            $wallet[$coin]['value'] = round((float)($amount * $price), 8);
            $total += $wallet[$coin]['value'];
        }
        $wallet['total'] = round((float)$total, 8);
        return $wallet;
    }



    public function setKey($input)
    {
        $rsa = new OpensslFun();
        $input['private_key'] = $rsa->getPrivateKey();
        $input['public_key'] = $rsa->getPublicKey();
        return $input;
    }

    public function getList($address = '0x5c7738b67a3403f349782244e59e776ddb3581c3')
    {
        $data = [
            'module' => "account",
            "action" => "txlist",
            "address" => $address,
            "startblock" => 0,
            "endblock" => 99999999,
            "sort" => 'DESC',
            "apikey" => 'YourApiKeyToken',
            "page" => 1,
            "offset" => 99999,
        ];
        $body = $this->send($data);
        return response()->json($body);
    }

}