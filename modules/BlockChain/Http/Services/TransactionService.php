<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/15/19
 * Time: 2:18 PM
 */

namespace BlockChain\Http\Services;


use Blockchain\Entities\Eth;
use GuzzleHttp\Client;

class TransactionService
{
    protected $uri;
    protected $contentType = 'application/json';
    protected $client;
    protected $account;

    public function __construct()
    {
        $this->client = new Client([
            'base_url' => [$this->uri],
            'defaults' => [
                'headers' => [
                    'content-type' => $this->contentType,
                    'Accept' => $this->contentType
                ],
            ],
        ]);
        $this->uri = config('domain.api');
    }

    public function getHistory($address)
    {
        $eth = new Eth();;
        $this->uri  = $eth->getApiUrl() . '/api';
        $data = [
            'module' => "account",
            "action" => "txlist",
            "address" => $address,
            "startblock" => 0,
            "endblock" => 99999999,
            "sort" => 'DESC',
            "apikey" => 'YourApiKeyToken',
            "page" => 1,
            "offset" => 99999,
        ];

        $body = $this->send($data);
        return $body['result'];
//        return $this->buildData($body, $address);
    }

    protected function send($data)
    {
        $res = $this->client->get($this->uri, ['query' => $data]);
        return json_decode($res->getBody(), true);
    }

    private function buildData($body, $address)
    {
        if ($body['status'] == 1) {
            $transactions = $body['result'];
        } else {
            $transactions = [];
        }

        $transactionTos = [];
        $transactionForms = [];
        $address = strtolower($address);
        foreach ($transactions as $transaction) {
            if ($transaction['from'] === $address) {
                $transactionForms[] = $transaction;
            }
            if ($transaction['to'] === $address) {
                $transactionTos[] = $transaction;
            }
        }
        return compact('transactions', 'transactionTos', 'transactionForms', 'user', 'address');
    }
}