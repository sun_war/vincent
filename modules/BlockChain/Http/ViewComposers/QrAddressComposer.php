<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/18/18
 * Time: 9:24 AM
 */

namespace BlockChain\Http\ViewComposers;


use BlockChain\Http\Services\QrService;
use Illuminate\View\View;

class QrAddressComposer
{
    private $service;
    public function __construct(QrService $service)
    {
        $this->service = $service;
    }

    public function compose(View $view)
    {
        $qrImage = $this->service->render(auth()->id());
        $view->with(compact('qrImage'));
    }
}