<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.user_id')}}</th>
<th>{{trans('label.private_key')}}</th>
<th>{{trans('label.public_key')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($cryptos as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->user_id}}</td>
<td>{{$row->private_key}}</td>
<td>{{$row->public_key}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('cryptos.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('cryptos.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('cryptos.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$cryptos->links()}}
</div>
