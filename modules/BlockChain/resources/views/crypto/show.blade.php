@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('cryptos.index')}}">{{trans('table.cryptos')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.private_key')}}</th>
<td>{!! $crypto->private_key !!}</td>
</tr><tr><th>{{__('label.public_key')}}</th>
<td>{!! $crypto->public_key !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#BlockChainMenu', '#cryptoMenu')
</script>
@endpush
