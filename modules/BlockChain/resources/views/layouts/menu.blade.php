<li class="has-sub root-level" id="BlockChainMenu"><a>
        <i class="fa fa-file"></i>
        <span class="title">{{__('menu.BlockChain')}}</span>
    </a>
    <ul>
        <li id="accountsMenu">
            <a href="{{route('accounts.create')}}">
                <span class="title">{{__('table.accounts')}}</span>
            </a>
        </li>
        <li id="cryptosMenu">
            <a href="{{route('cryptos.create')}}">
                <span class="title">{{__('table.cryptos')}}</span>
            </a>
        </li>
    </ul>
</li>