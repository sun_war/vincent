<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.user_id')}}</th>
<th>{{trans('label.mnemonic')}}</th>
<th>{{trans('label.private_key')}}</th>
<th>{{trans('label.keystore')}}</th>
<th>{{trans('label.status')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($accounts as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->user_id}}</td>
<td>{{$row->mnemonic}}</td>
<td>{{$row->private_key}}</td>
<td>{{$row->keystore}}</td>
<td>{{$row->status}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('accounts.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('accounts.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('accounts.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$accounts->links()}}
</div>
