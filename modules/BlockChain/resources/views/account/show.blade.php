@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('accounts.index')}}">{{trans('table.accounts')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.mnemonic')}}</th>
<td>{!! $account->mnemonic !!}</td>
</tr><tr><th>{{__('label.private_key')}}</th>
<td>{!! $account->private_key !!}</td>
</tr><tr><th>{{__('label.keystore')}}</th>
<td>{!! $account->keystore !!}</td>
</tr><tr><th>{{__('label.status')}}</th>
<td>{!! $account->status !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#BlockChainMenu', '#accountMenu')
</script>
@endpush
