<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 11/13/18
 * Time: 14:58
 */

namespace Blockchain\Entities;


class Eth
{
    public $network = 'ropsten';
    public $broadcastTransactionUrl = 'https://ropsten.etherscan.io';

    function getApiUrl()
    {
        switch ($this->network) {
            case 'mainnet':
                $ApiUrl = 'https://api.etherscan.io';
                break;
            case 'ropsten':
                $ApiUrl = 'https://api-ropsten.etherscan.io';
                break;
            default:
                $ApiUrl = 'https://ropsten.etherscan.io';
        }
        return $ApiUrl;
    }
}