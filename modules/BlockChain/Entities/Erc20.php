<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 11/7/18
 * Time: 08:33
 */

namespace Blockchain\Entities;


class Erc20
{
    public $network = 'ropsten';
    public $broadcastTransactionUrl = 'https://ropsten.etherscan.io';
    public $tokens = [
        'mgc4' => [
            'symbol' => 'mgc4',
            'name' => 'MGC004',
            'decimal' => 18,
            'address' => '0xebf0c068cc1dd9b343e92bc2cc09a2ca272d6511',
            'transactionFree' => 0.1
        ]
    ];

    function getApiUrl()
    {
        switch ($this->network) {
            case 'mainnet':
                $ApiUrl = 'https://api.etherscan.io';
                break;
            case 'ropsten':
                $ApiUrl = 'https://api-ropsten.etherscan.io';
                break;
            default:
                $ApiUrl = 'https://ropsten.etherscan.io';
        }
        return $ApiUrl;
    }
}