<?php

namespace Tutorial\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class LessonResult extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'lesson_results';
    public $fillable = [CREATED_BY_COL, LESSON_ID_COL, SCORE_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input[CREATED_BY_COL])) {
            $query->where(CREATED_BY_COL, $input[CREATED_BY_COL]);
        }
        if (isset($input[LESSON_ID_COL])) {
            $query->where(LESSON_ID_COL, $input[LESSON_ID_COL]);
        }
        if (isset($input[SCORE_COL])) {
            $query->where(SCORE_COL, $input[SCORE_COL]);
        }
        return $query;
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class, LESSON_ID_COL);
    }
}

