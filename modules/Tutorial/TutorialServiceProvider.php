<?php

namespace Tutorial\Providers;

use Illuminate\Support\ServiceProvider;
use Tutorial\Http\ViewComposers\TutorialComposer;
use Repositories\Http\Repositories\LessonCommentRepository;
use Repositories\Http\Repositories\LessonCommentRepositoryEloquent;
use Repositories\Http\Repositories\LessonFeedBackRepository;
use Repositories\Http\Repositories\LessonFeedBackRepositoryEloquent;
use Repositories\Http\Repositories\LessonRepository;
use Repositories\Http\Repositories\LessonRepositoryEloquent;
use Repositories\Http\Repositories\LessonResultRepository;
use Repositories\Http\Repositories\LessonResultRepositoryEloquent;
use Repositories\Http\Repositories\LessonSubCommentRepository;
use Repositories\Http\Repositories\LessonSubCommentRepositoryEloquent;
use Repositories\Http\Repositories\LessonTestRepository;
use Repositories\Http\Repositories\LessonTestRepositoryEloquent;
use Repositories\Http\Repositories\SectionRepository;
use Repositories\Http\Repositories\SectionRepositoryEloquent;
use Repositories\Http\Repositories\SectionResultRepository;
use Repositories\Http\Repositories\SectionResultRepositoryEloquent;
use Repositories\Http\Repositories\SectionTestRepository;
use Repositories\Http\Repositories\SectionTestRepositoryEloquent;
use Repositories\Http\Repositories\TutorialRepository;
use Repositories\Http\Repositories\TutorialRepositoryEloquent;
use Repositories\Http\Repositories\TutorialResultRepository;
use Repositories\Http\Repositories\TutorialResultRepositoryEloquent;
use Repositories\Http\Repositories\TutorialTestRepository;
use Repositories\Http\Repositories\TutorialTestRepositoryEloquent;

class TutorialServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database');
        $this->loadRoutesFrom(__DIR__ . '/router.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'tut');
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');

        view()->composer([
            'tut::section.index',
            'tut::section.create',
            'tut::section.update',
            'tut::lesson.create',
            'tut::lesson.update',
            'tut::lesson-test.index',
            'tut::lesson-test.create',
            'tut::lesson-test.update',
            'tut::lesson-result.index',
            'tut::lesson-result.create',
            'tut::lesson-result.update',
            'tut::tutorial-result.index',
            'tut::tutorial-result.update',
            'tut::layouts.components.tutorial'
        ], TutorialComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TutorialRepository::class, TutorialRepositoryEloquent::class);
        $this->app->bind(TutorialResultRepository::class, TutorialResultRepositoryEloquent::class);
        $this->app->bind(TutorialTestRepository::class, TutorialTestRepositoryEloquent::class);

        $this->app->bind(SectionRepository::class, SectionRepositoryEloquent::class);
        $this->app->bind(SectionTestRepository::class, SectionTestRepositoryEloquent::class);
        $this->app->bind(SectionResultRepository::class, SectionResultRepositoryEloquent::class);

        $this->app->bind(LessonTestRepository::class, LessonTestRepositoryEloquent::class);
        $this->app->bind(LessonCommentRepository::class, LessonCommentRepositoryEloquent::class);
        $this->app->bind(LessonFeedBackRepository::class, LessonFeedBackRepositoryEloquent::class);
        $this->app->bind(LessonRepository::class, LessonRepositoryEloquent::class);
        $this->app->bind(LessonSubCommentRepository::class, LessonSubCommentRepositoryEloquent::class);
        $this->app->bind(LessonResultRepository::class, LessonResultRepositoryEloquent::class);
    }
}
