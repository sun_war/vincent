@extends('edu::layouts.app')
@section('content')
    <div class="row">
        <h1 style="font-style: italic" class="text-info text-center">Đoán xí ngầu</h1>
        <form action="{{route('guess.play')}}" method="POST" id="gameForm">
            {!! csrf_field() !!}
            <div class="col-md-3 col-md-offset-9 text-info">
                <label>Đỗ thánh thử tài</label>
                <ul class="list-group">
                    @for($i = 1; $i < 7; $i++)
                        <li class="list-group-item bettingInput {{isset($number) && $number == $i ? 'list-group-item-success' : ''}}">{{$i}}
                            <input required type="radio"
                                   {{ isset($betting) && $betting == $i ? 'checked' : ''}} value="{{$i}}" class="pull-right "
                                   name="betting">
                        </li>
                    @endfor
                </ul>
                <div class="form-group">
                    <label>Đặt coin</label>
                    <input type="number" id="coin" value="{{ isset($coin) ? $coin : 1 }}" class="form-control"
                           placeholder="coin" name="coin">
                    <h4 style="color:green" id="number"></h4>
                </div>
                <div class="form-group">
                    <input type="checkbox" value="1" placeholder="coin" name="coin">
                    <p class="text-warning">Sử dụng tuyệt chiêu. Xung thiên pháo</p>
                </div>
                <div class="text-center form-group">
                    <button class="btn btn-primary" id="playBtn">Play</button>
                </div>
            </div>

            <input type="hidden" name="code" id="code">
            @include('gm::layouts.component.items')
        </form>
    </div>
@endsection

@push('head')
    <style>
        .main-content {
            background: url('{{asset('img/ebb2c65605aa0a2e8f432f0f8018b67f-1000.jpg')}}') !important;
        }
    </style>
@endpush

@push('js')
    <script src="{{asset('build/forceForm.js')}}"></script>
    <script>
        let opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-full-width",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        const game = {
            play: () => {
                const moneyTotal = '#moneyTotal';
                const itemSpecial = '.itemSpecial';
                const codeInput = '#code';
                $(itemSpecial).click( function () {
                    const self = $(this);
                    $(itemSpecial).css('background', 'white');
                    const codeVal = $(code).val();
                    let id = self.attr('id');
                    if(codeVal === id)
                    {
                        id = '';
                    } else {
                        self.css('background', 'green');
                    }
                    $(codeInput).val(id);
                });
                const playBtn = '#playBtn';
                const gameForm = '#gameForm';
                const url = $(gameForm).attr('action');
                const bettingInput = '.bettingInput';
                $(playBtn).click(function (e) {
                    e.preventDefault();
                    const self = $(this);
                    const data = $(gameForm).serialize();
                    $.ajax({
                        url: url,
                        data: data,
                        method: 'POST',
                        beforeSend: ()=> {
                            self.prop('disabled', true);
                        },
                        success: function (data) {
                            console.log(data)
                            $(moneyTotal).html(format.number(data.money, moneyTotal));
                            self.prop('disabled', false);
                            toastr.info(data.msg, "", opts);
                            $(bettingInput).removeClass('list-group-item-success');
                            $(bettingInput).eq(data.number - 1).addClass('list-group-item-success');
                        },
                        error: function () {
                            self.prop('disabled', false);
                            alert('ERROR')
                        }
                    });
                });
            }
        };
        game.play();
        $('#coin').magicFormatNumber('#number')
    </script>
@endpush