<?php
/**
 * Created by PhpStorm.
 * User: JK
 * Date: 3/15/2018
 * Time: 10:16 PM
 */

namespace Games\Http\Controllers;


use Illuminate\Http\Request;

class GuessController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->view = 'gm::guess.index';;
    }

    public function law()
    {
        $this->number = rand(1, 6);
        $this->win = $this->betting == $this->number;
        if ($this->win === false) {
            $this->coin = 0 - $this->coin;
        } else {
            $this->coin *= 5 ;
        }
        $this->mark($this->code, $this->coin);
    }
}