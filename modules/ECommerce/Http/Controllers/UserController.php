<?php
/**
 * Created by PhpStorm.
 * User: YingDiamond
 * Date: 3/1/2015
 * Time: 2:48 PM
 */

namespace ECommerce\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class UserController extends BaseController
{
    function index()
    {
        return view('eco::account.user')->with('users', User::get());
    }

    function getDeleteUser($id)
    {
        User::where('id', $id)->delete();
        return view('eco::account.user')
            ->with('users', User::get())
            ->with('global', 'Xóa thành công tài khoản');
    }

    function getViewUser($id)
    {
        return view('eco::account.viewUser')
            ->with('user', User::where('id', $id)->get());
    }

    public function postGetSession()
    {
        $a = session()->pull(Input::get('session'));
        return Response::json($a);
    }

} 