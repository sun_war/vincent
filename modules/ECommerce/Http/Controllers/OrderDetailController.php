<?php

namespace ECommerce\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use ECommerce\Models\OrderDetail;
use ECommerce\Http\Requests\OrderDetailCreateRequest;
use ECommerce\Http\Requests\OrderDetailUpdateRequest;
use ECommerce\Http\Repositories\OrderDetailRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class OrderDetailController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(OrderDetailRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['orderDetails'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('eco::order-detail.table', $data)->render();
        }
        return view('eco::order-detail.index', $data);
    }

    public function create()
    {
        return view('eco::order-detail.create');
    }

    public function store(OrderDetailCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('order-detail.index');
    }

    public function show($id)
    {
        $orderDetail = $this->repository->find($id);
        if(empty($orderDetail))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::order-detail.show', compact('orderDetail'));
    }

    public function edit($id)
    {
        $orderDetail = $this->repository->find($id);
        if(empty($orderDetail))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::order-detail.update', compact('orderDetail'));
    }

    public function update(OrderDetailUpdateRequest $request, $id)
    {
        $input = $request->all();
        $orderDetail = $this->repository->find($id);
        if(empty($orderDetail))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $orderDetail);
        session()->flash('success', 'update success');
        return redirect()->route('order-detail.index');
    }

    public function destroy($id)
    {
        $orderDetail = $this->repository->find($id);
        if(empty($orderDetail))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
