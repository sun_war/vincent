<?php

namespace ECommerce\Http\Controllers;

use App\Http\Controllers\Controller;
use ECommerce\Http\Requests\ResellerCreateRequest;
use ECommerce\Http\Requests\ResellerUpdateRequest;
use ECommerce\Http\Repositories\ResellerRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class ResellerController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(ResellerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['resellers'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('eco::reseller.table', $data)->render();
        }
        return view('eco::reseller.index', $data);
    }

    public function create()
    {
        return view('eco::reseller.create');
    }

    public function store(ResellerCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('resellers.index');
    }

    public function show($id)
    {
        $reseller = $this->repository->find($id);
        if (empty($reseller)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::reseller.show', compact('reseller'));
    }

    public function edit($id)
    {
        $reseller = $this->repository->find($id);
        if (empty($reseller)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::reseller.update', compact('reseller'));
    }

    public function update(ResellerUpdateRequest $request, $id)
    {
        $input = $request->all();
        $reseller = $this->repository->find($id);
        if (empty($reseller)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $reseller);
        session()->flash('success', 'update success');
        return redirect()->route('resellers.index');
    }

    public function destroy($id)
    {
        $reseller = $this->repository->find($id);
        if (empty($reseller)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
