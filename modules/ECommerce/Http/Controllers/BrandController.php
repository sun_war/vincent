<?php

namespace ECommerce\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use ECommerce\Models\Brand;
use ECommerce\Http\Requests\BrandCreateRequest;
use ECommerce\Http\Requests\BrandUpdateRequest;
use ECommerce\Http\Repositories\BrandRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class BrandController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(BrandRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['brands'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('eco::brand.table', $data)->render();
        }
        return view('eco::brand.index', $data);
    }

    public function create()
    {
        return view('eco::brand.create');
    }

    public function store(BrandCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('brand.index');
    }

    public function show($id)
    {
        $brand = $this->repository->find($id);
        if(empty($brand))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::brand.show', compact('brand'));
    }

    public function edit($id)
    {
        $brand = $this->repository->find($id);
        if(empty($brand))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::brand.update', compact('brand'));
    }

    public function update(BrandUpdateRequest $request, $id)
    {
        $input = $request->all();
        $brand = $this->repository->find($id);
        if(empty($brand))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $brand);
        session()->flash('success', 'update success');
        return redirect()->route('brand.index');
    }

    public function destroy($id)
    {
        $brand = $this->repository->find($id);
        if(empty($brand))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
