<?php

namespace ECommerce\Http\Repositories;


use ECommerce\Models\OrderDetail;
use ECommerce\Models\Product;
use Modularization\MultiInheritance\RepositoriesTrait;

use Illuminate\Support\Facades\Cache;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use ECommerce\Models\Order;

/**
 * Class NewsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OrderRepositoryEloquent extends BaseRepository implements OrderRepository
{
    use RepositoriesTrait;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Order::class;
    }

    public function myPaginate($input)
    {
        isset($input[PER_PAGE]) ?: $input[PER_PAGE] = 10;
        return $this->makeModel()
            ->with('details.product', 'creator')
            ->filter($input)
            ->paginate($input[PER_PAGE]);

    }

    public function store($input)
    {
        $user = auth()->user();
        $productPrices = app(Product::class)
            ->where(ID_COL, array_keys($input['cart']))
            ->pluck(PRICE_COL, ID_COL);

        $total = 0;
        $userId = auth()->id();
        $input[CREATED_BY_COL] = $userId;
        $input[IS_ACTIVE_COL] = 1;
        $input = $this->standardized($input, $this->makeModel());
        $order = $this->create($input);
        $order_details =  [];

        foreach ($input['cart'] as $product_id => $quantity) {
            $order_details[] = [
                ORDER_ID_COL => $order->id,
                PRODUCT_ID_COL => $product_id,
                QUANTITY_COL => $quantity,
                CREATED_BY_COL => $userId,
                PRICE_COL => $productPrices[$product_id]
            ];
            $total += $productPrices[$product_id] * $quantity;
        }

        if($user->coin >= $total) {
            app(OrderDetail::class)->insert($order_details);
            $user->coin -= $total;
            $user->save();
            \session()->forget('cart');
            return true;
        } else {
            session()->flash('error', 'Bạn không đủ tiền để đặt hàng');
        }
        return false;
    }


    public function edit($id)
    {
        $Order = $this->find($id);
        if(empty($Order))
        {
            return $Order;
        }
        return compact('Order');
    }

    public function change($input, $data)
    {
        $input[UPDATED_BY_COL] = auth()->id();
        $input = $this->standardized($input, $data);
        return $this->update($input, $data->id);
    }

    public function import($file)
    {
        set_time_limit(9999);
        $path = $this->makeModel()->uploadImport($file);
        return $this->importing($path);
    }

    private function standardized($input, $data)
    {
        $input = $data->uploads($input);
        return $data->checkbox($input);
    }

    public function destroy($data)
    {
        return $this->delete($data->id);
    }

    /**
     * Boot up the repository, ping criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
