<?php
/**
 * Created by PhpStorm.
 * User: CPM
 * Date: 5/24/2018
 * Time: 10:34 PM
 */

namespace ECommerce\Http\ViewComposers;

use ECommerce\Http\Repositories\BrandRepository;
use Illuminate\View\View;

class BrandComposer
{
    private $repository;
    public function __construct(BrandRepository $repository)
    {
        $this->repository = $repository;
    }

    public function compose(View $view)
    {
        $brandCompose = $this->repository->pluck('name', 'id');
        $view->with(compact('brandCompose'));
    }
}