@extends('edu::layouts.app')
@section('title', __('table.product'))
@section('content')
    <div class="row">
          <div class="col-lg-6">
            <a href="#">
              <img class="img-responsive" src="{{ \ViewFa::thumb($product->image)}}" alt="{{$product->name}}">
            </a>
          </div>
          <div class="col-lg-6">
            <h3><strong>Tên sản phẩm: </strong>{{$product->name}}</h3>
            <h3><strong>Giá: </strong>{{number_format($product->price)}} Đ</h3>
            <a href='{{route('cart.add', $product->id)}}' class="btn btn-success" role="button"><i class="fa fa-cart-plus"></i></a>
            <h3><strong>Thông tin</strong></h3>
            {!! $product->intro !!}
            {!! $product->details !!}
          </div>
     </div>
     <hr>
     @if(auth()->check())
         @include('eco::comment.setComment')
     @endif
@endsection