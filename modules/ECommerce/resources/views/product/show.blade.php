@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('product.index')}}">{{trans('table.products')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.show')}}</strong>
        </li>
    </ol>
    <div>
        {!! $products ->trademark_id !!}
        {!! $products ->name !!}
        {!! $products ->price !!}
        {!! $products ->image !!}
        {!! $products ->intro !!}
        {!! $products ->details !!}
        {!! $products ->discount !!}
        {!! $products ->views !!}
        {!! $products ->total !!}
        {!! $products ->recommended !!}
        {!! $products ->is_active !!}

    </div>
    <a href="{{url()->previous()}}" class="btn btn-default"><i class="fa fa-backward"></i></a>
@endsection