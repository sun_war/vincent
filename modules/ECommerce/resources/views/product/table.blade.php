<table class="table">
    <thead>
    <tr>

{{--        <th>{{trans('label.trademark_id')}}</th>--}}
        <th>{{trans('label.name')}}</th>
        <th>{{trans('label.price')}}</th>
        {{--<th>{{trans('label.image')}}</th>--}}
        {{--<th>{{trans('label.intro')}}</th>--}}
        {{--<th>{{trans('label.details')}}</th>--}}
        {{--<th>{{trans('label.discount')}}</th>--}}
        {{--<th>{{trans('label.views')}}</th>--}}
        <th>{{trans('label.total')}}</th>
        {{--<th>{{trans('label.recommended')}}</th>--}}
        {{--<th>{{trans('label.is_active')}}</th>--}}

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">

{{--            <td>{{$row->trademark_id}}</td>--}}
            <td>{{$row->name}}</td>
            <td>{{$row->price}}</td>
{{--            <td>{{$row->image}}</td>--}}
            {{--<td>{{$row->intro}}</td>--}}
            {{--<td>{{$row->details}}</td>--}}
            {{--<td>{{$row->discount}}</td>--}}
            {{--<td>{{$row->views}}</td>--}}
            <td>{{$row->total}}</td>
            {{--<td>{{$row->recommended}}</td>--}}
            {{--<td>{{$row->is_active}}</td>--}}

            <td class="text-right">
                <form method="POST" action="{{route('product.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('product.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    {{--<a href="{{route('product.show', $row->id)}}" class="btn btn-info btn-xs">--}}
                        {{--<i class="fa fa-eye"></i>--}}
                    {{--</a>--}}
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$products->links()}}
</div>
