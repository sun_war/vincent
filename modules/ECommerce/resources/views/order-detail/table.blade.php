<table class="table">
    <thead>
        <tr>
            <th>{{trans('label.order_id')}}</th>
<th>{{trans('label.product_id')}}</th>
<th>{{trans('label.quantity')}}</th>
<th>{{trans('label.price')}}</th>

            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($orderDetails as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td>{{$row->order_id}}</td>
<td>{{$row->product_id}}</td>
<td>{{$row->quantity}}</td>
<td>{{$row->price}}</td>

            <td class="text-right">
                <form method="POST" action="{{route('order-detail.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('order-detail.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('order-detail.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$orderDetails->links()}}
</div>
