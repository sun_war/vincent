@extends('edu::layouts.app')
    @section('title')
     Trang chủ
    @stop
    @section('content')
        @if(session()->has('global'))
            <p>{{session()->pull('global')}}</p>
        @endif
        @if(Auth::check())
            Xin chào, {{Auth::user()->fullname}}
            <hr>

        @else
        <p>Bạn chưa đăng nhập hệ thống</p>
        @endif
    @stop