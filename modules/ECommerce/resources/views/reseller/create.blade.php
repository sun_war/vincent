@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('resellers.index')}}">{{trans('table.resellers')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.create')}}</strong>
        </li>
    </ol>
    <div class="row">
        <form action="{{route('resellers.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group col-lg-12">
                <label for="price">{{trans('label.price')}}</label>
                <input type="number" required class="form-control" name="price" id="price">
            </div>
            <div class="form-group col-lg-12">
                <label for="product_id">{{trans('label.product_id')}}</label>
                <input type="number" required class="form-control" name="product_id" id="product_id">
            </div>
            <div class="form-group col-lg-12">
                <label for="created_by">{{trans('label.created_by')}}</label>
                <input type="number" required class="form-control" name="created_by" id="created_by">
            </div>
            <div class="form-group col-lg-12">
                <label for="qty">{{trans('label.qty')}}</label>
                <input type="number" required class="form-control" name="qty" id="qty">
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection