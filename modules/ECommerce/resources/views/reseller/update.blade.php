@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('resellers.index')}}">{{trans('table.resellers')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.update')}}</strong>
        </li>
    </ol>
    <div class="row">
        <form action="{{route('resellers.update', $reseller->id)}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group col-lg-12">
                <label for="price">{{trans('label.price')}}</label>
                <input required type="number" class="form-control" name="price" id="price" value="{{$reseller->price}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="product_id">{{trans('label.product_id')}}</label>
                <input required type="number" class="form-control" name="product_id" id="product_id"
                       value="{{$reseller->product_id}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="created_by">{{trans('label.created_by')}}</label>
                <input required type="number" class="form-control" name="created_by" id="created_by"
                       value="{{$reseller->created_by}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="qty">{{trans('label.qty')}}</label>
                <input required type="number" class="form-control" name="qty" id="qty" value="{{$reseller->qty}}">
            </div>

            <div class="col-lg-12 form-group">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script>
        Menu('#Menu', '#resellerMenu')
    </script>
@endpush
