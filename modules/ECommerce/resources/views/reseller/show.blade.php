@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('resellers.index')}}">{{trans('table.resellers')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.price')}}</th>
<td>{!! $reseller->price !!}</td>
</tr><tr><th>{{__('label.product_id')}}</th>
<td>{!! $reseller->product_id !!}</td>
</tr><tr><th>{{__('label.created_by')}}</th>
<td>{!! $reseller->created_by !!}</td>
</tr><tr><th>{{__('label.qty')}}</th>
<td>{!! $reseller->qty !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#Menu', '#resellerMenu')
</script>
@endpush
