<table class="table">
    <thead>
    <tr>
        {{--<th>{{trans('label.name')}}</th>--}}
{{--        <th>{{trans('label.image')}}</th>--}}
{{--        <th>{{trans('label.is_active')}}</th>--}}
{{--        <th>{{trans('label.description')}}</th>--}}

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($brands as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">

            <td>{{$row->name}}</td>
{{--            <td>{{$row->image}}</td>--}}
{{--            <td>{{$row->is_active}}</td>--}}
{{--            <td>{{$row->description}}</td>--}}

            <td class="text-right">
                <form method="POST" action="{{route('brand.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('brand.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    {{--<a href="{{route('brand.show', $row->id)}}" class="btn btn-info btn-xs">--}}
                        {{--<i class="fa fa-eye"></i>--}}
                    {{--</a>--}}
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$brands->links()}}
</div>
