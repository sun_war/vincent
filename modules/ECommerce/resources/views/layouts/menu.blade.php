<li class="has-sub root-level" id="ECommerceMenu">
    <a>
        <i class="fa fa-cart-plus"></i>
        <span class="title">E-commerce</span>
    </a>
    <ul>
        <li class="{{ request()->is('category*') ? 'active' : ''}}" id="categoryMenu">
            <a href="{{route('category.index')}}">
                <span class="title">{{__('table.categories')}}</span>
            </a>
        </li>
        <li class="{{ request()->is('brand*') ? 'active' : ''}}" id="branchMenu">
            <a href="{{route('brand.index')}}">
                <span class="title">{{__('table.brands')}}</span>
            </a>
        </li>
        <li class="{{ request()->is('type*') ? 'active' : ''}}" id="typeMenu">
            <a href="{{route('type.index')}}">
                <span class="title">{{__('table.types')}}</span>
            </a>
        </li>
        <li class="{{ request()->is('product*') ? 'active' : ''}}" id="productMenu">
            <a href="{{route('product.index')}}">
                <span class="title">{{__('table.products')}}</span>
            </a>
        </li>
        <li class="{{ request()->is('order*') ? 'active' : ''}}" id="orderMenu">
            <a href="{{route('order.index')}}">
                <span class="title">{{__('table.orders')}}</span>
            </a>
        </li>
        <li class="{{ request()->is('order-detail*') ? 'active' : ''}}" id="orderDetailMenu">
            <a href="{{route('order-detail.index')}}">
                <span class="title">{{__('table.order_details')}}</span>
            </a>
        </li>
    </ul>
</li>