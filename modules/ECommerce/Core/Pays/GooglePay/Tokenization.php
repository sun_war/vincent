<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 8/27/18
 * Time: 10:34 AM
 */

namespace ECommerce\Core\Pays\GooglePay;


class Tokenization
{
    private $gateway, $direct;

    public function setGateway($gateway)
    {
        $this->gateway = $gateway;
    }

    public function getGateway()
    {
        return [
            'type' => 'PAYMENT_GATEWAY',
            'parameters' => [
                'gateway' => 'ex',
                'gatewayMerchantId' => 'gatewayMerchantId'
            ]
        ];
    }

    public function setDirect($direct)
    {
        $this->direct = $direct;
    }

    public function getDirect() {
        return [
            'type' => 'DIRECT',
            'parameters' => [
                'protocolVersion' => 'ECV1',
                "publicKey" => ""
            ]
        ];
    }
}