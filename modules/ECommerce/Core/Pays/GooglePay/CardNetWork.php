<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 8/27/18
 * Time: 10:40 AM
 */

namespace ECommerce\Core\Pays\GooglePay;


class CardNetWork
{
    private $cardNames, $authMethods;

    public function setCardNames($cardNames)
    {
        $this->cardNames = $cardNames;
    }

    public function getCardNames()
    {
        return ["AMEX", "DISCOVER", "JCB", "MASTERCARD", "VISA"];
    }

    public function setAuthMethods($authMethods)
    {
        $this->authMethods = $authMethods;
    }

    public function authMethods()
    {
        return ["PAN_ONLY", "CRYPTOGRAM_3DS"];
    }
}