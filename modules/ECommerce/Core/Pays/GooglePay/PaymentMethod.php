<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 8/27/18
 * Time: 10:44 AM
 */

namespace ECommerce\Core\Pays\GooglePay;


class PaymentMethod
{
    public function getBaseMethod()
    {
        return [
            'type' => 'CARD',
            'parameters' => [
                'allowedAuthMethods' => '',
                'allowedCardNetworks' => ''
            ]
        ];
    }

    public function getCardPaymentMethod()
    {
        $token = new Tokenization();
        return array_merge([ 'tokenizationSpecification' => $token->getDirect()], $this->getBaseMethod());
    }
}