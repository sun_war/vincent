<?php

namespace ECommerce\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class OrderDetail extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'order_details';
    public $fillable = [ORDER_ID_COL, PRODUCT_ID_COL, QUANTITY_COL, PRICE_COL, CREATED_BY_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input[ORDER_ID_COL])) {
            $query->where(ORDER_ID_COL, $input[ORDER_ID_COL]);
        }
        if (isset($input[PRODUCT_ID_COL])) {
            $query->where(PRODUCT_ID_COL, $input[PRODUCT_ID_COL]);
        }
        if (isset($input[QUANTITY_COL])) {
            $query->where(QUANTITY_COL, $input[QUANTITY_COL]);
        }
        if (isset($input[PRICE_COL])) {
            $query->where(PRICE_COL, $input[PRICE_COL]);
        }
        return $query;
    }

    public function product()
    {
        return $this->belongsTo(Product::class, PRODUCT_ID_COL);
    }

    public function order()
    {
        return $this->belongsTo(Order::class, ORDER_ID_COL);
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/order_details'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

