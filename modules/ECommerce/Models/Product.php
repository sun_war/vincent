<?php

namespace ECommerce\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Product extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'products';
    public $fillable = [BRAND_ID_COL, NAME_COL, PRICE_COL, IMAGE_COL, INTRO_COL, DETAILS_COL, CODE_COL,
        DISCOUNT_COL, VIEWS_COL, TOTAL_COL, RECOMMENDED_COL, IS_ACTIVE_COL, TYPE_ID_COL, CATEGORY_ID_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input[BRAND_ID_COL])) {
            $query->where(BRAND_ID_COL, $input[BRAND_ID_COL]);
        }
        if (isset($input[NAME_COL])) {
            $query->where(NAME_COL, 'LIKE', '%' . $input[NAME_COL] . '%');
        }
        if (isset($input[CODE_COL])) {
            $query->where(CODE_COL, 'LIKE', '%' . $input[CODE_COL] . '%');
        }
        if (isset($input[PRICE_COL])) {
            $query->where(PRICE_COL, $input[PRICE_COL]);
        }
        if (isset($input[IMAGE_COL])) {
            $query->where(IMAGE_COL, $input[IMAGE_COL]);
        }
        if (isset($input[INTRO_COL])) {
            $query->where(INTRO_COL, $input[INTRO_COL]);
        }
        if (isset($input[DETAILS_COL])) {
            $query->where(DETAILS_COL, $input[DETAILS_COL]);
        }
        if (isset($input[DISCOUNT_COL])) {
            $query->where(DISCOUNT_COL, $input[DISCOUNT_COL]);
        }
        if (isset($input[VIEWS_COL])) {
            $query->where(VIEWS_COL, $input[VIEWS_COL]);
        }
        if (isset($input[TOTAL_COL])) {
            $query->where(TOTAL_COL, $input[TOTAL_COL]);
        }
        if (isset($input[RECOMMENDED_COL])) {
            $query->where(RECOMMENDED_COL, $input[RECOMMENDED_COL]);
        }
        if (isset($input[TYPE_ID_COL])) {
            $query->where(TYPE_ID_COL, $input[TYPE_ID_COL]);
        }
        if (isset($input[IS_ACTIVE_COL])) {
            $query->where(IS_ACTIVE_COL, $input[IS_ACTIVE_COL]);
        }
        if (isset($input[CATEGORY_ID_COL])) {
            $query->where(CATEGORY_ID_COL, $input[CATEGORY_ID_COL]);
        }
        if (isset($input['prices'])) {
            $prices = explode('-', $input['prices']);
            $query->where(PRICE_COL, '>=', $prices[0]);
            $query->where(PRICE_COL, '<=', $prices[1]);
        }
        return $query;
    }

    public $fileUpload = [IMAGE_COL => 1];
    protected $pathUpload = [IMAGE_COL => '/images/products'];
    protected $thumbImage = [
        IMAGE_COL => [
            '/thumbs/' => [
                [200, 150], [300, 225], [400, 300]
            ]
        ]
    ];
    protected $checkbox = [IS_ACTIVE_COL];
}

