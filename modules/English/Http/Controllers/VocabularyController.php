<?php

namespace English\Http\Controllers;

use App\Http\Controllers\Controller;
use English\Http\Requests\VocabularyCreateRequest;
use English\Http\Requests\VocabularyUpdateRequest;
use English\Http\Repositories\VocabularyRepository;
use Illuminate\Http\Request;

class VocabularyController extends Controller
{
    private $repository;

    public function __construct(VocabularyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['vocabularies'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('en::vocabularies.table', $data)->render();
        }
        return view('en::vocabularies.index', $data);
    }

    public function create()
    {
        return view('en::vocabularies.create');
    }

    public function store(VocabularyCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        return redirect(route('vocabularies.index'));
    }

    public function show($id)
    {
        $vocabulary = $this->repository->find($id);
        if (empty($vocabulary)) {
            session()->flash('error', 'Not Found');
            return back();
        }
        return view('en::vocabularies.show', compact('vocabulary'));
    }

    public function edit($id)
    {
        $vocabulary = $this->repository->find($id);
        if (empty($vocabulary)) {
            session()->flash('error', 'Not Found');
            return back();
        }
        return view('en::vocabularies.update', compact('vocabulary'));
    }

    public function update(VocabularyUpdateRequest $request, $id)
    {
        $input = $request->all();
        $vocabulary = $this->repository->find($id);
        if (empty($vocabulary)) {
            session()->flash('error', 'Not Found');
            return back();
        }
        $this->repository->change($input, $vocabulary);
        return redirect(route('vocabularies.index'));
    }

    public function destroy($id)
    {
        $vocabulary = $this->repository->find($id);
        if (empty($vocabulary)) {
            session()->flash('error', 'Not Found');
            return back();
        }
        $this->repository->delete($id);
    }
}
