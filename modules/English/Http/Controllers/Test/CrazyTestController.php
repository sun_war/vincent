<?php

namespace English\Http\Controllers\Test;

use English\Http\Repositories\CrazyRepository;
use English\Http\Services\CrazyTestService;
use Illuminate\Http\Request;

class CrazyTestController
{
    private $repository, $doView, $doneView, $service;

    public function __construct(CrazyRepository $repository, CrazyTestService $service)
    {
        $this->repository = $repository;
        $this->doView = 'en::test.crazy.do';
        $this->doneView = 'en::test.crazy.done';
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $input['orderBy'] = [ID_COL => 'ASC'];
        $questions = $this->repository->myPaginate($input);
        return view($this->doView, compact('questions'));
    }

    public function doing($id)
    {
        $data = $this->service->doing($id);
        $data['routeName'] = 'test.crazy.doing';

        return view($this->doView, $data);
    }

    public function done($id)
    {
        $sentences = request('sentences');
        $meanings = request('meanings');

        $data = $this->service->done($id, $sentences, $meanings);
        if (empty($data)) {
            session()->flash('error', 'Lesson not found');
            return redirect()->route('home');
        }
        return view($this->doneView, $data)->with(compact('sentences', 'meanings'));
    }

    public function writing($id)
    {
        $data = $this->service->writing($id);
        $data['routeName'] = 'test.crazy.writing';
        return view('en::test.crazy.writing', $data);
    }

    public function written($id)
    {
        $sentences = request('sentences');
        $meanings = request('meanings');
        $data = $this->service->written($id, $sentences, $meanings);
        if (empty($data)) {
            session()->flash('error', 'Lesson not found');
            return redirect()->route('home');
        }
        return view('en::test.crazy.written', $data)->with(compact('sentences', 'meanings'));
    }
}
