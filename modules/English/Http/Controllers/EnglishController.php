<?php

namespace English\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Models\Slide;
use English\Models\Crazy;
use English\Models\CrazyHistory;
use English\Models\FillInTheBlank;
use English\Models\Mistake;
use English\Models\Pronunciation;
use English\Models\Similarity;

class EnglishController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $crazyList = Crazy::pluck(NAME_COL, ID_COL);

        if (auth()->check()) {
            $user_id = auth()->id();
            $filter = [
                USER_ID_COL => $user_id,
                TYPE_COL => 0
            ];
            foreach ($crazyList as $id => $name) {
                $filter[CRAZY_ID_COL] = $id;
            }
        }

        $pronunciationPage = ceil(app(Pronunciation::class)->count() / ENGLISH_PER_PAGE);
        $mistakesPage = ceil(app(Mistake::class)->count() / ENGLISH_PER_PAGE);
        $fillInTheBlankPage = ceil(app(FillInTheBlank::class)->count() / ENGLISH_PER_PAGE);
        $similarityPage = ceil(app(Similarity::class)->count() / ENGLISH_PER_PAGE);
        $slides = app(Slide::class)->where(IS_ACTIVE_COL, 1)->orderBy(NO_COL)->get();

        $data = compact('pronunciationPage', 'mistakesPage', 'fillInTheBlankPage', 'similarityPage',
            'crazyList', 'slides');

        return view('en::english.index', $data);
    }
}
