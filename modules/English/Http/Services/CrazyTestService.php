<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 8/8/18
 * Time: 8:36 PM
 */

namespace English\Http\Services;

use English\Http\Repositories\CrazyHistoryRepository;
use English\Http\Repositories\CrazyRepository;
use English\Http\Repositories\CrazyWriteHistoryRepository;
use Illuminate\Support\Facades\DB;
use Modularization\Facades\FormatFa;

class CrazyTestService
{
    private $repository, $historyRepository, $crazyWriteHistoryRepository;
    private $data, $details, $sentenceList, $score = 0;
    private $crazy = NULL;

    public function __construct(
        CrazyRepository $repository,
        CrazyHistoryRepository $historyRepository,
        CrazyWriteHistoryRepository $crazyWriteHistoryRepository
    )
    {
        $this->repository = $repository;
        $this->historyRepository = $historyRepository;
        $this->crazyWriteHistoryRepository = $crazyWriteHistoryRepository;
    }

    public function doing($id)
    {
        $this->crazy = $crazy = $this->repository->find($id);
        if (empty($this->crazy)) {
            return $this->crazy;
        }
        $ens = $this->getListRand(SENTENCE_COL);
        $vis = $this->getListRand(MEANING_COL);
        $this->data = compact('crazy', 'ens', 'vis');
        $this->courseList()
            ->statistic($this->historyRepository)
            ->getCrazyCourse();
        return $this->data;
    }

    public function done($id, $sentences, $meanings)
    {
        $this->crazy = $this->data['crazy'] = $this->repository->find($id);
        if (empty($this->crazy)) {
            return $this->crazy;
        }
        $this->getDetail()
            ->getSentenceList()
            ->checkTest($sentences, $meanings)
            ->getCrazyList()
            ->saveHistory($this->historyRepository)
            ->statistic($this->historyRepository)
            ->getCrazyCourse();
        return $this->data;
    }

    public function writing($id)
    {
        $this->crazy = $crazy = $this->repository->find($id);
        if (empty($this->crazy)) {
            return $this->crazy;
        }
        $lesson = $this->getLesson(SENTENCE_COL);
        $ens = $this->getListHash($lesson);
        $randEns = $this->getListRandEn($lesson);
        $vis = $this->getListRand(MEANING_COL);
        $this->data = compact('crazy', 'ens', 'vis', 'randEns');
        $this->courseList()
            ->statistic($this->crazyWriteHistoryRepository)
            ->getCrazyCourse();
        return $this->data;
    }

    private function courseList()
    {
        $this->data['crazyList'] = $this->repository
            ->filterList([CRAZY_COURSE_ID_COL => $this->crazy[CRAZY_COURSE_ID_COL]]);
        return $this;
    }

    public function written($id, $sentences, $meanings)
    {
        $this->crazy = $this->data['crazy'] = $this->repository->find($id);
        if (empty($this->crazy)) {
            return $this->crazy;
        }
        $this->getDetail()
            ->getSentenceList()
            ->checkWritten($sentences, $meanings)
            ->getCrazyList()
            ->saveHistory($this->crazyWriteHistoryRepository)
            ->statistic($this->crazyWriteHistoryRepository)
            ->getCrazyCourse();
        return $this->data;
    }

    private function getLesson($field)
    {
         return $this->crazy->details()
            ->orderBy(NO_COL)
            ->pluck($field, ID_COL);
    }

    private function getListHash($lesson)
    {
        $data = [];
        foreach ($lesson as $id => $value) {
            $data[$id] = $this->hash($value);
        }
        return $data;
    }

    private function getListRandEn($lesson) {
        foreach ($lesson as $id => $value) {
            $lesson[$id] = $this->rand($value);
        }
        return $lesson;
    }

    private function hash($subject)
    {
        $pattern = '/[a-zA-Z]{1}/';
//        $pattern = '/[a-o]{1}/';
        return preg_replace($pattern, '*', $subject);
    }

    private function rand($subject) {
        $array = explode(' ', $subject);
        $array = $this->shuffle_assoc($array);
        return implode(' ', $array);
    }

    function shuffle_assoc($array) {
        $keys = array_keys($array);
        shuffle($keys);
        shuffle($keys);
        $new = [];
        foreach($keys as $key) {
            $new[$key] = $array[$key];
        }
       return $new;
    }

    private function getListRand($field)
    {
        return $this->crazy->details()
            ->orderBy(DB::raw('RAND()'))
            ->pluck($field, ID_COL);
    }

    private function getDetail()
    {
        $this->details = $this->crazy->details()
            ->orderBy(NO_COL)
            ->get([MEANING_COL, ID_COL, SENTENCE_COL]);
        return $this;
    }

    private function getSentenceList()
    {
        $this->sentenceList = $this->details
            ->pluck(SENTENCE_COL, ID_COL);
        return $this;
    }

    private function getCrazyList()
    {
        $this->data['crazyList'] = $this->repository
            ->filterList([CRAZY_COURSE_ID_COL => $this->crazy[CRAZY_COURSE_ID_COL]]);
        return $this;
    }

    private function checkTest($sentences, $meanings)
    {
        $ens = [];
        $resultMap = [];
        foreach ($this->details as $i => $crazyDetail) {
            $resultMap[$i] = false;
            if ($sentences[$i] == $crazyDetail[ID_COL] && $meanings[$i] === $crazyDetail[MEANING_COL]) {
                $this->score++;
                $resultMap[$i] = true;
            }
            $ens[$i] = $this->sentenceList[$sentences[$i]];
        }
        session()->flash('success', "Bạn làm đúng $this->score câu");
        $this->data = array_merge($this->data, compact('score', 'ens', 'resultMap'));
        return $this;
    }

    private function checkWritten($sentences, $meanings)
    {
        $lesson = $this->getLesson(SENTENCE_COL);
        $ens = $this->getListHash($lesson);
        $randEns = $this->getListRandEn($lesson);
        $resultMap = [];
        foreach ($this->details as $i => $crazyDetail) {
            $crazyDetailId = $crazyDetail[ID_COL];
            $resultMap[$crazyDetailId] = false;
            if ($this->isWritePass($sentences[$crazyDetailId], $crazyDetail[SENTENCE_COL], $meanings[$crazyDetailId], $crazyDetail[MEANING_COL])) {
                $this->score++;
                $resultMap[$crazyDetailId] = true;
            }
        }
        session()->flash('success', "Bạn làm đúng $this->score câu");
        $this->data = array_merge($this->data, compact('score', 'ens', 'resultMap', 'randEns'));
        return $this;
    }

    private function isWritePass($sentenceIn, $sentence, $meaningIn, $meaning)
    {
        $sentenceIn = $this->convert($sentenceIn);
        $sentence = $this->convert($sentence);
        return ($sentenceIn === $sentence && $meaningIn === $meaning);
    }

    private function convert($input)
    {
        return strtolower(trim(FormatFa::oneSpace($input)));
    }

    private function saveHistory($repository)
    {
        if (auth()->check()) {
            $mark = $this->score === 0 ? 0 : (int)($this->score / count($this->details) * 1000);
            $historyData = [
                USER_ID_COL => auth()->id(),
                CRAZY_ID_COL => $this->crazy->id,
                SCORE_COL => $mark,
                TYPE_COL => request(TYPE_COL, 0),
            ];
            $repository->create($historyData);
        }
        return $this;
    }

    private function statistic($repository)
    {
        if (auth()->check()) {
            $filter = [
                USER_ID_COL => auth()->id(),
                CRAZY_ID_COL => $this->crazy->id,
            ];
            $this->data['testedCount'] = $repository
                ->filterCount($filter);
            foreach ([0 => 'avgFirst', 1 => 'avgAgain'] as $type => $value) {
                $filter[TYPE_COL] = $type;
                $this->data[$value] = round($repository
                        ->filterAvg($filter, SCORE_COL) / 100, 2);
            }
        }
        return $this;
    }

    public function getCrazyCourse()
    {
        $this->data['crazyCourse'] = $this->crazy->crazyCourse;
        return $this;
    }
}