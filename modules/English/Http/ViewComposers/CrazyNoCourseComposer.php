<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/25/18
 * Time: 3:31 PM
 */

namespace English\Http\ViewComposers;


use English\Models\Crazy;
use Illuminate\View\View;

class CrazyNoCourseComposer
{
    public function compose(View $view)
    {
        $crazyNoCourseCompose = Crazy::whereNull(CRAZY_COURSE_ID_COL)
            ->pluck(NAME_COL, ID_COL);
        return $view->with(compact('crazyNoCourseCompose'));
    }
}