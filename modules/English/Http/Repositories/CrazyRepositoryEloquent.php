<?php

namespace English\Http\Repositories;


use English\Models\Crazy;
use Illuminate\Support\Facades\DB;
use Modularization\MultiInheritance\RepositoriesTrait;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class NewsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CrazyRepositoryEloquent extends BaseRepository implements CrazyRepository
{
    use RepositoriesTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Crazy::class;
    }

    public function myPaginate($input)
    {
        isset($input[PER_PAGE]) ?: $input[PER_PAGE] = 10;
        return $this->makeModel()
            ->filter($input)
            ->orderBy('id', 'DESC')
            ->paginate($input[PER_PAGE]);
    }

    public function store($input)
    {
        $input[CREATED_BY_COL] = auth()->id();
        $input = $this->standardized($input, $this->makeModel());
        $crazy = $this->create($input);
        if (isset($input['sentences'])) {
            $this->detailCreates($input, $crazy);
        }
    }

    public function detailCreates($input, $crazy)
    {
        $data = [];
        foreach ($input['sentences'] as $no => $sentence) {
            $data[] = [
                SENTENCE_COL => $sentence,
                MEANING_COL => $input['meanings'][$no],
                TIME_COL => $input['times'][$no],
                NO_COL => $no + 1,
                IS_ACTIVE_COL => 1,
                CREATED_BY_COL => auth()->id(),
            ];
        }
        $crazy->details()->createMany($data);
    }

    public function edit($id)
    {
        $crazy = $this->find($id);
        if (empty($crazy)) {
            return $crazy;
        }
        $details = $crazy->details()->orderBy(NO_COL)->get();
        return compact('crazy', 'details');
    }

    public function change($input, $crazy)
    {
        $input[UPDATED_BY_COL] = auth()->id();
        $input = $this->standardized($input, $crazy);
        $crazy = $this->update($input, $crazy->id);
        if (isset($input['sentences'])) {
            $this->detailUpdate($input, $crazy);
        }
        return $crazy;
    }

    private function detailUpdate($input, $crazy)
    {
        $no = 0;
        $detailIds = [];
        if (isset($input['sentences'])) {
            foreach ($input['sentences'] as $detailId => $sentence) {
                $data = [
                    NO_COL => ++$no,
                    TIME_COL => $input['times'][$detailId],
                    SENTENCE_COL => $sentence,
                    MEANING_COL => $input['meanings'][$detailId],
                    IS_ACTIVE_COL => 1,
                ];
                if ((int)($detailId) === 0) {
                    $newCrazy = $crazy->details()->create($data);
                    $detailIds[] = $newCrazy->id;
                } else {
                    app(CrazyDetailRepositoryEloquent::class)
                        ->update($data, $detailId);
                    $detailIds[] = $detailId;
                }
            }
            $this->deleteDetails($crazy, $detailIds);
            unset($input['sentences']);
            unset($input['meanings']);
        }
    }

    private function deleteDetails($crazy, $detailIds)
    {
        $crazy->details()
            ->whereNotIn(ID_COL, $detailIds)
            ->delete();
    }

    public function import($file)
    {
        set_time_limit(9999);
        $path = $this->makeModel()->uploadImport($file);
        return $this->importing($path);
    }

    private function standardized($input, $data)
    {
        return $data->uploads($input);
    }

    public function destroy($data)
    {
        return $this->delete($data->id);
    }

    /**
     * Boot up the repository, ping criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
