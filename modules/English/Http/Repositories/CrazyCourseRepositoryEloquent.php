<?php

namespace English\Http\Repositories;


use English\Models\Crazy;
use English\Models\CrazyCourse;
use English\Models\CrazyHistory;
use English\Models\CrazyWriteHistory;
use Modularization\MultiInheritance\RepositoriesTrait;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class NewsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CrazyCourseRepositoryEloquent extends BaseRepository implements CrazyCourseRepository
{
    use RepositoriesTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CrazyCourse::class;
    }

    public function myPaginate($input)
    {
        isset($input[PER_PAGE]) ?: $input[PER_PAGE] = 10;
        return $this->makeModel()
            ->filter($input)
            ->orderBy('id', 'DESC')
            ->paginate($input[PER_PAGE]);
    }

    public function store($input)
    {
        $input[CREATED_BY_COL] = auth()->id();
        $input = $this->standardized($input, $this->makeModel());
        $crazyCourse = $this->create($input);
        $this->updateCourse($input['crazy_ids'], $crazyCourse->id);
        return $crazyCourse;
    }

    private function updateCourse($crazyIds, $crazyCourseId)
    {
        Crazy::where(CRAZY_COURSE_ID_COL, $crazyCourseId)
            ->update([CRAZY_COURSE_ID_COL => NULL]);

        Crazy::whereIn(ID_COL, $crazyIds)
            ->update([CRAZY_COURSE_ID_COL => $crazyCourseId]);
    }

    public function edit($id)
    {
        $crazyCourse = $this->find($id);
        if (empty($crazyCourse)) {
            return $crazyCourse;
        }
        $crazyList = $crazyCourse->crazies()->pluck(NAME_COL, ID_COL);
        return compact('crazyCourse', 'crazyList');
    }

    public function getList($id)
    {
        $crazyCourse = $this->find($id);

        if (empty($crazyCourse)) {
            return NULL;
        }

        $crazyList = $crazyCourse->crazies()->pluck(NAME_COL, ID_COL);

        $testCounts = [];
        $avgScores = [];

        if (auth()->check()) {
            $user_id = auth()->id();
            $filter = [
                USER_ID_COL => $user_id,
                TYPE_COL => 0
            ];
            foreach ($crazyList as $id => $name) {
                $filter[CRAZY_ID_COL] = $id;
                $testCounts[$id] = CrazyHistory::filter($filter)
                    ->count();
                $avgScores[$id] = round(CrazyHistory::filter($filter)
                        ->avg(SCORE_COL) / 100, 2);

                $testWriteCounts[$id] = CrazyWriteHistory::filter($filter)
                    ->count();

                $avgWriteScores[$id] = round(CrazyWriteHistory::filter($filter)
                        ->avg(SCORE_COL) / 100, 2);
            }
        } else {
            foreach ($crazyList as $id => $name) {
                $testCounts[$id] = 0;
                $avgScores[$id] = 0;
                $testWriteCounts[$id] = 0;
                $avgWriteScores[$id] = 0;
            }
        }
        return compact('crazyCourse', 'crazyList', 'testCounts', 'avgScores', 'testWriteCounts', 'avgWriteScores');
    }

    public function change($input, $data)
    {
        $input[UPDATED_BY_COL] = auth()->id();
        $input = $this->standardized($input, $data);
        $this->updateCourse($input['crazy_ids'], $data->id);
        return $this->update($input, $data->id);
    }

    public function import($file)
    {
        set_time_limit(9999);
        $path = $this->makeModel()->uploadImport($file);
        return $this->importing($path);
    }

    private function standardized($input, $data)
    {
        return $data->uploads($input);
    }

    public function destroy($data)
    {
        return $this->delete($data->id);
    }

    /**
     * Boot up the repository, ping criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
