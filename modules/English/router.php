<?php
/**
 * Created by PhpStorm.
 * User: CPM
 * Date: 6/12/2018
 * Time: 10:21 PM
 */

Route::group(['middleware' => ['web'], 'namespace' => 'English\Http\Controllers', 'prefix' => 'english'], function () {

    Route::group(['prefix' => 'test', 'namespace' => 'Test'], function () {
        Route::get('fill-in-the-blanks', 'FillInTheBlankTestController@index')->name('test.fill-in-the-blank.index');
        Route::post('fill-in-the-blanks', 'FillInTheBlankTestController@done')->name('test.fill-in-the-blank.done');
        Route::get('pronunciations', 'PronunciationTestController@index')->name('test.pronunciation.index');
        Route::post('pronunciations', 'PronunciationTestController@done')->name('test.pronunciation.done');
        Route::get('mistakes', 'MistakeTestController@index')->name('test.mistake.index');
        Route::post('mistakes', 'MistakeTestController@done')->name('test.mistake.done');
        Route::get('similarities', 'SimilarityTestController@index')->name('test.similarity.index');
        Route::post('similarities', 'SimilarityTestController@done')->name('test.similarity.done');
        Route::get('crazies', 'CrazyTestController@index')->name('test.crazy.index');
        Route::get('crazies/{id}', 'CrazyTestController@doing')->name('test.crazy.doing');
        Route::post('crazies/{id}', 'CrazyTestController@done')->name('test.crazy.done');
        Route::get('crazy-writings/{id}', 'CrazyTestController@writing')->name('test.crazy.writing');
        Route::post('crazy-written/{id}', 'CrazyTestController@written')->name('test.crazy.written');
    });

    Route::get('/', 'EnglishController@index')->name('english.index');

    Route::group(['middleware' => ['auth:admin', 'role:admin']], function () {
        Route::resource('crazy-write-histories' , 'CrazyWriteHistoryController');
        Route::resource('fill-in-the-blanks', 'FillInTheBlankController');
        Route::resource('pronunciations', 'PronunciationController');
        Route::resource('mistakes', 'MistakeController');
        Route::resource('vocabularies', 'VocabularyController');
        Route::resource('similarities', 'SimilarityController');
        Route::resource('crazies', 'CrazyController');
        Route::resource('crazy-details', 'CrazyDetailController');
        Route::resource('crazy-histories', 'CrazyHistoryController');
        Route::resource('crazy-courses', 'CrazyCourseController');
    });

    Route::group(['prefix' => 'involve/vocabulary', 'namespace' => 'Involve'], function () {
        Route::post('imports', 'VocabularyController@import')
            ->name('involve.vocabulary.import')
            ->middleware(['auth:admin', 'role:admin']);
        Route::get('searches', 'VocabularyController@search')->name('involve.vocabulary.search');
    });

    Route::group(['prefix' => '', 'namespace' => 'Frontend'], function () {
        Route::get('crazy-course-list/{id}', 'CrazyCourseController@getList')->name('frontend.crazy-course.list');
    });
});

Route::get('reg', function () {
    $pattern = '/[a-zA-Z]{1}/';
    $subject = 'Can I borrow $5?';
    echo preg_replace($pattern, '*', $subject);
});