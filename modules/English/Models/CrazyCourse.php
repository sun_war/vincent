<?php

namespace English\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class CrazyCourse extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'crazy_courses';
    public $fillable = ['name', IMG_COL, 'description', 'is_active', 'created_by', 'updated_by'];

    public function scopeFilter($query, $input)
    {
        foreach ($this->fillable as $value) {
            if (isset($input[$value])) {
                $query->where($value, $input[$value]);
            }
        }
        return $query;
    }

    public function crazies()
    {
        return $this->hasMany(Crazy::class, CRAZY_COURSE_ID_COL);
    }

    public $fileUpload = [IMG_COL => 1];
    protected $pathUpload = [IMG_COL => '/images/crazy_courses'];
    protected $thumbImage = [
        IMG_COL => [
            '/thumbs/' => [
               [200, 200],
            ]
        ]
    ];
}

