<?php

namespace English\Models;

use Illuminate\Database\Eloquent\Model;
use Modularization\MultiInheritance\ModelsTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Crazy extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'crazies';
    public $fillable = [NAME_COL, AUDIO_COL, CREATED_BY_COL, UPDATED_BY_COL, IS_ACTIVE_COL, CRAZY_COURSE_ID_COL];

    public function scopeFilter($query, $input)
    {
        foreach ($this->fillable as $value) {
            if (isset($input[$value])) {
                $query->where($value, $input[$value]);
            }
        }
        return $query;
    }

    public function details()
    {
        return $this->hasMany(CrazyDetail::class, CRAZY_ID_COL);
    }

    public function crazyCourse()
    {
        return $this->belongsTo(CrazyCourse::class, CRAZY_COURSE_ID_COL);
    }

    public function getAudioPath()
    {
        return asset('storage' . $this[AUDIO_COL]);
    }

    public $fileUpload = [AUDIO_COL => 0];
    protected $pathUpload = [AUDIO_COL => '/audio/crazies'];
    protected $thumbImage = [];
}

