<?php

namespace English\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Mistake extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'mistakes';
    public $fillable = [QUESTION_COL, A_COL, B_COL, C_COL, D_COL, ANSWER_COL, REPAIR_COL, IS_ACTIVE_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input[QUESTION_COL])) {
            $query->where(QUESTION_COL, 'LIKE', '%' . $input[QUESTION_COL] . '%');
        }
        if (isset($input[A_COL])) {
            $query->where(A_COL, $input[A_COL]);
        }
        if (isset($input[B_COL])) {
            $query->where(B_COL, $input[B_COL]);
        }
        if (isset($input[C_COL])) {
            $query->where(C_COL, $input[C_COL]);
        }
        if (isset($input[D_COL])) {
            $query->where(D_COL, $input[D_COL]);
        }
        if (isset($input[ANSWER_COL])) {
            $query->where(ANSWER_COL, $input[ANSWER_COL]);
        }
        if (isset($input[REPAIR_COL])) {
            $query->where(REPAIR_COL, $input[REPAIR_COL]);
        }
        if (isset($input[IS_ACTIVE_COL])) {
            $query->where(IS_ACTIVE_COL, $input[IS_ACTIVE_COL]);
        }
        return $query;
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/mistakes'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];

    protected $checkbox = [IS_ACTIVE_COL];
}

