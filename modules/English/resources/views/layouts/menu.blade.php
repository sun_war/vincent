<li class="has-sub root-level" id="englishMenu">
    <a>
        <i class="fa fa-language"></i>
        <span class="title">{{__('menu.english')}}</span>
    </a>
    <ul>
        <li  id="pronunciationMenu">
            <a href="{{route('pronunciations.index')}}">
                <span class="title">{{__('table.pronunciations')}}</span>
            </a>
        </li>
        <li  id="mistakeMenu">
            <a href="{{route('mistakes.index')}}">
                <span class="title">{{__('table.mistakes')}}</span>
            </a>
        </li>
        <li  id="fillInTheBlankMenu">
            <a href="{{route('fill-in-the-blanks.index')}}">
                <span class="title">{{__('table.fill_in_the_blanks')}}</span>
            </a>
        </li>
        <li  id="similarityMenu">
            <a href="{{route('similarities.index')}}">
                <span class="title">{{__('table.similarities')}}</span>
            </a>
        </li>
        <li  id="crazyCourseMenu">
            <a href="{{route('crazy-courses.index')}}">
                <span class="title">Crazy course</span>
            </a>
        </li>
        <li  id="crazyMenu">
            <a href="{{route('crazies.index')}}">
                <span class="title">Crazy</span>
            </a>
        </li>
        <li id="vocabularyMenu">
            <a href="{{route('vocabularies.index')}}">
                <i class="entypo-inbox"></i>
                <span class="title">Vocabulary</span>
            </a>
        </li>
    </ul>
</li>