<?php

namespace Test\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use Test\Models\QuestionResult;
use Test\Http\Requests\QuestionResultCreateRequest;
use Test\Http\Requests\QuestionResultUpdateRequest;
use Test\Http\Repositories\QuestionResultRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class QuestionResultController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(QuestionResultRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['questionResults'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('test::question-result.table', $data)->render();
        }
        return view('test::question-result.index', $data);
    }

    public function create()
    {
        return view('test::question-result.create');
    }

    public function store(QuestionResultCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('question-result.index');
    }

    public function show($id)
    {
        $questionResult = $this->repository->find($id);
        if(empty($questionResult))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('test::question-result.show', compact('questionResult'));
    }

    public function edit($id)
    {
        $questionResult = $this->repository->find($id);
        if(empty($questionResult))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('test::question-result.update', compact('questionResult'));
    }

    public function update(QuestionResultUpdateRequest $request, $id)
    {
        $input = $request->all();
        $questionResult = $this->repository->find($id);
        if(empty($questionResult))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $questionResult);
        session()->flash('success', 'update success');
        return redirect()->route('question-result.index');
    }

    public function destroy($id)
    {
        $questionResult = $this->repository->find($id);
        if(empty($questionResult))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
