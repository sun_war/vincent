<?php

namespace Test\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use Test\Models\TestResult;
use Test\Http\Requests\TestResultCreateRequest;
use Test\Http\Requests\TestResultUpdateRequest;
use Test\Http\Repositories\TestResultRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class TestResultController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(TestResultRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['testResults'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('test::test-result.table', $data)->render();
        }
        return view('test::test-result.index', $data);
    }

    public function create()
    {
        return view('test::test-result.create');
    }

    public function store(TestResultCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('test-result.index');
    }

    public function show($id)
    {
        $testResult = $this->repository->find($id);
        if(empty($testResult))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('test::test-result.show', compact('testResult'));
    }

    public function edit($id)
    {
        $testResult = $this->repository->find($id);
        if(empty($testResult))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('test::test-result.update', compact('testResult'));
    }

    public function update(TestResultUpdateRequest $request, $id)
    {
        $input = $request->all();
        $testResult = $this->repository->find($id);
        if(empty($testResult))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $testResult);
        session()->flash('success', 'update success');
        return redirect()->route('test-result.index');
    }

    public function destroy($id)
    {
        $testResult = $this->repository->find($id);
        if(empty($testResult))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
