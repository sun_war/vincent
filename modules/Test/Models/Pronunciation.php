<?php

namespace English\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Pronunciation extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'pronunciations';
    public $fillable = ['a', 'b', 'c', 'd', 'pronunciation_a', 'pronunciation_b', 'pronunciation_c', 'pronunciation_d', 'answer', 'reason', 'is_active'];

    public function scopeFilter($query, $input)
    {
        if(isset($input['a'])) {
                $query->where('a', $input['a']); 
                }
if(isset($input['b'])) {
                $query->where('b', $input['b']); 
                }
if(isset($input['c'])) {
                $query->where('c', $input['c']); 
                }
if(isset($input['d'])) {
                $query->where('d', $input['d']); 
                }
if(isset($input['pronunciation_a'])) {
                $query->where('pronunciation_a', $input['pronunciation_a']); 
                }
if(isset($input['pronunciation_b'])) {
                $query->where('pronunciation_b', $input['pronunciation_b']); 
                }
if(isset($input['pronunciation_c'])) {
                $query->where('pronunciation_c', $input['pronunciation_c']); 
                }
if(isset($input['pronunciation_d'])) {
                $query->where('pronunciation_d', $input['pronunciation_d']); 
                }
if(isset($input['answer'])) {
                $query->where('answer', $input['answer']); 
                }
if(isset($input['reason'])) {
                $query->where('reason', $input['reason']); 
                }
if(isset($input['is_active'])) {
                $query->where('is_active', $input['is_active']); 
                }

        return $query;
    }


    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/pronunciations'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

