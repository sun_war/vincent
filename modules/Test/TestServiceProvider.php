<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 1/15/18
 * Time: 2:17 PM
 */

namespace Test;

use Illuminate\Support\ServiceProvider;
use Test\Http\Repositories\MultiChoiceRepository;
use Test\Http\Repositories\MultiChoiceRepositoryEloquent;
use Test\Http\Repositories\QuestionResultRepository;
use Test\Http\Repositories\QuestionResultRepositoryEloquent;
use Test\Http\Repositories\TestResultRepository;
use Test\Http\Repositories\TestResultRepositoryEloquent;

class TestServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'test');
        $this->loadRoutesFrom(__DIR__ . '/router.php');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        if ($this->app->runningInConsole()) {
            $this->commands([
//                ForceDBCommand::class
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MultiChoiceRepository::class, MultiChoiceRepositoryEloquent::class);
        $this->app->bind(TestResultRepository::class, TestResultRepositoryEloquent::class);
        $this->app->bind(QuestionResultRepository::class, QuestionResultRepositoryEloquent::class);
    }
}