<form class="form-group row" id="formFilter" action="{{route('multi_choices.index')}}" method="POST">
    <div class="col-sm-5 form-group">
        <label for="question">{{trans('label.question')}}</label>
        <input  name="question" class="form-control inputFilter" placeholder="question">
    </div>
    <div class="form-group col-lg-1">
        <label for="level">{{trans('label.level')}}</label>
        <select class="form-control selectFilter" name="level" id="level">
            <option value="">All</option>
            @foreach(LEVEL as $value)
                <option value="{{$value}}">{{$value}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-lg-2">
        <label for="knowledge">{{trans('label.knowledge')}}</label>
        <select class="form-control selectFilter" name="knowledge" id="knowledge">
            <option value="">All</option>
            @foreach(KNOWLEDGE as $id => $value)
                <option value="{{$id}}">{{$value}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-lg-2">
        <label for="professional">{{trans('label.professional')}}</label>
        <select class="form-control selectFilter" name="professional" id="professional">
            <option value="">All</option>
            @foreach(PROFESSIONAL as $id => $value)
                <option value="{{$id}}">{{$value}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-sm-2 form-group">
        <label>{{trans('label.action')}}</label>
        <div>
            <a class="btn btn-primary btn-group" href="{{route('multi_choices.create')}}">
                <i class="fa fa-plus"></i>
            </a>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-upload"></i> Upload
            </button>
        </div>
    </div>
</form>