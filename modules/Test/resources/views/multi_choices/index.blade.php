@extends('layouts.app')
@section('title')
    Marked
@endsection
@section('content')
    <ol class="breadcrumb">
        <li>
            <a href="/"><i class="fa fa-home"></i>{{__('common.home')}}</a>
        </li>
        <li>
            <a>{{__('table.multi_choices')}}</a>
        </li>
    </ol>

    @include('test::multi_choices.filter')

    <div id="table">
        @include('test::multi_choices.table')
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload excel</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="doingQuestion" tabindex="-1" role="dialog" aria-labelledby="doingQuestionLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="doingQuestionLabel">Do question</h4>
                </div>
                <div class="modal-body" id="doingQuestionBody">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">&laquo;</button>
                    <button type="button" class="btn btn-primary">&raquo;</button>
                    <button type="button" class="btn btn-primary" id="questionDoneBtn">{{__('button.done')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('button.close')}}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
<script src="{{asset('build/form-filter.js')}}"></script>
<script>
    const doingQuestionBody = '#doingQuestionBody';
    const doingQuestionBtn = '.doingQuestionBtn';
    $(document).on('click', doingQuestionBtn, function () {
        const self = $(this);
        const url = self.attr('url');
        $.ajax({
            url: url,
            method: 'GET',
            success: (data) => {
                $(doingQuestionBody).html(data)
            },
            error: () => {
               alert('error')
            }
        });
    });
    const questionDoneBtn = '#questionDoneBtn'
</script>
@endpush