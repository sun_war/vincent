<li class="has-sub root-level" id="testMenu">
    <a>
        <i class="fa fa-pencil"></i>
        <span class="title">Test</span>
    </a>
    <ul>
        <li id="multiChoiceMenu">
            <a href="{{route('multi_choices.index')}}">
                <i class="entypo-newspaper"></i>
                <span class="title">{{__('menu.multi_choice')}}</span>
            </a>
        </li>
        <li class="{{ request()->is('test-result*') ? 'active' : ''}}" id="testResultMenu">
            <a href="{{route('test-result.index')}}">
                <span class="title"><i class="fa fa-edit"></i> {{__('table.test_results')}}</span>
            </a>
        </li>
        <li class="{{ request()->is('question-result*') ? 'active' : ''}}" id="questionResultMenu">
            <a href="{{route('question-result.index')}}">
                <span class="title"><i class="fa fa-edit"></i> {{__('table.question_results')}}</span>
            </a>
        </li>
    </ul>
</li>