@extends('layouts.app')
@section('content')
<div class="row">
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('test-result.index')}}">{{trans('table.test_results')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.create')}}</strong>
        </li>
    </ol>

    <form action="{{route('test-result.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group col-lg-6">
    <label for="score">{{trans('label.score')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" checked value="1" name="score"  id="score" >
        </label>
    </div>
</div>

<div class="form-group col-lg-6">
    <label for="created_by">{{trans('label.created_by')}}</label>
    <input type="number" class="form-control" name="created_by"  id="created_by">
</div>
<div class="form-group col-lg-6">
    <label for="level">{{trans('label.level')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" checked value="1" name="level"  id="level" >
        </label>
    </div>
</div>

<div class="form-group col-lg-6">
    <label for="knowledge">{{trans('label.knowledge')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" checked value="1" name="knowledge"  id="knowledge" >
        </label>
    </div>
</div>

<div class="form-group col-lg-6">
    <label for="professional">{{trans('label.professional')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" checked value="1" name="professional"  id="professional" >
        </label>
    </div>
</div>

<div class="form-group col-lg-6">
    <label for="page">{{trans('label.page')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" checked value="1" name="page"  id="page" >
        </label>
    </div>
</div>

<div class="form-group col-lg-6">
    <label for="status">{{trans('label.status')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" checked value="1" name="status"  id="status" >
        </label>
    </div>
</div>


        <div class="col-lg-12">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection