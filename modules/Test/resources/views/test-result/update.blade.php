@extends('layouts.app')
@section('content')
<div class="row">
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('test-result.index')}}">{{trans('table.test_results')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.update')}}</strong>
        </li>
    </ol>
    <form action="{{route('test-result.update', $testResult->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-6">
    <label for="score">{{trans('label.score')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" {{$testResult->score !== 1 ?: 'checked'}} name="score"  id="score" value="1">
        </label>
    </div>
</div>
<div class="form-group col-lg-6">
    <label for="created_by">{{trans('label.created_by')}}</label>
    <input type="number" class="form-control" name="created_by"  id="created_by" value="{{$testResult->created_by}}">
</div>
<div class="form-group col-lg-6">
    <label for="level">{{trans('label.level')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" {{$testResult->level !== 1 ?: 'checked'}} name="level"  id="level" value="1">
        </label>
    </div>
</div>
<div class="form-group col-lg-6">
    <label for="knowledge">{{trans('label.knowledge')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" {{$testResult->knowledge !== 1 ?: 'checked'}} name="knowledge"  id="knowledge" value="1">
        </label>
    </div>
</div>
<div class="form-group col-lg-6">
    <label for="professional">{{trans('label.professional')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" {{$testResult->professional !== 1 ?: 'checked'}} name="professional"  id="professional" value="1">
        </label>
    </div>
</div>
<div class="form-group col-lg-6">
    <label for="page">{{trans('label.page')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" {{$testResult->page !== 1 ?: 'checked'}} name="page"  id="page" value="1">
        </label>
    </div>
</div>
<div class="form-group col-lg-6">
    <label for="status">{{trans('label.status')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" {{$testResult->status !== 1 ?: 'checked'}} name="status"  id="status" value="1">
        </label>
    </div>
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection