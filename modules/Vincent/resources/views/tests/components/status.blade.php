<div class="modal fade doing" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Tình trạng làm bài </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    @isset($exactly)
                        @foreach($questions as $k => $question)
                            <div class="col-sm-2 col-xs-2 col-md-1 form-group">
                                <a href="#{{$k++}}" id="check{{$k}}" class="btn btn-sm {{$exactly[$question->id] ? 'btn-success' : 'btn-default'}} btn-block">{{$k}}</a>
                            </div>
                        @endforeach
                    @else
                        @isset($questions)
                            @for($i=1; $i<= count($questions); $i++)
                                <div class="col-sm-2 col-xs-2 col-md-1 form-group">
                                    <a href="#{{$i}}" id="check{{$i}}" class="btn btn-sm btn-default btn-block">{{$i}}</a>
                                </div>
                            @endfor
                        @endisset
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>