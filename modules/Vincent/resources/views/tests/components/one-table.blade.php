<div  id="table">
    <form id="formTest" action="{{route('test.one-mark', $questions[0]->id)}}?page={{$questions->currentPage()}}" method="POST">
        {{csrf_field()}}
        <div class="text-center" id="linkPaginate">
            {!! $questions->links() !!}
        </div>
        @foreach($questions as $k=> $question)
            <input type="hidden" value="{{$question->id}}" name="id">
            <div class="form-group text-info">@markdown($question->question)</div>
            <table class="table">
                @if($question->answer > 5)
                    @foreach(REP_LIST as $i => $rep)
                        @if(trim($question->$rep) !== '')
                            <tr>
                                <td width="20px">
                                    <input type="checkbox" value="{{$i}}" class="done" data="{{$k}}"
                                           name="answer[]"></td>
                                <td>{{trim($question->$rep)}}</td>
                            </tr>
                        @endif
                    @endforeach
                @else
                    @foreach(REP_LIST as $i => $rep)
                        @if(trim($question->$rep) !== '')
                            <tr>
                                <td width="20px">
                                    <input type="radio" value="{{$i}}" class="done" data="{{$k}}"
                                           name="answer">
                                </td>
                                <td>{{trim($question->$rep)}}</td>
                            </tr>
                        @endif
                    @endforeach
                @endif
            </table>
        @endforeach
        <div class="text-center">
            <button class="btn btn-success btn-icon" id="mark"> Xong <i class="fa fa-check-circle"></i></button>
        </div>
    </form>
</div>


