@extends('edu::layouts.app')
@section('title', 'Ngoại ngữ')
@section('content')
    <h1>Tra cứu với hơn 3000 từ tiếng anh thông dụng, giúp bạn luyện tập từ vựng tiếng anh trọng tâm nhất</h1>
    <form class="form-group row" id="formFilter" action="{{route('edu.language.list')}}" method="POST">
        <div class="col-sm-4 form-group">
            <input  name="word" class="form-control inputFilter" placeholder="word">
        </div>
        <div class="col-sm-4 form-group">
            <input  name="type" class="form-control inputFilter" placeholder="type">
        </div>
        <div class="col-sm-4 form-group">
            <input  name="meaning" class="form-control inputFilter" placeholder="meaning">
        </div>
    </form>

    <div id="table" class="row">
        @include('edu::languages.includes.paginate')
    </div>

@endsection
@push('head')

@endpush
@push('js')
    <script src="{{asset('build/form-filter.js')}}"></script>
@endpush


