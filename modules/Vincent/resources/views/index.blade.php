@extends('edu::layouts.app')
@section('content')
    <!-- Main Slider -->
    @include('edu::layouts.components.slides')
    <!-- Features Blocks -->
    @include('edu::layouts.components.features')
    @include('edu::layouts.components.tutorial')
    @include('edu::layouts.components.quotations')
    @include('edu::layouts.components.expert')
    <section>
        <div class="row">
            <div class="feature-block col-lg-12">
                <h1>
                    <i class="entypo-cog"></i>
                    Thông tin về chúng tôi
                </h1>
            </div>
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="embed-responsive embed-responsive-16by9">
                    {!! $about->map !!}
                </div>
            </div>

            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="tile-group tile-white">
                    <div class="tile-entry">
                        <h4>Address</h4>
                        <p>
                            {{$about->address}}
                        </p>
                    </div>
                    <div class="tile-entry">
                        <h4 class="form-group">Contact</h4>
                        <p>
                            Name: Vincent Minh Cương <br/>
                            Phone: {{$about->phone_number}} <br/>
                            Email: {{$about->email}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="footer-widgets">
        <div class="row">
            <div class="col-sm-12">
                <h3><a href="#">{{config('app.name')}}</a></h3>
                <p>
                    {{config('app.name')}} là hệ thống học tập, trao đổi kiến thức, kinh nghiệm, đặc biệt là trải
                    nghiệm nhẹ nhàng
                    từng bước một tiến thẳng tới thế giới IT đầy ma thuật.
                </p>
            </div>
        </div>
    </section>
@endsection

@push('js')
    <script>
        $('.carousel').carousel({
            interval: 10000
        })
    </script>
@endpush