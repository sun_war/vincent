<section class="features-blocks row">

    <div class=" vspace"><!-- "vspace" class is added to distinct this row -->
        <div class="col-sm-4">
            <div class="feature-block">
                <h3>
                    <i class="entypo-cog"></i>
                    Nội dung
                </h3>
                <p>
                    Hệ thống bài học sát với thực tế, đa phân khúc ứng với mọi đối tượng từ người chưa biết, đã có kinh
                    nghiệm làm việc, hoặc muốn trở thành chuyên gia
                </p>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="feature-block">
                <h3>
                    <i class="entypo-gauge"></i>
                    Trải nghiệm
                </h3>
                <p>
                    24/7 Tương tác với hệ thống mọi lúc mọi nơi, cực kì dễ dàng và đặc biệt hoàn toàn miễn phí. Cơ hội
                    giao lưu, học hỏi, tìm được người cùng chung chí hướng
                </p>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="feature-block">
                <h3>
                    <i class="entypo-lifebuoy"></i>
                    Hỗ trợ
                </h3>
                <p>
                    Dễ dàng đóng góp ý kiến, nhận được sự phải hồi và chăm sóc của đội ngũ phát triển hay chính công
                    đồng người sử dụng.
                </p>
            </div>
        </div>
    </div>
    <!-- Separator -->
    <div class="col-md-12">
        <hr/>
    </div>
</section>