<section class="slider-container row form-group">
    <div class="col-md-12">
        <div id="sliders" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                @foreach($slides as $k => $slide)
                    <li data-target="#sliders" data-slide-to="{{$k}}" class="{{$k == 0 ? 'active' : ''}}"></li>
                @endforeach
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                @foreach($slides as $k => $slide)
                    <div class="item {{$k == 0 ? 'active' : ''}}">
                        <img src="{{ \ViewFa::thumb($slide->image, [1200, 640])}}" alt="{{$slide->name}}">
                        <div class="carousel-caption">
                            {!! $slide->content !!}
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#sliders" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#sliders" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <br>
</section>