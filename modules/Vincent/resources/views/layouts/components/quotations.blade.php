<section class="quotation-container row  form-group">
    <div class="feature-block col-lg-12">
        <h3>
            <i class="entypo-cog"></i>
            Danh ngôn về IT
        </h3>
        <p>
            Những câu danh ngôn hay về thế giới IT
        </p>
    </div>
    <div class="col-md-12 form-group">
        <div id="quotations" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach($quotations as $k => $quotation)
                    <li data-target="#quotations" data-slide-to="{{$k}}"
                        class="{{$k == 0 ? 'active' : ''}}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner" role="listbox">
                @foreach($quotations as $k => $quotation)
                    <div class="item {{$k == 0 ? 'active' : ''}}">
                        <img src="{{ \ViewFa::thumb($quotation->image, [1200, 400])}}" alt="quotations">
                        <div class="carousel-caption">
                            <h4 style="color: whitesmoke; ">{!! $quotation->content !!}</h4>
                            <h5 class="text-info">
                                <cite>{{$quotation['author']}}</cite> - {{$quotation->job}}
                            </h5>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <br>
</section>