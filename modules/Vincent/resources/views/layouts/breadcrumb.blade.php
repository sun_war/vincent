<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h1>Blog</h1>
                <ol class="breadcrumb" >
                    <li>
                        <a href="{{route('admin')}}"><i class="entypo-home></i></a>
                    </li>
                    <li>
                        <a href="{{route('admin')}}">Frontend</a>
                    </li>
                    <li class="active">
                        <strong>Blog</strong>
                    </li>
                </ol>
            </div>
            <div class="col-sm-3">
                <h2 class="text-muted text-right">86 Posts</h2>
            </div>
        </div>
    </div>
</section>