<header class="navbar navbar-fixed-top"><!-- set fixed position by adding class "navbar-fixed-top" -->
    <div class="navbar-inner">
        <div class="navbar-brand">
            <a href="/" style="color: whitesmoke">
                <strong>{{config('app.name')}}</strong>
            </a>
        </div>
        <ul class="navbar-nav">
            <li>
                <a href="{{route('edu.tutorial.index')}}">
                    <span><i class="entypo-docs"></i> {{__('nav.tutorial')}}</span>
                </a>
            </li>
            <li>
                <a href="{{route('edu.test.list')}}">
                    <span><i class="entypo-graduation-cap"></i> {{__('nav.test')}}</span>
                </a>
            </li>
            <li class="has-sub">
                <a>
                    <i class="fa fa-chain"></i>
                    <span class="title">Blockchain</span>
                </a>
                <ul>
                    <li>
                        <a href="{{route('wallet')}}">
                            <span class="title"><i class="fa fa-google-wallet"></i> Wallet</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('exchange')}}">
                            <span class="title"><i class="fa fa-exchange"></i> Exchange</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('eco.product.show')}}">
                            <span class="title"><i class="fa fa-shopping-cart"></i> Shop</span>
                        </a>
                    </li>
                </ul>
            </li>

            {{--<li>--}}
                {{--<a href="{{route('eth')}}">--}}
                    {{--<span><i class="fa fa-money"></i> Ethereum</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="{{route('game.index')}}">--}}
                    {{--<span><i class="entypo-play"></i> Game</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li>
                <a href="{{route('english.index')}}">
                    <span><i class="fa fa-language"></i> English</span>
                </a>
            </li>
            <li>
                <a href="{{route('review')}}">
                    <span><i class="fa fa-star"></i> Review</span>
                </a>
            </li>
            <li>
                <a href="{{route('msg')}}">
                    <span><i class="fa fa-home"></i> Chat room</span>
                </a>
            </li>
            {{--<li>--}}
                {{--<a href="{{route('charts')}}">--}}
                    {{--<span><i class="entypo-chart-line"></i> Shark tank</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li>
                <a href="{{route('contact')}}">
                    <span><i class="entypo-mail"></i> {{__('nav.contact')}}</span>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav navbar-right">
            @if(auth()->check())
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <span  style="color: palegoldenrod">
                            <strong>
                            <span id="moneyTotal">{{number_format(auth()->user()->coin)}}</span>
                                <i class="fa fa-bitcoin"></i>
                                {{auth()->user()->last_name}}
                            </strong>
                        </span>
                        <i class="entypo-user"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <a href="{{route('acl.profile')}}">
                                <i class="entypo-user"></i> {{__('auth.profile')}}
                            </a>
                        </li>
                        <li class="sep"></li>
                        <li>
                            <a onclick="$('#logoutForm').submit()">
                                <i class="entypo-logout"></i>
                            </a>
                            <form id="logoutForm" style="display: none" method="post" action="{{route('logout')}}" >
                                {!! csrf_field() !!}
                            </form>
                        </li>
                    </ul>
                </li>
            @else
                <li>
                    <a href="{{asset('login')}}">
                        <span>
                            {{__('auth.login')}} <i class="entypo-login"></i>
                        </span>
                    </a>
                </li>
                <li class="sep"></li>
                <li>
                    <a href="{{asset('register')}}">
                        <span>
                            {{__('auth.register')}} <i class="entypo-user-add"></i>
                        </span>
                    </a>
                </li>
            @endif
        </ul>
        <ul class="nav navbar-right pull-right">
            <li class="visible-xs">
                <div class="horizontal-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <i class="entypo-menu"></i>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</header>