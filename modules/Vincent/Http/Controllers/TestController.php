<?php
/**
 * Created by PhpStorm.
 * User: JK
 * Date: 2/14/2018
 * Time: 10:12 PM
 */

namespace Vincent\Http\Controllers;

use Illuminate\Http\Request;
use Test\Http\Repositories\MultiChoiceRepository;
use Tutorial\Models\Lesson;
use Tutorial\Models\LessonResult;
use Tutorial\Models\LessonTest;
use Repositories\Http\Repositories\LessonResultRepository;
use Repositories\Http\Repositories\LessonTestRepository;
use Vincent\Core\Services\TestService;

class TestController
{
    private $testService, $multiChoiceRepository;

    public function __construct(TestService $testService, MultiChoiceRepository $multiChoiceRepository)
    {
        $this->testService = $testService;
        $this->multiChoiceRepository = $multiChoiceRepository;
    }

    public function getList(Request $request)
    {
        $input = $request->all();
        $data ['count'] = $this->testService->countList($input);
        if ($request->ajax()) {
            return view('edu::tests.includes.list-unit', $data)->render();
        }
        return view('edu::tests.list', $data);
    }

    public function doing(Request $request)
    {
        $input = $request->all();
        $input['questions'] = $this->testService->getTest($input);
        return view('edu::tests.doing', $input);
    }

    public function marking(Request $request)
    {
        $input = $request->all();
        $data = $this->testService->marking($request);
        $message = 'Số câu bạn trả lời đúng là: ' . $data[SCORE_COL] . '/' . count($data['questions']);
        session()->flash('global', $message);
        return view('edu::tests.marked', $data)->withInput($input);
    }

    public function result(Request $request)
    {
        $input = $request->all();
        $data = $this->testService->result($input);
        return view('edu::histories.tutorial.result', $data);
    }

    public function lesson($lesson_id)
    {
        $lesson = app(Lesson::class)
            ->with(['section:id,name,tutorial_id', 'section.tutorial:id,name'])
            ->find($lesson_id);
        if (empty($lesson)) {
            session()->flash('success', 'Lesson not found');
            return back();
        }
        $questions = app(LessonTestRepository::class)
            ->filterGet([LESSON_ID_COL => $lesson_id]);

        return view('edu::tests.lesson', compact('questions', 'lesson'));
    }

    public function lessonResult($lesson_id, Request $request)
    {
        $answers = $request->answers;
        $questions = app(LessonTest::class)->where(LESSON_ID_COL, $lesson_id)->get();
        $totalQuestion = count($questions);
        $mark = $this->testService->scoreLesson($questions, $answers);
        $this->history($totalQuestion, $lesson_id, $mark);
        session()->flash('global', "Bạn làm đúng {$mark[SCORE_COL]}/{$totalQuestion} câu hỏi");
        $data = array_merge($mark, compact('questions', LESSON_ID_COL, 'answers'));
        return view('edu::tests.lesson-result', $data);
    }

    private function history($totalQuestion, $lesson_id, $mark)
    {
        if (auth()->check() && $totalQuestion > 0) {
            $result = app(LessonResultRepository::class)
                ->filterFirst([
                    CREATED_BY_COL => auth()->id(),
                    LESSON_ID_COL => $lesson_id
                ]);

            $score = $mark[SCORE_COL] * 1000 / $totalQuestion;

            if (empty($result)) {
                $this->createLessonResult($score, $lesson_id);
                $increment = pow(2, (int) $score / 1000);
            } else {
                $result->score = $score;
                $result->save();
                $increment = (int) $score / 1000;
            }

            $user = auth()->user();
            $user->coin += $increment;
            $user->save();

        }
    }

    private function createLessonResult($score, $lesson_id)
    {
        $resultData = [
            SCORE_COL => $score,
            CREATED_BY_COL => auth()->id(),
            LESSON_ID_COL => $lesson_id,
        ];
        app(LessonResult::class)->create($resultData);
    }

    public function question()
    {
        $questions = $this->multiChoiceRepository->with(['result' => function ($query) {
            $query->where(CREATED_BY_COL, auth()->id());
        }])->paginate(25);
        return $questions;
    }

    public function questionResult(Request $request, $id)
    {

        $answer = $request->get(ANSWER_COL);
        $question = $this->multiChoiceRepository->find($id);
        if ($question->answer === $answer) {
            $data = [
                QUESTION_ID_COL => $id,
                CREATED_BY_COL => auth()->id()
            ];
            $result = $this->multiChoiceRepository->findWhere($data);
            if (empty($result)) {
                $this->multiChoiceRepository->create($data);
            }
        }
        return back();
    }
}