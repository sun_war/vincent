<?php

namespace Vincent\Http\Controllers;

use App\Http\Controllers\Controller;
use Tutorial\Models\Lesson;
use Tutorial\Models\Section;
use Tutorial\Models\Tutorial;

class TutorialController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $tutorials = app(Tutorial::class)
            ->select([ID_COL, NAME_COL, DESCRIPTION_COL, IMG_COL])
            ->where(IS_ACTIVE_COL, 1)
            ->orderBy(ID_COL, "DESC")
            ->get();
        return view('edu::tutorials.index', compact('tutorials'));
    }

    public function show($id)
    {
        $tutorial = app(Tutorial::class)->find($id);
        if (empty($tutorial)) {
            session()->flash('error', 'Category not found');
        }
        $sections = app(Section::class)
            ->where(TUTORIAL_ID_COL, $id)
            ->with(['lessons' => function ($query) {
                $query->orderBy(NO_COL, 'ASC')->where(IS_ACTIVE_COL, 1);
            }, 'lessons.results' => function ($query) {
                $query->where(CREATED_BY_COL, auth()->id());
            }])
            ->orderBy(NO_COL, 'ASC')
            ->get();
        return view('edu::tutorials.show', compact('sections', 'tutorial'));
    }

    public function section($id)
    {
        $sections = app(Section::class)
            ->where(TUTORIAL_ID_COL, $id)
            ->withCount('lessons')
            ->get(['id', 'name']);
        return view('edu::tutorials.section', compact('sections'));
    }

    public function lesson($id)
    {
        $lesson = app(Lesson::class)->find($id);
        if(empty($lesson)) {
            session()->flash('error', 'Lesson not found');
            return redirect()->route('home');
        }
        $lesson->views += 1;
        $lesson->last_view = date('Y-m-d H:i:s');
        $lesson->save();
        $section = $lesson->section;
        $lessonList = $section->lessons()->orderBy(NO_COL, 'ASC')
            ->where(IS_ACTIVE_COL, 1)
            ->pluck('title', 'id');
        $data = compact('lesson', 'section', 'lessonList');
        return view('edu::tutorials.lesson', $data);
    }
}
