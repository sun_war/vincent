<?php

namespace Vincent\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Http\Requests\ContactCreateRequest;
use Landing\Models\About;
use Landing\Models\Contact;
use Landing\Models\Quotation;
use Landing\Models\Slide;
use Tutorial\Models\Tutorial;

class VincentController extends Controller
{
    public function __construct()
    {
    }

    private $contact = [
        PHONE_NUMBER_COL => '01659003851',
        ADDRESS_COL => 'Tân Chi - Tiên Du - Bắc Ninh',
        EMAIL_COL => 'i.am.m.cuong@gmail.com',
        MAP_COL => '<iframe class="embed-responsive-item" width="100%"
                                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7420.005376567759!2d105.80816198672962!3d21.585818161023987!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x515f4860ede9e108!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBDw7RuZyBuZ2jhu4cgVGjDtG5nIHRpbiB2w6AgVHJ1eeG7gW4gdGjDtG5n!5e0!3m2!1sen!2s!4v1526117791253"
                                frameborder="0" style="border:0" allowfullscreen></iframe>',
        INFO_COL => '',
    ];

    public function index()
    {
        $slides = app(Slide::class)->where(IS_ACTIVE_COL, 1)->orderBy(NO_COL)->get();
        $features = [

        ];
        $about = app(About::class)
            ->where(IS_ACTIVE_COL, 1)
            ->orderBy(ID_COL, 'DESC')
            ->first();
        if (empty($about)) {
            $about = (object)($this->contact);
        }
        $quotations = app(Quotation::class)
            ->where(IS_ACTIVE_COL, 1)
            ->orderBy(NO_COL)
            ->get();
        $tutorials = app(Tutorial::class)
            ->where(IS_ACTIVE_COL, 1)
            ->limit(6)
            ->get();
        return view('edu::index', compact('quotations', 'features', 'slides', 'tutorials', 'about'));
    }

    public function contact()
    {
        $about = app(About::class)
            ->where(IS_ACTIVE_COL, 1)
            ->orderBy(ID_COL, 'DESC')
            ->first();
        if (empty($about)) {
            $about = (object)($this->contact);
        }
        return view('edu::contact', compact('about'));
    }

    public function contacted(ContactCreateRequest $request)
    {
        $input = $request->all();
        if (auth()->check()) {
            $input[CREATED_BY_COL] = auth()->id();
        }
        app(Contact::class)->create($input);
        session()->flash('success', 'Bạn đã liên hệ thành công');
        return back();
    }
}
