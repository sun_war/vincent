<?php
/**
 * Created by PhpStorm.
 * User: JK
 * Date: 2/12/2018
 * Time: 11:09 PM
 */
namespace Vincent\Providers;
use Illuminate\Support\ServiceProvider;

class VincentServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom( base_path('modules/Vincent') . '/router.php');
        $this->loadViewsFrom(base_path('modules/Vincent')  . '/resources/views', 'edu');

    }
    public function register()
    {

    }
}