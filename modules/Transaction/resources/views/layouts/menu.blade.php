<li class="has-sub root-level" id="transactionMenu">
    <a>
        <i class="fa fa-money"></i>
        <span class="title">{{__('menu.transaction')}}</span>
    </a>
    <ul>
        <li class="{{ request()->is('transaction*') ? 'active' : ''}}" id="transactionMenu">
            <a href="{{route('transaction.index')}}">
                <span class="title">{{__('table.transactions')}}</span>
            </a>
        </li>
        <li class="{{ request()->is('contract*') ? 'active' : ''}}" id="contractMenu">
            <a href="{{route('contract.index')}}">
                <span class="title">{{__('table.contracts')}}</span>
            </a>
        </li>
    </ul>
</li>