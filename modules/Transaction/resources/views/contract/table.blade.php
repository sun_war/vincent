<table class="table">
    <thead>
        <tr>
            <th>{{trans('label.lender')}}</th>
<th>{{trans('label.borrower')}}</th>
<th>{{trans('label.money')}}</th>
<th>{{trans('label.interest')}}</th>
<th>{{trans('label.period')}}</th>
<th>{{trans('label.is_transaction')}}</th>
<th>{{trans('label.auto_pay')}}</th>
<th>{{trans('label.is_auto_pay')}}</th>
<th>{{trans('label.lender_confirm')}}</th>
<th>{{trans('label.borrower_confirm')}}</th>
<th>{{trans('label.active_time')}}</th>

            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($contracts as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td>{{$row->lender}}</td>
<td>{{$row->borrower}}</td>
<td>{{$row->money}}</td>
<td>{{$row->interest}}</td>
<td>{{$row->period}}</td>
<td>{{$row->is_transaction}}</td>
<td>{{$row->auto_pay}}</td>
<td>{{$row->is_auto_pay}}</td>
<td>{{$row->lender_confirm}}</td>
<td>{{$row->borrower_confirm}}</td>
<td>{{$row->active_time}}</td>

            <td class="text-right">
                <form method="POST" action="{{route('contract.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('contract.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('contract.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$contracts->links()}}
</div>
