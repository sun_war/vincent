@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('contract.index')}}">{{trans('table.contracts')}}</a>
    </li>
    <li class="active">
        <strong>Table</strong>
    </li>
</ol>
<div>
    {!! '$'.lender !!}
{!! '$'.borrower !!}
{!! '$'.money !!}
{!! '$'.interest !!}
{!! '$'.period !!}
{!! '$'.is_transaction !!}
{!! '$'.auto_pay !!}
{!! '$'.is_auto_pay !!}
{!! '$'.lender_confirm !!}
{!! '$'.borrower_confirm !!}
{!! '$'.active_time !!}

</div>
<a href="{{url()->previous()}}" class="btn btn-default"><i class="fa fa-backward"></i></a>
@endsection