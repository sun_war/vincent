@extends('layouts.app')
@section('content')
    <div class="row">
        <ol class="breadcrumb bc-3">
            <li>
                <a href="/"><i class="fa fa-home"></i></a>
            </li>
            <li>
                <a href="{{route('contract.index')}}">{{trans('table.contracts')}}</a>
            </li>
            <li class="active">
                <strong>{{__('action.update')}}</strong>
            </li>
        </ol>
        <form action="{{route('contract.update', $contract->id)}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group col-lg-4">
                <label for="lender">{{trans('label.lender')}}</label>
                <input type="number" value="{{$contract->lender}}" class="form-control" name="lender" id="lender">
            </div>
            <div class="form-group col-lg-4">
                <label for="money">{{trans('label.money')}}</label>
                <input type="number" value="{{$contract->money}}" class="form-control" name="money" id="money">
            </div>

            <div class="form-group col-lg-4">
                <label for="period">{{trans('label.period')}}</label>
                <input class="form-control"  value="{{$contract->period}}"  name="period" id="period">
            </div>
            <div class="form-group col-lg-4">
                <label for="borrower">{{trans('label.borrower')}}</label>
                <input type="number"  value="{{$contract->borrower}}" class="form-control" name="borrower" id="borrower">
            </div>

            <div class="form-group col-lg-4">
                <label for="auto_pay">{{trans('label.auto_pay')}}</label>
                <input type="number"  value="{{$contract->auto_pay}}" class="form-control" name="auto_pay" id="auto_pay">
            </div>
            <div class="form-group col-lg-4">
                <label for="active_time">{{trans('label.active_time')}}</label>
                <input class="form-control"  value="{{$contract->active_time}}" name="active_time" id="active_time">
            </div>
            <div class="form-group col-lg-3">
                <label for="is_auto_pay">{{trans('label.is_auto_pay')}}</label>
                <div class="checkbox">
                    <label>
                        <div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
                            <input type="checkbox"  {{$contract->is_auto_pay == 1 ? 'checked' : ''}} value="1" name="is_auto_pay" id="is_auto_pay">
                        </div>
                    </label>
                </div>
            </div>
            <div class="form-group col-lg-3">
                <label for="is_transaction">{{trans('label.is_transaction')}}</label>
                <div class="checkbox">
                    <label>
                        <div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
                            <input type="checkbox" {{$contract->is_transaction == 1 ? 'checked' : ''}}  value="1" name="is_transaction" id="is_transaction">
                        </div>
                    </label>
                </div>
            </div>
            <div class="form-group col-lg-3">
                <label for="interest">{{trans('label.interest')}}</label>
                <div class="checkbox">
                    <label>
                        <div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
                            <input type="checkbox" {{$contract->interest == 1 ? 'checked' : ''}}  value="1" name="interest" id="interest">
                        </div>
                    </label>
                </div>
            </div>
            <div class="col-lg-12">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection