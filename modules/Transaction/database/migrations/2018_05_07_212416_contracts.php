<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lender')->unsigned();
            $table->integer('borrower')->unsigned();
            $table->bigInteger('money')->unsigned()->default(0);
            $table->tinyInteger('interest')->unsigned();
            $table->datetime('period');
            $table->tinyInteger('is_transaction');
            $table->bigInteger('auto_pay');
            $table->tinyInteger('is_auto_pay');
            $table->tinyInteger('lender_confirm')->default(0);
            $table->tinyInteger('borrower_confirm')->default(0);
            $table->datetime('active_time');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
