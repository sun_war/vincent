<?php

namespace Transaction;

use Illuminate\Support\ServiceProvider;

use Transaction\Repositories\ContractRepository;
use Transaction\Repositories\ContractRepositoryEloquent;
use Transaction\Repositories\TransactionRepositoryEloquent;
use Transaction\Repositories\TransactionRepository;

class TransactionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
//        $this->loadMigrationsFrom(__DIR__ . '/database');
        $this->loadRoutesFrom(__DIR__ . '/router.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'transaction');
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ContractRepository::class, ContractRepositoryEloquent::class);
        $this->app->bind(TransactionRepository::class, TransactionRepositoryEloquent::class);
    }
}
