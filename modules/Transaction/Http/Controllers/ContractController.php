<?php

namespace Transaction\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use Transaction\Models\Contract;
use Transaction\Http\Requests\ContractCreateRequest;
use Transaction\Http\Requests\ContractUpdateRequest;
use Transaction\Repositories\ContractRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class ContractController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(ContractRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['contracts'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('transaction::contract.table', $data)->render();
        }
        return view('transaction::contract.index', $data);
    }

    public function create()
    {
        return view('transaction::contract.create');
    }

    public function store(ContractCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('contract.index');
    }

    public function show($id)
    {
        $contract = $this->repository->find($id);
        if(empty($contract))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('transaction::contract.show', compact('contract'));
    }

    public function edit($id)
    {
        $contract = $this->repository->find($id);
        if(empty($contract))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('transaction::contract.update', compact('contract'));
    }

    public function update(ContractUpdateRequest $request, $id)
    {
        $input = $request->all();
        $contract = $this->repository->find($id);
        if(empty($contract))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $contract);
        session()->flash('success', 'update success');
        return redirect()->route('contract.index');
    }

    public function destroy($id)
    {
        $contract = $this->repository->find($id);
        if(empty($contract))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
