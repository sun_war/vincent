<?php

namespace Transaction\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use Transaction\Models\Transaction;
use Transaction\Http\Requests\TransactionCreateRequest;
use Transaction\Http\Requests\TransactionUpdateRequest;
use Transaction\Repositories\TransactionRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class TransactionController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(TransactionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['transactions'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('transaction::transaction.table', $data)->render();
        }
        return view('transaction::transaction.index', $data);
    }

    public function create()
    {
        return view('transaction::transaction.create');
    }

    public function store(TransactionCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('transaction.index');
    }

    public function show($id)
    {
        $transaction = $this->repository->find($id);
        if(empty($transaction))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('transaction::transaction.show', compact('transaction'));
    }

    public function edit($id)
    {
        $transaction = $this->repository->find($id);
        if(empty($transaction))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('transaction::transaction.update', compact('transaction'));
    }

    public function update(TransactionUpdateRequest $request, $id)
    {
        $input = $request->all();
        $transaction = $this->repository->find($id);
        if(empty($transaction))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $transaction);
        session()->flash('success', 'update success');
        return redirect()->route('transaction.index');
    }

    public function destroy($id)
    {
        $transaction = $this->repository->find($id);
        if(empty($transaction))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
