<?php

namespace Transaction\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Contract extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'contracts';
    public $fillable = ['lender', 'borrower', 'money', 'interest', 'period', 'is_transaction', 'auto_pay', 'is_auto_pay', 'lender_confirm', 'borrower_confirm', 'active_time'];

    public function scopeFilter($query, $input)
    {
        if (isset($input['lender'])) {
            $query->where('lender', $input['lender']);
        }
        if (isset($input['borrower'])) {
            $query->where('borrower', $input['borrower']);
        }
        if (isset($input['money'])) {
            $query->where('money', $input['money']);
        }
        if (isset($input['interest'])) {
            $query->where('interest', $input['interest']);
        }
        if (isset($input['period'])) {
            $query->where('period', $input['period']);
        }
        if (isset($input['is_transaction'])) {
            $query->where('is_transaction', $input['is_transaction']);
        }
        if (isset($input['auto_pay'])) {
            $query->where('auto_pay', $input['auto_pay']);
        }
        if (isset($input['is_auto_pay'])) {
            $query->where('is_auto_pay', $input['is_auto_pay']);
        }
        if (isset($input['lender_confirm'])) {
            $query->where('lender_confirm', $input['lender_confirm']);
        }
        if (isset($input['borrower_confirm'])) {
            $query->where('borrower_confirm', $input['borrower_confirm']);
        }
        if (isset($input['active_time'])) {
            $query->where('active_time', $input['active_time']);
        }

        return $query;
    }


    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/contracts'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

