<?php
/**
 * Created by PhpStorm.
 * User: JK
 * Date: 3/25/2018
 * Time: 11:33 PM
 */
Route::group(
    [
        'middleware' => ['web', 'auth:admin'],
        'namespace' => 'Transaction\Http\Controllers',
        'prefix' => 'transaction'
    ], function () {
        Route::resource('transaction' , 'TransactionController');
        Route::resource('contract' , 'ContractController');
});




