<?php

namespace Landing\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Slide extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'slides';
    public $fillable = [IMAGE_COL, TITLE_COL, CONTENT_COL, IS_ACTIVE_COL, NO_COL, CREATED_BY_COL, UPDATED_BY_COL, LOCALE_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input[IMAGE_COL])) {
            $query->where(IMAGE_COL, $input[IMAGE_COL]);
        }
        if (isset($input[TITLE_COL])) {
            $query->where(TITLE_COL, $input[TITLE_COL]);
        }
        if (isset($input[CONTENT_COL])) {
            $query->where(CONTENT_COL, $input[CONTENT_COL]);
        }
        if (isset($input[IS_ACTIVE_COL])) {
            $query->where(IS_ACTIVE_COL, $input[IS_ACTIVE_COL]);
        }
        return $query;
    }



    public $fileUpload = [IMAGE_COL => 1];
    protected $pathUpload = [IMAGE_COL => '/images/slides'];
    protected $thumbImage = [
        IMAGE_COL => [
            '/thumbs/' => [
                [1200, 640], [900, 500], [600, 320]
            ]
        ]
    ];
    protected $checkbox = [IS_ACTIVE_COL];
}

