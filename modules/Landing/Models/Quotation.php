<?php

namespace Landing\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Quotation extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'quotations';
    public $fillable = [IMAGE_COL, CONTENT_COL, AUTHOR_COL, JOB_COL, IS_ACTIVE_COL, NO_COL, CREATED_BY_COL, UPDATED_BY_COL, LOCALE_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input[IMAGE_COL])) {
            $query->where(IMAGE_COL, $input[IMAGE_COL]);
        }
        if (isset($input[CONTENT_COL])) {
            $query->where(CONTENT_COL, $input[CONTENT_COL]);
        }
        if (isset($input[AUTHOR_COL])) {
            $query->where(AUTHOR_COL, $input[AUTHOR_COL]);
        }
        if (isset($input[JOB_COL])) {
            $query->where(JOB_COL, $input[JOB_COL]);
        }
        if (isset($input[IS_ACTIVE_COL])) {
            $query->where(IS_ACTIVE_COL, $input[IS_ACTIVE_COL]);
        }
        return $query;
    }

    public $fileUpload = [IMAGE_COL => 1];
    protected $pathUpload = [IMAGE_COL => '/images/quotations'];
    protected $thumbImage = [
        IMAGE_COL => [
            '/thumbs/' => [
                [1200, 400], [600, 200], [300, 100]
            ]
        ]
    ];
    protected $checkbox = [IS_ACTIVE_COL];
}

