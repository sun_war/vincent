<?php

namespace Landing\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Contact extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'contacts';
    public $fillable = [NAME_COL, EMAIL_COL, PHONE_NUMBER_COL, CREATED_BY_COL, TITLE_COL, MESSAGE_COL, CREATED_BY_COL, UPDATED_BY_COL, LOCALE_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input[NAME_COL])) {
            $query->where(NAME_COL, $input[NAME_COL]);
        }
        if (isset($input[EMAIL_COL])) {
            $query->where(EMAIL_COL, $input[EMAIL_COL]);
        }
        if (isset($input[PHONE_NUMBER_COL])) {
            $query->where(PHONE_NUMBER_COL, $input[PHONE_NUMBER_COL]);
        }
        if (isset($input[CREATED_BY_COL])) {
            $query->where(CREATED_BY_COL, $input[CREATED_BY_COL]);
        }
        if (isset($input[TITLE_COL])) {
            $query->where(TITLE_COL, $input[TITLE_COL]);
        }
        if (isset($input[MESSAGE_COL])) {
            $query->where(MESSAGE_COL, $input[MESSAGE_COL]);
        }

        return $query;
    }


    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/contacts'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

