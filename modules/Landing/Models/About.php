<?php

namespace Landing\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class About extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'abouts';
    public $fillable = [PHONE_NUMBER_COL, ADDRESS_COL, EMAIL_COL, MAP_COL, INFO_COL, IS_ACTIVE_COL, CREATED_BY_COL, UPDATED_BY_COL, LOCALE_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input[PHONE_NUMBER_COL])) {
            $query->where(PHONE_NUMBER_COL, $input[PHONE_NUMBER_COL]);
        }
        if (isset($input[ADDRESS_COL])) {
            $query->where(ADDRESS_COL, $input[ADDRESS_COL]);
        }
        if (isset($input[EMAIL_COL])) {
            $query->where(EMAIL_COL, $input[EMAIL_COL]);
        }
        if (isset($input[MAP_COL])) {
            $query->where(MAP_COL, $input[MAP_COL]);
        }
        if (isset($input[INFO_COL])) {
            $query->where(INFO_COL, $input[INFO_COL]);
        }
        if (isset($input[IS_ACTIVE_COL])) {
            $query->where(IS_ACTIVE_COL, $input[IS_ACTIVE_COL]);
        }

        return $query;
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/abouts'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = [IS_ACTIVE_COL];
}

