<?php
/**
 * Created by PhpStorm.
 * User: CPM
 * Date: 5/6/2018
 * Time: 11:30 AM
 */

Route::group(['middleware' => ['web', 'auth:admin', 'role:admin', 'locale.db'], 'prefix' => 'cpn'], function () {
    Route::group(['namespace' => 'Landing\Http\Controllers'], function () {

        Route::resource('about', 'AboutController');

        Route::resource('applications', 'ApplicationController');
        Route::resource('contact', 'ContactController');
        Route::resource('configs' , 'ConfigController');
        Route::resource('members', 'MemberController');
        Route::resource('quotation', 'QuotationController');
        Route::resource('introduces', 'IntroduceController');
        Route::resource('products', 'ProductController');
        Route::resource('services', 'ServiceController');
        Route::resource('slide', 'SlideController');
        Route::resource('short-news', 'ShortNewsController');
        Route::resource('site-settings' , 'SiteSettingController');
        Route::resource('titles', 'TitleController');

        Route::post('slide-sort', 'SlideController@sort')->name('slide.sort');
        Route::post('quotation-sort', 'QuotationController@sort')->name('quotation.sort');
    });
});

