@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('contact.index')}}">{{trans('table.contacts')}}</a>
        </li>
        <li class="active">
            <strong>Table</strong>
        </li>
    </ol>
    <div>
        {!! $contact ->name !!}
        {!! $contact ->email !!}
        {!! $contact ->phone_number !!}
        {!! $contact ->created_by !!}
        {!! $contact ->title !!}
        {!! $contact ->message !!}

    </div>
    <a href="{{url()->previous()}}" class="btn btn-default"><i class="fa fa-backward"></i></a>
@endsection