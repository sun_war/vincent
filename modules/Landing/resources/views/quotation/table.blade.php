<div id="sortable">
    @foreach($quotations as $row)
        <div class="row form-group">
            <div class="col-lg-3">
                <a href="{{route('quotation.edit', $row->id)}}" class="member-img">
                    <img src="{{$row->image ? asset('storage') . $row->image : 'http://placehold.it/200x150'}}" class="img-rounded img-responsive"/>
                </a>
            </div>
            <div class="col-sm-7">
                <h4><strong>No. <span class="no"></span></strong></h4> {!! $row->content !!}
            </div>
            <div class="col-sm-2 text-right">
                <form method="POST" action="{{route('quotation.destroy', $row->id)}}">
                    <input type="hidden" class="ids" name="ids[]" value="{{$row->id}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('quotation.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('quotation.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </div>
        </div>
    @endforeach
</div>

<div class="text-center">
    <button class="btn btn-primary btn-icon" id="sortBtn"><i class="fa fa-sort-amount-asc"></i> {{__('button.save')}} </button>
</div>
