@extends('layouts.app')
@section('content')
    <ol class="breadcrumb">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('about.index')}}">{{trans('table.abouts')}}</a>
        </li>
        <li class="active">
            <strong>Table</strong>
        </li>
    </ol>
    <div>
        <table class="table table-striped">
            <tr>
                <th>{{__('label.phone_number')}}</th>
                <td>{!! $about->phone_number !!}</td>
            </tr>
            <tr>
                <th>{{__('label.address')}}</th>
                <td>{!! $about->address !!}</td>
            </tr>
            <tr>
                <th>{{__('label.email')}}</th>
                <td>{!! $about->email !!}</td>
            </tr>
            <tr>
                <th>{{__('label.map')}}</th>
                <td> <iframe src="{!! $about->map !!}" width="100%" height="100%" frameborder="0" style="border: solid 1px #cfcfcf" allowfullscreen></iframe></td>
            </tr>
            <tr>
                <th>{{__('label.content')}}</th>
                <td>{!! $about->info !!}</td>
            </tr>
        </table>
    </div>
@endsection