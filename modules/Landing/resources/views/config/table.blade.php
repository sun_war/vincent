<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.image_meta')}}</th>
        <th>{{trans('label.logo')}}</th>
        <th>{{trans('label.title')}}</th>
        <th>{{trans('label.description_meta')}}</th>
        {{--<th>{{trans('label.face_script')}}</th>--}}
        {{--<th>{{trans('label.google_script')}}</th>--}}
        {{--<th>{{trans('label.is_active')}}</th>--}}
        {{--<th>{{trans('label.locale')}}</th>--}}
        {{--<th>{{trans('label.created_by')}}</th>--}}
        {{--<th>{{trans('label.updated_by')}}</th>--}}

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($configs as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td>
                <a href="{{route('services.edit', $row->id)}}" class="member-img">
                    <img width="150px" src="{{$row->image_meta ? asset('storage') . $row->image_meta : 'http://placehold.it/200x150'}}" class="img-rounded img-responsive"/>
                </a>
            </td>
            <td>
                <a href="{{route('services.edit', $row->id)}}" class="member-img">
                    <img width="150px" src="{{$row->logo ? asset('storage') . $row->logo : 'http://placehold.it/200x150'}}" class="img-rounded img-responsive"/>
                </a>
            </td>
            <td>{{$row->title}}</td>
            <td>{{$row->description_meta}}</td>
            {{--<td>{!! $row->face_script !!}</td>--}}
            {{--<td>{!! $row->google_script !!}</td>--}}
            {{--<td>{{$row->is_active}}</td>--}}
            {{--<td>{{$row->locale}}</td>--}}
            {{--<td>{{$row->created_by}}</td>--}}
            {{--<td>{{$row->updated_by}}</td>--}}

            <td class="text-right">
                <form method="POST" action="{{route('configs.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('configs.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('configs.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$configs->links()}}
</div>
