@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('configs.index')}}">{{trans('table.configs')}}</a>
        </li>
        <li class="active">
            <strong>Show</strong>
        </li>
    </ol>
    <div>
        <table class="table">
            <tbody>
            <tr>
                <th>{{__('label.image')}}</th>
                <td>
                    <img width="250px" src="{{$config->image_meta ? asset('storage') . $config->image_meta : 'http://placehold.it/200x150'}}" class="img-rounded img-responsive"/>
                </td>
            </tr>
            <tr>
                <th>{{__('label.logo')}}</th>
                <td>
                    <img width="250px" src="{{$config->logo ? asset('storage') . $config->logo : 'http://placehold.it/200x150'}}" class="img-rounded img-responsive"/>
                </td>
            </tr>
            <tr>
                <th>{{__('label.title')}}</th>
                <td>{!! $config->title !!}</td>
            </tr>
            <tr>
                <th>{{__('label.description_meta')}}</th>
                <td>{!! $config->description_meta !!}</td>
            </tr>
            <tr>
                <th>{{__('label.face_script')}}</th>
                <td>{!! $config->face_script !!}</td>
            </tr>
            <tr>
                <th>{{__('label.google_script')}}</th>
                <td>{!! $config->google_script !!}</td>
            </tr>
            <tr>
                <th>{{__('label.is_active')}}</th>
                <td>{!! $config->is_active ?
                '<button class="btn btn-xs btn-success"><i class="fa fa-check"></i></button>' :
                '<button class="btn btn-xs btn-default"><i class="fa fa-ban"></i></button>' !!}</td>
            </tr>
            {{--<tr>--}}
                {{--<th>{{__('label.is_active')}}</th>--}}
                {{--<td>{!! $config->is_active !!}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<th>{{__('label.locale')}}</th>--}}
                {{--<td>{!! $config->locale !!}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<th>{{__('label.created_by')}}</th>--}}
                {{--<td>{!! $config->created_by !!}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<th>{{__('label.updated_by')}}</th>--}}
                {{--<td>{!! $config->updated_by !!}</td>--}}
            {{--</tr>--}}
            </tbody>
        </table>
    </div>
@endsection

@push('js')
    <script>
        Menu('#Menu', '#configMenu')
    </script>
@endpush
