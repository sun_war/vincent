<li class="has-sub root-level" id="componentMenu">
    <a>
        <i class="fa fa-pagelines"></i>
        <span class="title">{{__('menu.landing_page')}}</span>
    </a>
    <ul class="sub-menu">
        <li id="introduceMenu">
            <a href="{{route('introduces.index')}}">
                <span class="title">{{__('table.introduces')}}</span>
            </a>
        </li>
        <li id="serviceMenu">
            <a href="{{route('services.index')}}">
                <span class="title">{{__('table.services')}}</span>
            </a>
        </li>
        <li id="applicationMenu">
            <a href="{{route('applications.index')}}">
                <span class="title">{{__('table.applications')}}</span>
            </a>
        </li>
        <li id="productMenu">
            <a href="{{route('products.index')}}">
                <span class="title">{{__('table.products')}}</span>
            </a>
        </li>
        <li id="shortNewsMenu">
            <a href="{{route('short-news.index')}}">
                <span class="title">{{__('table.short_news')}}</span>
            </a>
        </li>        <li id="memberMenu">
            <a href="{{route('members.index')}}">
                <span class="title">{{__('table.members')}}</span>
            </a>
        </li>
        <li id="slideMenu">
            <a href="{{route('slide.index')}}">
                <span class="title">{{__('table.slides')}}</span>
            </a>
        </li>
        <li id="quotationMenu">
            <a href="{{route('quotation.index')}}">
                <span class="title">{{__('table.quotations')}}</span>
            </a>
        </li>
        <li id="aboutMenu">
            <a href="{{route('about.index')}}">
                <span class="title">{{__('table.abouts')}}</span>
            </a>
        </li>
        <li id="contactMenu">
            <a href="{{route('contact.index')}}">
                <span class="title">{{__('table.contacts')}}</span>
            </a>
        </li>
        <li id="titleMenu">
            <a href="{{route('titles.index')}}">
                <span class="title">{{__('table.titles')}}</span>
            </a>
        </li>
        <li id="siteSettingsMenu">
            <a href="{{route('site-settings.create')}}">
                <span class="title">{{__('table.site_settings')}}</span>
            </a>
        </li>
    </ul>
</li>

<li class="root-level" id="configMenu">
    <a href="{{route('configs.index')}}">
        <i class="fa fa-cogs"></i>
        <span class="title">{{__('menu.configs')}}</span>
    </a>
</li>