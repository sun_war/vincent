<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.page_name')}}</th>
<th>{{trans('label.site_email')}}</th>
<th>{{trans('label.site_phone_number')}}</th>
<th>{{trans('label.language')}}</th>
<th>{{trans('label.copyright')}}</th>
<th>{{trans('label.social')}}</th>
<th>{{trans('label.created_by')}}</th>
<th>{{trans('label.updated_by')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($siteSettings as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->page_name}}</td>
<td>{{$row->site_email}}</td>
<td>{{$row->site_phone_number}}</td>
<td>{{$row->language}}</td>
<td>{{$row->copyright}}</td>
<td>{{$row->social}}</td>
<td>{{$row->created_by}}</td>
<td>{{$row->updated_by}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('site-settings.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('site-settings.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('site-settings.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$siteSettings->links()}}
</div>
