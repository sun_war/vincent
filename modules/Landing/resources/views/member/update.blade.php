@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('members.index')}}">{{trans('table.members')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.update')}}</strong>
        </li>
    </ol>
    <div class="row">
        <form action="{{route('members.update', $member->id)}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group col-lg-6">
                <label for="image" style="display: block">{{trans('label.image')}}</label>
                <div class="form-group">
                    <div class="fileinput fileinput-newform-group" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" data-trigger="fileinput">
                            <img class="img-responsive" src="{{$member->image ? asset('storage') . $member->image : 'http://placehold.it/200x150'}}" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail img-responsive" ></div>
                        <div>
                            <span class="btn btn-white btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="image" id="image" accept="image/*">
                            </span>
                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label for="name">{{trans('label.name')}}</label>
                <input required class="form-control" name="name" id="name" value="{{$member->name}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="name">{{trans('label.job')}}</label>
                <input required class="form-control" name="job" id="job" value="{{$member->job}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="twitter_link">{{trans('label.twitter_link')}}</label>
                <input required class="form-control" name="twitter_link" id="twitter_link"
                       value="{{$member->twitter_link}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="facebook_link">{{trans('label.facebook_link')}}</label>
                <input required class="form-control" name="facebook_link" id="facebook_link"
                       value="{{$member->facebook_link}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="google_link">{{trans('label.google_link')}}</label>
                <input required class="form-control" name="google_link" id="google_link"
                       value="{{$member->google_link}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="locale">{{trans('label.locale')}}</label>
                <select required class="form-control" name="locale" id="locale">
                    @foreach(LOCALES as $LOCALE => $name)
                        <option {{$member->locale === $LOCALE ? 'selected' : ''}} value="{{$LOCALE}}">{{$name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-12">
                <label for="is_active">{{trans('label.is_active')}}</label>
                <div class="checkbox">
                    <label>
                        <input required type="checkbox" {{$member->is_active !== 1 ?: 'checked'}} name="is_active"
                               id="is_active" value="1">
                    </label>
                </div>
            </div>
            {{--<div class="form-group col-lg-6">--}}
            {{--<label for="no">{{trans('label.no')}}</label>--}}
            {{--<div class="checkbox">--}}
            {{--<label>--}}
            {{--<input required type="checkbox" {{$member->no !== 1 ?: 'checked'}} name="no" id="no" value="1">--}}
            {{--</label>--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="col-lg-12 form-group">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script>
        Menu('#componentMenu', '#memberMenu')
    </script>
@endpush
