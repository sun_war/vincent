<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Http\Requests\MemberCreateRequest;
use Landing\Http\Requests\MemberUpdateRequest;
use Landing\Http\Repositories\MemberRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class MemberController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(MemberRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $input[LOCALE_COL] = \App::getLocale();
        $data['members'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('cpn::member.table', $data)->render();
        }
        return view('cpn::member.index', $data);
    }

    public function create()
    {
        return view('cpn::member.create');
    }

    public function store(MemberCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('members.index');
    }

    public function show($id)
    {
        $member = $this->repository->find($id);
        if (empty($member)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::member.show', compact('member'));
    }

    public function edit($id)
    {
        $member = $this->repository->find($id);
        if (empty($member)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::member.update', compact('member'));
    }

    public function update(MemberUpdateRequest $request, $id)
    {
        $input = $request->all();
        $member = $this->repository->find($id);
        if (empty($member)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $member);
        session()->flash('success', 'update success');
        return redirect()->route('members.index');
    }

    public function destroy($id)
    {
        $member = $this->repository->find($id);
        if (empty($member)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
