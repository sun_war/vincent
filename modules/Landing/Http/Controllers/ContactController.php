<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use Landing\Models\Contact;
use Landing\Http\Requests\ContactCreateRequest;
use Landing\Http\Requests\ContactUpdateRequest;
use Landing\Http\Repositories\ContactRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class ContactController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(ContactRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['contacts'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('cpn::contact.table', $data)->render();
        }
        return view('cpn::contact.index', $data);
    }

    public function create()
    {
        return view('cpn::contact.create');
    }

    public function store(ContactCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('contact.index');
    }

    public function show($id)
    {
        $contact = $this->repository->find($id);
        if(empty($contact))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::contact.show', compact('contact'));
    }

    public function edit($id)
    {
        $contact = $this->repository->find($id);
        if(empty($contact))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::contact.update', compact('contact'));
    }

    public function update(ContactUpdateRequest $request, $id)
    {
        $input = $request->all();
        $contact = $this->repository->find($id);
        if(empty($contact))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $contact);
        session()->flash('success', 'update success');
        return redirect()->route('contact.index');
    }

    public function destroy($id)
    {
        $contact = $this->repository->find($id);
        if(empty($contact))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
