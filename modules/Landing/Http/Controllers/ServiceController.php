<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Http\Requests\ServiceCreateRequest;
use Landing\Http\Requests\ServiceUpdateRequest;
use Landing\Http\Repositories\ServiceRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class ServiceController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $input[LOCALE_COL] = \App::getLocale();
        $data['services'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('cpn::service.table', $data)->render();
        }
        return view('cpn::service.index', $data);
    }

    public function create()
    {
        return view('cpn::service.create');
    }

    public function store(ServiceCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('services.index');
    }

    public function show($id)
    {
        $service = $this->repository->find($id);
        if (empty($service)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::service.show', compact('service'));
    }

    public function edit($id)
    {
        $service = $this->repository->find($id);
        if (empty($service)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::service.update', compact('service'));
    }

    public function update(ServiceUpdateRequest $request, $id)
    {
        $input = $request->all();
        $service = $this->repository->find($id);
        if (empty($service)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $service);
        session()->flash('success', 'update success');
        return redirect()->route('services.index');
    }

    public function destroy($id)
    {
        $service = $this->repository->find($id);
        if (empty($service)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
