<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Http\Requests\TitleCreateRequest;
use Landing\Http\Requests\TitleUpdateRequest;
use Landing\Http\Repositories\TitleRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class TitleController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(TitleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $input[LOCALE_COL] = \App::getLocale();
        $data['titles'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('cpn::title.table', $data)->render();
        }
        return view('cpn::title.index', $data);
    }

    public function create()
    {
        return view('cpn::title.create');
    }

    public function store(TitleCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('titles.index');
    }

    public function show($id)
    {
        $title = $this->repository->find($id);
        if (empty($title)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::title.show', compact('title'));
    }

    public function edit($id)
    {
        $title = $this->repository->find($id);
        if (empty($title)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::title.update', compact('title'));
    }

    public function update(TitleUpdateRequest $request, $id)
    {
        $input = $request->all();
        $title = $this->repository->find($id);
        if (empty($title)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $title);
        session()->flash('success', 'update success');
        return redirect()->route('titles.index');
    }

    public function destroy($id)
    {
        $title = $this->repository->find($id);
        if (empty($title)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
