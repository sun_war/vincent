<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use Landing\Models\About;
use Landing\Http\Requests\AboutCreateRequest;
use Landing\Http\Requests\AboutUpdateRequest;
use Landing\Http\Repositories\AboutRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class AboutController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(AboutRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $input[LOCALE_COL] = \App::getLocale();
        $data['abouts'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('cpn::about.table', $data)->render();
        }
        return view('cpn::about.index', $data);
    }

    public function create()
    {
        return view('cpn::about.create');
    }

    public function store(AboutCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('about.index');
    }

    public function show($id)
    {
        $about = $this->repository->find($id);
        if(empty($about))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::about.show', compact('about'));
    }

    public function edit($id)
    {
        $about = $this->repository->find($id);
        if(empty($about))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::about.update', compact('about'));
    }

    public function update(AboutUpdateRequest $request, $id)
    {
        $input = $request->all();
        $about = $this->repository->find($id);
        if(empty($about))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $about);
        session()->flash('success', 'update success');
        return redirect()->route('about.index');
    }

    public function destroy($id)
    {
        $about = $this->repository->find($id);
        if(empty($about))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
