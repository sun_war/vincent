/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
import Echo from "laravel-echo"
window.io = require('socket.io-client');

window.echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001',
});


require('./chat/group');
require('./chat/message');
