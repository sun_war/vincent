<div class="modal fade" id="managerModal" tabindex="-1" role="dialog" aria-labelledby="managerModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="managerModalLabel">Manager media</h4>
            </div>
            <div class="modal-body">
                <div id="contentManager">
                    @include('media::manager.content')
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" class="copied">
</div>