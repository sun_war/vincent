<div class="row">
    <a class="btn btn-default folderShow" href="{{$parentFolder}}">...</a>
    <form class="col-lg-3" action="{{$newFolderRoute}}" method="GET" id="newFolderForm">
        <div class="input-group">
            <input name="folder" class="form-control" required placeholder="enter folder name">
            <span class="input-group-btn">
                 <button class="btn btn-default">
                     <i class="fa fa-plus"></i>
                 </button>
            </span>
        </div>
    </form>
    <form class="col-lg-3" id="uploadForm" action="{{$uploadRoute}}" method="POST"
          enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="input-group">
            <input type="file" required name="image" class="form-control">
            <span class="input-group-btn">
                 <button class="btn btn-default">
                     <i class="fa fa-upload"></i>
                 </button>
            </span>
        </div>
    </form>
</div>
@if(count($images))
    <div class="row">
        <div class="col-lg-12">
            <h3>Image</h3>
        </div>
        @foreach($images as $name => $image)
            <div class="col-lg-3 form-group text-center">
                <img src="{{$image}}" class="img-responsive copyName form-group" data-url="{{$image}}" alt="">
                <a href="{{$image}}" target="_blank" class="btn btn-default btn-xs">
                    <i class="fa fa-link"></i>
                </a>
                <button class="btn btn-default btn-xs">
                    <i class="fa fa-trash-o"></i>
                </button>
            </div>
        @endforeach
    </div>
@endif
@if(count($folders))
    <div class="row">
        <div class="col-lg-12">
            <h3>Folder</h3>
        </div>
        @foreach($folders as $name => $folder)
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 form-group text-center">
                <a href="{{$folder}}" class="folderShow" alt="{{$name}}">
                    <img src="{{asset('folder.png')}}" alt="" class="img-responsive">
                    {{$name}} <i class="fa fa-trash-o"></i>
                </a>
            </div>
        @endforeach
    </div>
@endif
