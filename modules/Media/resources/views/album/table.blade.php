<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.title')}}</th>
<th>{{trans('label.image')}}</th>
<th>{{trans('label.is_active')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($albums as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->title}}</td>
<td>{{$row->image}}</td>
<td>{{$row->is_active}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('albums.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('albums.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('albums.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$albums->links()}}
</div>
