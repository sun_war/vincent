<li class="has-sub root-level" id="mediaMenu">
    <a>
        <i class="fa fa-magic"></i>
        <span class="title">Media</span>
    </a>
    <ul>
        <li class="{{ request()->is('image*') ? 'active' : ''}}" id="imageMenu">
            <a href="{{route('image.index')}}">
                <span class="title">{{__('table.images')}}</span>
            </a>
        </li>
        <li class="{{ request()->is('album*') ? 'active' : ''}}" id="albumMenu">
            <a href="{{route('album.index')}}">
                <span class="title">{{__('table.albums')}}</span>
            </a>
        </li>
    </ul>
</li>