<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.title')}}</th>
<th>{{trans('label.album_id')}}</th>
<th>{{trans('label.image')}}</th>
<th>{{trans('label.link')}}</th>
<th>{{trans('label.is_active')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($images as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->title}}</td>
<td>{{$row->album_id}}</td>
<td>{{$row->image}}</td>
<td>{{$row->link}}</td>
<td>{{$row->is_active}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('images.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('images.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('images.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$images->links()}}
</div>
