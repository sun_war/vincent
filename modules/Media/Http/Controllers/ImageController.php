<?php

namespace Media\Http\Controllers;

use App\Http\Controllers\Controller;
use Media\Http\Requests\ImageCreateRequest;
use Media\Http\Requests\ImageUpdateRequest;
use Media\Repositories\ImageRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class ImageController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(ImageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['images'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('media::image.table', $data)->render();
        }
        return view('media::image.index', $data);
    }

    public function create()
    {
        return view('media::image.create');
    }

    public function store(ImageCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('images.index');
    }

    public function show($id)
    {
        $image = $this->repository->find($id);
        if (empty($image)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('media::image.show', compact('image'));
    }

    public function edit($id)
    {
        $image = $this->repository->find($id);
        if (empty($image)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('media::image.update', compact('image'));
    }

    public function update(ImageUpdateRequest $request, $id)
    {
        $input = $request->all();
        $image = $this->repository->find($id);
        if (empty($image)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $image);
        session()->flash('success', 'update success');
        return redirect()->route('images.index');
    }

    public function destroy($id)
    {
        $image = $this->repository->find($id);
        if (empty($image)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
