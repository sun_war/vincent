<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 1/10/19
 * Time: 4:18 PM
 */

namespace Media\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Media\Http\Repositories\MediaService;
use Modularization\Facades\UploadFun;

class ManagerController extends Controller
{
    private $service;

    public function __construct(MediaService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $path = request('path', 'storage');
        $dir = public_path($path);
        $data = $this->service->done($dir);
        if (request()->ajax()) {
            return view('media::manager.content', $data)->render();
        }
        return view('media::manager.index', $data);
    }

    public function newFolder()
    {
        try {
            $dir = request('dir');
            $folder = request('folder', 'new-folder');
            mkdir($dir . '/' . $folder);

            $data = $this->service->done($dir);
            if (request()->ajax()) {
                return view('media::manager.content', $data)->render();
            }
            return back();
        } catch (\Exception $exception) {
            session()->flash('error', $exception->getMessage());
        }

        return back();
    }

    public function upload(Request $request)
    {
        $input = $request->all();
        $dir = $input['dir'];
        app(UploadFun::class)
            ->images($input['image'], $dir, []);
        $data = $this->service->done($dir);
        if (request()->ajax()) {
            return view('media::manager.content', $data)->render();
        }
        return back();
    }
}