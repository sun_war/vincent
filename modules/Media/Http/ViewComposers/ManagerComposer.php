<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 1/11/19
 * Time: 5:57 PM
 */

namespace Media\Http\ViewComposers;


use Illuminate\View\View;
use Media\Http\Repositories\MediaService;

class ManagerComposer
{
    private $service;

    public function __construct(MediaService $service)
    {
        $this->service = $service;
    }

    public function compose(View $view) {
        $data = $this->service->done(public_path('storage'));
        $view->with($data);
    }
}