<?php

namespace Media\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Media\Http\ViewComposers\ManagerComposer;
use Media\Repositories\AlbumRepository;
use Media\Repositories\AlbumRepositoryEloquent;
use Media\Repositories\ImageRepository;
use Media\Repositories\ImageRepositoryEloquent;

class MediaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/router.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'media');
//        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');
        Blade::directive('imageMan', function ($expression) {
            return '<button type="button" class="btn btn-primary btn-icon btn-xs pull-right"
                data-toggle="modal" data-target="#managerModal">
                Manager <i class="fa fa-image"></i>
                </button>';
        });
        view()->composer([
            'media::manager.modals.show'
        ], ManagerComposer::class);

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ImageRepository::class, ImageRepositoryEloquent::class);
        $this->app->bind(AlbumRepository::class, AlbumRepositoryEloquent::class);
    }
}
