<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 5/8/18
 * Time: 12:56 PM
 */
Route::group(
    [
        'middleware' => ['web', 'auth:admin', 'role:admin'],
        'namespace' => 'Media\Http\Controllers',
        'prefix' => 'media'
    ], function () {
    Route::resource('images' , 'ImageController');
    Route::resource('albums' , 'AlbumController');

    Route::get('/', 'ManagerController@index')->name('media.manager');
    Route::get('/new-folder', 'ManagerController@newFolder')->name('media.manager.new-folder');
    Route::post('/upload', 'ManagerController@upload')->name('media.manager.upload');
});


