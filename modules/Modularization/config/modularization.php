<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 4/29/17
 * Time: 6:20 PM
 */

return [
    'constant' => app_path('Constants'),
    'extends' => 'layouts.app',
    'content' => 'content',
    'extra_css' => 'css',
    'extra_js' => 'js',
];