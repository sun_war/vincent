<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 5/26/17
 * Time: 3:33 PM
 */

namespace Modularization\Core\Components\Http\Repositories;


use Modularization\Core\Components\BaseComponent;

class InterfaceComponent extends BaseComponent
{
    public function __construct()
    {
        $this->source = file_get_contents($this->getSource());
    }

    public function building($table, $nameSpace)
    {
        $this->buildNameSpace($nameSpace);
        $this->buildClassName($table);
        return $this->source;
    }

    private function getSource()
    {
        return $this->getViewPath( '/mvc/interfaceRepo.txt');
    }
}