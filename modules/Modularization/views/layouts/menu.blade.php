<li class="has-sub root-level" id="modularizationMenu">
    <a>
        <i class="fa fa-file"></i>
        <span class="title">{{__('menu.build_module')}}</span>
    </a>
    <ul>
        <li  id="crudMenu">
            <a href="{{route('dbmagic.create')}}">
                <span class="title">{{__('menu.crud')}}</span>
            </a>
        </li>
    </ul>
</li>