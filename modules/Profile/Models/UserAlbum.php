<?php

namespace Profile\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserAlbum extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;
//    use SoftDeletes;

    public $table = 'user_albums';
    public $fillable = ['title', 'description', 'image', 'views', 'like'];

    public function scopeFilter($query, $input)
    {
        foreach ($this->fillable as $value) {
            if (isset($input[$value])) {
                $query->where($value, $input[$value]);
            }
        }
        return $query;
    }

    public function images() {
        return $this->hasMany(UserImage::class, USER_ALBUM_ID_COL);
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/user_albums'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

