<?php

namespace Profile\Models;

use App\User;
use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserImage extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;
    use SoftDeletes;

    public $table = 'user_images';
    public $fillable = ['created_by', 'title', 'description', 'image', USER_ALBUM_ID_COL];

    public function scopeFilter($query, $input)
    {
        foreach ($this->fillable as $value) {
            if (isset($input[$value])) {
                $query->where($value, $input[$value]);
            }
        }
        return $query;
    }

    public function like() {
        return $this->hasMany(UserImageLike::class, USER_IMAGE_ID_COL);
    }

    public function users() {
        return $this->belongsToMany(User::class, USER_IMAGE_LIKES_TB,
            USER_IMAGE_ID_COL, CREATED_BY_COL);
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/user_images'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

