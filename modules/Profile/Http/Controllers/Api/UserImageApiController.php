<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserImageCreateRequest;
use Profile\Http\Requests\UserImageUpdateRequest;
use Profile\Http\Resources\UserImageResource;
use Profile\Http\Repositories\UserImageRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserImageApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserImageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserImageResource($data);
    }

    public function create()
    {
        return view('profile::user-image.create');
    }

    public function store(UserImageCreateRequest $request)
    {
        $input = $request->all();
        $userImage = $this->repository->store($input);
        return new UserImageResource($userImage);
    }

    public function show($id)
    {
        $userImage = $this->repository->find($id);
        return new UserImageResource($userImage);
    }

    public function edit($id)
    {
        $userImage = $this->repository->find($id);
        if (empty($userImage)) {
            return new UserImageResource([$userImage]);
        }
        return new UserImageResource($userImage);
    }

    public function update(UserImageUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userImage = $this->repository->find($id);
        if (empty($userImage)) {
            return new UserImageResource($userImage);
        }
        $data = $this->repository->change($input, $userImage);
        return new UserImageResource($data);
    }

    public function destroy($id)
    {
        $userImage = $this->repository->find($id);
        if (empty($userImage)) {
            return new UserImageResource($userImage);
        }
        $data = $this->repository->delete($id);
        return new UserImageResource([$data]);
    }
}