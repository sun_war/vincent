<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserFollowCreateRequest;
use Profile\Http\Requests\UserFollowUpdateRequest;
use Profile\Http\Resources\UserFollowResource;
use Profile\Http\Repositories\UserFollowRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserFollowApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserFollowRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserFollowResource($data);
    }

    public function create()
    {
        return view('profile::user-follow.create');
    }

    public function store(UserFollowCreateRequest $request)
    {
        $input = $request->all();
        $userFollow = $this->repository->store($input);
        return new UserFollowResource($userFollow);
    }

    public function show($id)
    {
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            return new UserFollowResource([$userFollow]);
        }
        return new UserFollowResource($userFollow);
    }

    public function edit($id)
    {
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            return new UserFollowResource([$userFollow]);
        }
        return new UserFollowResource($userFollow);
    }

    public function update(UserFollowUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            return new UserFollowResource([$userFollow]);
        }
        $data = $this->repository->change($input, $userFollow);
        return new UserFollowResource($data);
    }

    public function destroy($id)
    {
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            return new UserFollowResource($userFollow);
        }
        $data = $this->repository->delete($id);
        return new UserFollowResource([$data]);
    }
}