<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserAlbumCreateRequest;
use Profile\Http\Requests\UserAlbumUpdateRequest;
use Profile\Http\Resources\UserAlbumResource;
use Profile\Http\Repositories\UserAlbumRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserAlbumApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserAlbumRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserAlbumResource($data);
    }

    public function create()
    {
        return view('profile::user-album.create');
    }

    public function store(UserAlbumCreateRequest $request)
    {
        $input = $request->all();
        $userAlbum = $this->repository->store($input);
        return new UserAlbumResource($userAlbum);
    }

    public function show($id)
    {
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            return new UserAlbumResource([$userAlbum]);
        }
        $userAlbum->imageList = $userAlbum->images;
        return new UserAlbumResource($userAlbum);
    }

    public function edit($id)
    {
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            return new UserAlbumResource([$userAlbum]);
        }
        return new UserAlbumResource($userAlbum);
    }

    public function update(UserAlbumUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            return new UserAlbumResource([$userAlbum]);
        }
        $data = $this->repository->change($input, $userAlbum);
        return new UserAlbumResource($data);
    }

    public function destroy($id)
    {
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            return new UserAlbumResource($userAlbum);
        }
        $data = $this->repository->delete($id);
        return new UserAlbumResource([$data]);
    }
}