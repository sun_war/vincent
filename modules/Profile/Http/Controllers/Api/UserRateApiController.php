<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserRateCreateRequest;
use Profile\Http\Requests\UserRateUpdateRequest;
use Profile\Http\Resources\UserRateResource;
use Profile\Http\Repositories\UserRateRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserRateApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserRateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserRateResource($data);
    }

    public function create()
    {
        return view('profile::user-rate.create');
    }

    public function store(UserRateCreateRequest $request)
    {
        $input = $request->all();
        $userRate = $this->repository->store($input);
        return new UserRateResource($userRate);
    }

    public function show($id)
    {
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            return new UserRateResource([$userRate]);
        }
        return new UserRateResource($userRate);
    }

    public function edit($id)
    {
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            return new UserRateResource([$userRate]);
        }
        return new UserRateResource($userRate);
    }

    public function update(UserRateUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            return new UserRateResource([$userRate]);
        }
        $data = $this->repository->change($input, $userRate);
        return new UserRateResource($data);
    }

    public function destroy($id)
    {
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            return new UserRateResource($userRate);
        }
        $data = $this->repository->delete($id);
        return new UserRateResource([$data]);
    }
}