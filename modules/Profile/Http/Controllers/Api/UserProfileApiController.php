<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserProfileCreateRequest;
use Profile\Http\Requests\UserProfileUpdateRequest;
use Profile\Http\Resources\UserProfileResource;
use Profile\Http\Repositories\UserProfileRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserProfileApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserProfileRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserProfileResource($data);
    }

    public function create()
    {
        return view('profile::user-profile.create');
    }

    public function store(UserProfileCreateRequest $request)
    {
        $input = $request->all();
        $userProfile = $this->repository->store($input);
        return new UserProfileResource($userProfile);
    }

    public function show($id)
    {
        $userProfile = $this->repository->find($id);
        if (empty($userProfile)) {
            return new UserProfileResource([$userProfile]);
        }
        return new UserProfileResource($userProfile);
    }

    public function edit($id)
    {
        $userProfile = $this->repository->find($id);
        if (empty($userProfile)) {
            return new UserProfileResource([$userProfile]);
        }
        return new UserProfileResource($userProfile);
    }

    public function update(UserProfileUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userProfile = $this->repository->find($id);
        if (empty($userProfile)) {
            return new UserProfileResource([$userProfile]);
        }
        $data = $this->repository->change($input, $userProfile);
        return new UserProfileResource($data);
    }

    public function destroy($id)
    {
        $userProfile = $this->repository->find($id);
        if (empty($userProfile)) {
            return new UserProfileResource($userProfile);
        }
        $data = $this->repository->delete($id);
        return new UserProfileResource([$data]);
    }
}