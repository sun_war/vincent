<?php

namespace Profile\Http\Repositories;


use Modularization\MultiInheritance\RepositoriesTrait;

use Illuminate\Support\Facades\Cache;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Profile\Models\UserImage;

/**
 * Class NewsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserImageRepositoryEloquent extends BaseRepository implements UserImageRepository
{
    use RepositoriesTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserImage::class;
    }

    public function myPaginate($input)
    {
        isset($input[PER_PAGE]) ?: $input[PER_PAGE] = 10;
        return $this->makeModel()
            ->filter($input)
            ->orderBy('id', 'DESC')
            ->paginate($input[PER_PAGE]);
    }

    public function store($input)
    {
        $input[CREATED_BY_COL] = auth()->id();
        $input = $this->standardized($input, $this->makeModel());
        return $this->create($input);
    }

    public function edit($id)
    {
        $userImage = $this->find($id);
        if (empty($userImage)) {
            return $userImage;
        }
        return compact('userImage');
    }

    public function change($input, $data)
    {
        $input[UPDATED_BY_COL] = auth()->id();
        $input = $this->standardized($input, $data);
        return $this->update($input, $data->id);
    }

    public function import($file)
    {
        set_time_limit(9999);
        $path = $this->makeModel()->uploadImport($file);
        return $this->importing($path);
    }

    private function standardized($input, $data)
    {
        return $data->uploads($input);
    }

    public function destroy($data)
    {
        return $this->delete($data->id);
    }

    public function getSingleById($id, $operator, $order)
    {
        return $this->makeModel()
            ->where('id', $id)
            ->orWhere('id', $operator, $id)
            ->orderBy('id', $order)
            ->first();
    }

    /**
     * Boot up the repository, ping criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
