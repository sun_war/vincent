<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 7/23/18
 * Time: 10:23 AM
 */

Route::group(['namespace' => 'Profile\Http\Controllers',
    'middleware' => ['web', 'auth:admin'], 'prefix' => 'profile'],
    function () {
        Route::resource('user-albums' , 'UserAlbumController');
        Route::resource('user-follows' , 'UserFollowController');
        Route::resource('user-image-likes' , 'UserImageLikeController');
        Route::resource('user-images', 'UserImageController');
        Route::resource('user-rates' , 'UserRateController');
        Route::resource('user-profiles' , 'UserProfileController');
});

Route::name('api.')
    ->namespace('Profile\Http\Controllers\Api')
    ->prefix('api/studio')
    ->middleware(['api', 'auth:api'])
    ->group(function () {
        Route::resource('user-albums' , 'UserAlbumApiController');
        Route::resource('user-follows' , 'UserFollowApiController');
        Route::resource('user-image-likes' , 'UserImageLikeApiController');
        Route::resource('user-images', 'UserImageApiController');
        Route::resource('user-rates' , 'UserRateApiController');
        Route::resource('user-profiles' , 'UserProfileApiController');
    });

Route::name('frontend.')
    ->namespace('Profile\Http\Controllers\Frontend')
    ->prefix('frontend')
    ->middleware(['web', 'auth'])
    ->group(function () {
        Route::resource('user-rates' , 'UserRateController');
        Route::resource('user-images', 'UserImageController');
        Route::resource('user-albums', 'UserAlbumController');
        Route::resource('user-image-likes' , 'UserImageLikeApiController');
        Route::resource('user-images', 'UserImageController');
        Route::resource('user-profiles', 'UserProfileController');
    });

Route::namespace('Profile\Http\Controllers')
   // ->prefix('frontend')
    ->middleware(['web'])
    ->group(function () {
        Route::get('review' , 'ReviewController@index')->name('review');
        Route::get('review/single/{id}' , 'ReviewController@single')->name('review.single');
    });
