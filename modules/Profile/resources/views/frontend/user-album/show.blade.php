@extends('edu::layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 form-group">
            <div class="input-group">
                <input class="form-control" placeholder="Search for username...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
              </span>
            </div><!-- /input-group -->
        </div><!-- /.col-lg-6 -->
    </div>
    @include('profile::frontend.vendor.image.show')
    @include('profile::frontend.user-image.modals.create', [USER_ALBUM_ID_COL => $userAlbum->id])
    @include('profile::frontend.user-album.modals.create')
@endsection
@push('css')

@endpush
@push('js')
    <script>
        const likeImage = '#likeImage';
        const numLike = '#numLike';
        $(document).on('click', likeImage, function () {
            const self = $(this);
            const id = self.attr('data-id');
            const noLike = parseInt($(numLike).text());
            $(numLike).text(noLike + 1);
            const data = {
                user_image_id: id
            };
            const url = self.attr('data-route');
            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                success: function (result) {
                    console.log(result);
                }
            });
        });
    </script>
@endpush