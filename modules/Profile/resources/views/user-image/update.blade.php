@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('user-images.index')}}">{{trans('table.user_images')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.update')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('user-images.update', $userImage->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-12">
    <label for="created_by">{{trans('label.created_by')}}</label>
    <input required type="number" class="form-control" name="created_by" id="created_by" value="{{$userImage->created_by}}">
</div>
<div class="form-group col-lg-12">
    <label for="title">{{trans('label.title')}}</label>
    <input required type="number" class="form-control" name="title" id="title" value="{{$userImage->title}}">
</div>
<div class="form-group col-lg-12">
    <label for="description">{{trans('label.description')}}</label>
    <input required type="number" class="form-control" name="description" id="description" value="{{$userImage->description}}">
</div>
<div class="form-group col-lg-12">
    <label for="image">{{trans('label.image')}}</label>
    <input required type="file" name="image" id="image">
    <p class="help-block">Example block-level help text here.</p>
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        Menu('#ProfileMenu', '#userImageMenu')
    </script>
@endpush
