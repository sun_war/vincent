@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('user-images.index')}}">{{trans('table.user_images')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.created_by')}}</th>
<td>{!! $user_image->created_by !!}</td>
</tr><tr><th>{{__('label.title')}}</th>
<td>{!! $user_image->title !!}</td>
</tr><tr><th>{{__('label.description')}}</th>
<td>{!! $user_image->description !!}</td>
</tr><tr><th>{{__('label.image')}}</th>
<td>{!! $user_image->image !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#ProfileMenu', '#userImageMenu')
</script>
@endpush
