<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.image')}}</th>
        <th>{{trans('label.created_by')}}</th>
        <th>{{trans('label.title')}}</th>
        <th>{{trans('label.description')}}</th>


        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($userImages as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td><img src="{{$row->getImage($row->image)}}" height="100px" alt=""></td>
            <td>{{$row->creatorName('email')}}</td>
            <td>{{$row->title}}</td>
            <td>{{$row->description}}</td>


            <td class="text-right">
                <form method="POST" action="{{route('user-images.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('user-images.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('user-images.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$userImages->links()}}
</div>
