@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('user-image-likes.index')}}">{{trans('table.user_image_likes')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.create')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('user-image-likes.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group col-lg-12">
    <label for="user_images_id">{{trans('label.user_images_id')}}</label>
    <input type="number" required class="form-control" name="user_images_id" id="user_images_id">
</div>
<div class="form-group col-lg-12">
    <label for="created_by">{{trans('label.created_by')}}</label>
    <input type="number" required class="form-control" name="created_by" id="created_by">
</div>

        <div class="col-lg-12">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
<script>
    Menu('#ProfileMenu', '#adaAccountMenu')
</script>
@endpush