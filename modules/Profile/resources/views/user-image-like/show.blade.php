@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('user-image-likes.index')}}">{{trans('table.user_image_likes')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.user_images_id')}}</th>
<td>{!! $user_image_like->user_images_id !!}</td>
</tr><tr><th>{{__('label.created_by')}}</th>
<td>{!! $user_image_like->created_by !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#ProfileMenu', '#userImageLikeMenu')
</script>
@endpush
