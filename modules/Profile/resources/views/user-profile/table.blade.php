<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.image_one')}}</th>
        <th>{{trans('label.image_two')}}</th>
        <th>{{trans('label.image_three')}}</th>
        <th>{{trans('label.user_id')}}</th>
        <th>{{trans('label.height')}}</th>
        <th>{{trans('label.weight')}}</th>
        <th>{{trans('label.round_one')}}</th>
        <th>{{trans('label.round_two')}}</th>
        <th>{{trans('label.round_three')}}</th>

        {{--<th>{{trans('label.website')}}</th>--}}
        {{--<th>{{trans('label.hobby')}}</th>--}}
        {{--<th>{{trans('label.slogan')}}</th>--}}

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($userProfiles as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td><img src="{{$row->getImage($row->image_one)}}" alt="" height="200px"></td>
            <td><img src="{{$row->getImage($row->image_two)}}" alt="" height="200px"></td>
            <td><img src="{{$row->getImage($row->image_three)}}" alt="" height="200px"></td>
            <td>{{$row->user_id}}</td>
            <td>{{$row->height}}</td>
            <td>{{$row->weight}}</td>
            <td>{{$row->round_one}}</td>
            <td>{{$row->round_two}}</td>
            <td>{{$row->round_three}}</td>

            {{--<td>{{$row->website}}</td>--}}
            {{--<td>{{$row->hobby}}</td>--}}
            {{--<td>{{$row->slogan}}</td>--}}

            <td class="text-right">
                <form method="POST" action="{{route('user-profiles.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('user-profiles.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('user-profiles.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$userProfiles->links()}}
</div>
