@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('user-profiles.index')}}">{{trans('table.user_profiles')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.update')}}</strong>
        </li>
    </ol>
    <div class="row">
        <form action="{{route('user-profiles.update', $userProfile->id)}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group col-lg-2 col-md-2">
                <label for="height">{{trans('label.height')}}</label>
                <input required type="number" class="form-control" name="height" id="height"
                       value="{{$userProfile->height}}">
            </div>
            <div class="form-group col-lg-2 col-md-2">
                <label for="weight">{{trans('label.weight')}}</label>
                <input required type="number" class="form-control" name="weight" id="weight"
                       value="{{$userProfile->weight}}">
            </div>
            <div class="form-group col-lg-2 col-md-2">
                <label for="round_one">{{trans('label.round_one')}}</label>
                <input required type="number" class="form-control" name="round_one" id="round_one"
                       value="{{$userProfile->round_one}}">
            </div>
            <div class="form-group col-lg-2 col-md-2">
                <label for="round_two">{{trans('label.round_two')}}</label>
                <input required type="number" class="form-control" name="round_two" id="round_two"
                       value="{{$userProfile->round_two}}">
            </div>
            <div class="form-group col-lg-2 col-md-2">
                <label for="round_three">{{trans('label.round_three')}}</label>
                <input required type="number" class="form-control" name="round_three" id="round_three"
                       value="{{$userProfile->round_three}}">
            </div>
            <div class="form-group col-lg-12 col-md-12">
                <label for="website">{{trans('label.website')}}</label>
                <input required class="form-control" name="website" id="website" value="{{$userProfile->website}}">
            </div>
            <div class="form-group col-lg-12 col-md-12">
                <label for="hobby">{{trans('label.hobby')}}</label>
                <input required class="form-control" name="hobby" id="hobby" value="{{$userProfile->hobby}}">
            </div>
            <div class="form-group col-lg-12 col-md-12">
                <label for="slogan">{{trans('label.slogan')}}</label>
                <input required class="form-control" name="slogan" id="slogan" value="{{$userProfile->slogan}}">
            </div>

            <div class="form-group col-lg-4 col-md-4">
                <label for="image_one">{{trans('label.image_one')}}</label>
                <input type="file" name="image_one" id="image_one">
            </div>
            <div class="form-group col-lg-4 col-md-4">
                <label for="image_two">{{trans('label.image_two')}}</label>
                <input type="file" name="image_two" id="image_two">
            </div>
            <div class="form-group col-lg-4 col-md-4">
                <label for="image_three">{{trans('label.image_three')}}</label>
                <input type="file" name="image_three" id="image_three">
            </div>

            <div class="col-lg-12 col-md-12 form-group">
                <button class="btn btn-primary form-group">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack form-group">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default form-group">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default form-group">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script>
        Menu('#ProfileMenu', '#userProfileMenu')
    </script>
@endpush
