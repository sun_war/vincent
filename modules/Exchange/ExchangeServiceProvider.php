<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/15/19
 * Time: 9:59 PM
 */

namespace Exchange;


use Exchange\Http\Repositories\BuyRepository;
use Exchange\Http\Repositories\BuyRepositoryEloquent;
use Exchange\Http\Repositories\DepositRepository;
use Exchange\Http\Repositories\DepositRepositoryEloquent;
use Exchange\Http\Repositories\HotWalletRepository;
use Exchange\Http\Repositories\HotWalletRepositoryEloquent;
use Exchange\Http\Repositories\OrderBuyRepository;
use Exchange\Http\Repositories\OrderBuyRepositoryEloquent;
use Exchange\Http\Repositories\OrderSellRepository;
use Exchange\Http\Repositories\OrderSellRepositoryEloquent;
use Exchange\Http\Repositories\SellRepository;
use Exchange\Http\Repositories\SellRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class ExchangeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'exchange');
        $this->loadRoutesFrom(__DIR__ . '/routers/api.php');
        $this->loadRoutesFrom(__DIR__ . '/routers/web.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BuyRepository::class, BuyRepositoryEloquent::class);
        $this->app->bind(DepositRepository::class, DepositRepositoryEloquent::class);
        $this->app->bind(HotWalletRepository::class, HotWalletRepositoryEloquent::class);
        $this->app->bind(OrderBuyRepository::class, OrderBuyRepositoryEloquent::class);
        $this->app->bind(OrderSellRepository::class, OrderSellRepositoryEloquent::class);
        $this->app->bind(SellRepository::class, SellRepositoryEloquent::class);
    }
}