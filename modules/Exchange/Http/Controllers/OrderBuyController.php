<?php

namespace Exchange\Http\Controllers;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\OrderBuyCreateRequest;
use Exchange\Http\Requests\OrderBuyUpdateRequest;
use Exchange\Http\Repositories\OrderBuyRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class OrderBuyController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(OrderBuyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['orderBuys'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('exchange::order-buy.table', $data)->render();
        }
        return view('exchange::order-buy.index', $data);
    }

    public function create()
    {
        return view('exchange::order-buy.create');
    }

    public function store(OrderBuyCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('order-buys.index');
    }

    public function show($id)
    {
        $orderBuy = $this->repository->find($id);
        if (empty($orderBuy)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::order-buy.show', compact('orderBuy'));
    }

    public function edit($id)
    {
        $orderBuy = $this->repository->find($id);
        if (empty($orderBuy)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::order-buy.update', compact('orderBuy'));
    }

    public function update(OrderBuyUpdateRequest $request, $id)
    {
        $input = $request->all();
        $orderBuy = $this->repository->find($id);
        if (empty($orderBuy)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $orderBuy);
        session()->flash('success', 'update success');
        return redirect()->route('order-buys.index');
    }

    public function destroy($id)
    {
        $orderBuy = $this->repository->find($id);
        if (empty($orderBuy)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
