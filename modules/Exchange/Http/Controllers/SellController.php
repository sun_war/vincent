<?php

namespace Exchange\Http\Controllers;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\SellCreateRequest;
use Exchange\Http\Requests\SellUpdateRequest;
use Exchange\Http\Repositories\SellRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class SellController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(SellRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['sells'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('exchange::sell.table', $data)->render();
        }
        return view('exchange::sell.index', $data);
    }

    public function create()
    {
        return view('exchange::sell.create');
    }

    public function store(SellCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('sells.index');
    }

    public function show($id)
    {
        $sell = $this->repository->find($id);
        if (empty($sell)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::sell.show', compact('sell'));
    }

    public function edit($id)
    {
        $sell = $this->repository->find($id);
        if (empty($sell)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::sell.update', compact('sell'));
    }

    public function update(SellUpdateRequest $request, $id)
    {
        $input = $request->all();
        $sell = $this->repository->find($id);
        if (empty($sell)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $sell);
        session()->flash('success', 'update success');
        return redirect()->route('sells.index');
    }

    public function destroy($id)
    {
        $sell = $this->repository->find($id);
        if (empty($sell)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
