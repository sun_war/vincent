<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Exchange\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\HotWalletCreateRequest;
use Exchange\Http\Requests\HotWalletUpdateRequest;
use Exchange\Http\Resources\HotWalletResource;
use Exchange\Http\Repositories\HotWalletRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class HotWalletApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(HotWalletRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new HotWalletResource($data);
    }

    public function create()
    {
        return view('exchange::hot-wallet.create');
    }

    public function store(HotWalletCreateRequest $request)
    {
        $input = $request->all();
        $hotWallet = $this->repository->store($input);
        return new HotWalletResource($hotWallet);
    }

    public function show($id)
    {
        $hotWallet = $this->repository->find($id);
        if (empty($hotWallet)) {
            return new HotWalletResource([$hotWallet]);
        }
        return new HotWalletResource($hotWallet);
    }

    public function edit($id)
    {
        $hotWallet = $this->repository->find($id);
        if (empty($hotWallet)) {
            return new HotWalletResource([$hotWallet]);
        }
        return new HotWalletResource($hotWallet);
    }

    public function update(HotWalletUpdateRequest $request, $id)
    {
        $input = $request->all();
        $hotWallet = $this->repository->find($id);
        if (empty($hotWallet)) {
            return new HotWalletResource([$hotWallet]);
        }
        $data = $this->repository->change($input, $hotWallet);
        return new HotWalletResource($data);
    }

    public function destroy($id)
    {
        $hotWallet = $this->repository->find($id);
        if (empty($hotWallet)) {
            return new HotWalletResource($hotWallet);
        }
        $data = $this->repository->delete($id);
        return new HotWalletResource([$data]);
    }
}