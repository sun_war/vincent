<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Exchange\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\OrderBuyCreateRequest;
use Exchange\Http\Requests\OrderBuyUpdateRequest;
use Exchange\Http\Resources\OrderBuyResource;
use Exchange\Http\Repositories\OrderBuyRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class OrderBuyApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(OrderBuyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new OrderBuyResource($data);
    }

    public function create()
    {
        return view('exchange::order-buy.create');
    }

    public function store(OrderBuyCreateRequest $request)
    {
        $input = $request->all();
        $orderBuy = $this->repository->store($input);
        return new OrderBuyResource($orderBuy);
    }

    public function show($id)
    {
        $orderBuy = $this->repository->find($id);
        if (empty($orderBuy)) {
            return new OrderBuyResource([$orderBuy]);
        }
        return new OrderBuyResource($orderBuy);
    }

    public function edit($id)
    {
        $orderBuy = $this->repository->find($id);
        if (empty($orderBuy)) {
            return new OrderBuyResource([$orderBuy]);
        }
        return new OrderBuyResource($orderBuy);
    }

    public function update(OrderBuyUpdateRequest $request, $id)
    {
        $input = $request->all();
        $orderBuy = $this->repository->find($id);
        if (empty($orderBuy)) {
            return new OrderBuyResource([$orderBuy]);
        }
        $data = $this->repository->change($input, $orderBuy);
        return new OrderBuyResource($data);
    }

    public function destroy($id)
    {
        $orderBuy = $this->repository->find($id);
        if (empty($orderBuy)) {
            return new OrderBuyResource($orderBuy);
        }
        $data = $this->repository->delete($id);
        return new OrderBuyResource([$data]);
    }
}