<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Exchange\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\BuyCreateRequest;
use Exchange\Http\Requests\BuyUpdateRequest;
use Exchange\Http\Resources\BuyResource;
use Exchange\Http\Repositories\BuyRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class BuyApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(BuyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new BuyResource($data);
    }

    public function create()
    {
        return view('exchange::buy.create');
    }

    public function store(BuyCreateRequest $request)
    {
        $input = $request->all();
        $buy = $this->repository->store($input);
        return new BuyResource($buy);
    }

    public function show($id)
    {
        $buy = $this->repository->find($id);
        if (empty($buy)) {
            return new BuyResource([$buy]);
        }
        return new BuyResource($buy);
    }

    public function edit($id)
    {
        $buy = $this->repository->find($id);
        if (empty($buy)) {
            return new BuyResource([$buy]);
        }
        return new BuyResource($buy);
    }

    public function update(BuyUpdateRequest $request, $id)
    {
        $input = $request->all();
        $buy = $this->repository->find($id);
        if (empty($buy)) {
            return new BuyResource([$buy]);
        }
        $data = $this->repository->change($input, $buy);
        return new BuyResource($data);
    }

    public function destroy($id)
    {
        $buy = $this->repository->find($id);
        if (empty($buy)) {
            return new BuyResource($buy);
        }
        $data = $this->repository->delete($id);
        return new BuyResource([$data]);
    }
}