<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Exchange\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\SellCreateRequest;
use Exchange\Http\Requests\SellUpdateRequest;
use Exchange\Http\Resources\SellResource;
use Exchange\Http\Repositories\SellRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class SellApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(SellRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new SellResource($data);
    }

    public function create()
    {
        return view('exchange::sell.create');
    }

    public function store(SellCreateRequest $request)
    {
        $input = $request->all();
        $sell = $this->repository->store($input);
        return new SellResource($sell);
    }

    public function show($id)
    {
        $sell = $this->repository->find($id);
        if (empty($sell)) {
            return new SellResource([$sell]);
        }
        return new SellResource($sell);
    }

    public function edit($id)
    {
        $sell = $this->repository->find($id);
        if (empty($sell)) {
            return new SellResource([$sell]);
        }
        return new SellResource($sell);
    }

    public function update(SellUpdateRequest $request, $id)
    {
        $input = $request->all();
        $sell = $this->repository->find($id);
        if (empty($sell)) {
            return new SellResource([$sell]);
        }
        $data = $this->repository->change($input, $sell);
        return new SellResource($data);
    }

    public function destroy($id)
    {
        $sell = $this->repository->find($id);
        if (empty($sell)) {
            return new SellResource($sell);
        }
        $data = $this->repository->delete($id);
        return new SellResource([$data]);
    }
}