<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Exchange\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\DepositCreateRequest;
use Exchange\Http\Requests\DepositUpdateRequest;
use Exchange\Http\Resources\DepositResource;
use Exchange\Http\Repositories\DepositRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class DepositApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(DepositRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new DepositResource($data);
    }

    public function create()
    {
        return view('exchange::deposit.create');
    }

    public function store(DepositCreateRequest $request)
    {
        $input = $request->all();
        $deposit = $this->repository->store($input);
        return new DepositResource($deposit);
    }

    public function show($id)
    {
        $deposit = $this->repository->find($id);
        if (empty($deposit)) {
            return new DepositResource([$deposit]);
        }
        return new DepositResource($deposit);
    }

    public function edit($id)
    {
        $deposit = $this->repository->find($id);
        if (empty($deposit)) {
            return new DepositResource([$deposit]);
        }
        return new DepositResource($deposit);
    }

    public function update(DepositUpdateRequest $request, $id)
    {
        $input = $request->all();
        $deposit = $this->repository->find($id);
        if (empty($deposit)) {
            return new DepositResource([$deposit]);
        }
        $data = $this->repository->change($input, $deposit);
        return new DepositResource($data);
    }

    public function destroy($id)
    {
        $deposit = $this->repository->find($id);
        if (empty($deposit)) {
            return new DepositResource($deposit);
        }
        $data = $this->repository->delete($id);
        return new DepositResource([$data]);
    }
}