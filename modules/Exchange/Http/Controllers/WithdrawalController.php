<?php

namespace Exchange\Http\Controllers;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\WithdrawalCreateRequest;
use Exchange\Http\Requests\WithdrawalUpdateRequest;
use Exchange\Http\Repositories\WithdrawalRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class WithdrawalController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(WithdrawalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['withdrawals'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('exchange::withdrawal.table', $data)->render();
        }
        return view('exchange::withdrawal.index', $data);
    }

    public function create()
    {
        return view('exchange::withdrawal.create');
    }

    public function store(WithdrawalCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('withdrawals.index');
    }

    public function show($id)
    {
        $withdrawal = $this->repository->find($id);
        if (empty($withdrawal)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::withdrawal.show', compact('withdrawal'));
    }

    public function edit($id)
    {
        $withdrawal = $this->repository->find($id);
        if (empty($withdrawal)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::withdrawal.update', compact('withdrawal'));
    }

    public function update(WithdrawalUpdateRequest $request, $id)
    {
        $input = $request->all();
        $withdrawal = $this->repository->find($id);
        if (empty($withdrawal)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $withdrawal);
        session()->flash('success', 'update success');
        return redirect()->route('withdrawals.index');
    }

    public function destroy($id)
    {
        $withdrawal = $this->repository->find($id);
        if (empty($withdrawal)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
