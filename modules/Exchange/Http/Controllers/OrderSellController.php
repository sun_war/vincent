<?php

namespace Exchange\Http\Controllers;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\OrderSellCreateRequest;
use Exchange\Http\Requests\OrderSellUpdateRequest;
use Exchange\Http\Repositories\OrderSellRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class OrderSellController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(OrderSellRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['orderSells'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('exchange::order-sell.table', $data)->render();
        }
        return view('exchange::order-sell.index', $data);
    }

    public function create()
    {
        return view('exchange::order-sell.create');
    }

    public function store(OrderSellCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('order-sells.index');
    }

    public function show($id)
    {
        $orderSell = $this->repository->find($id);
        if (empty($orderSell)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::order-sell.show', compact('orderSell'));
    }

    public function edit($id)
    {
        $orderSell = $this->repository->find($id);
        if (empty($orderSell)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::order-sell.update', compact('orderSell'));
    }

    public function update(OrderSellUpdateRequest $request, $id)
    {
        $input = $request->all();
        $orderSell = $this->repository->find($id);
        if (empty($orderSell)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $orderSell);
        session()->flash('success', 'update success');
        return redirect()->route('order-sells.index');
    }

    public function destroy($id)
    {
        $orderSell = $this->repository->find($id);
        if (empty($orderSell)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
