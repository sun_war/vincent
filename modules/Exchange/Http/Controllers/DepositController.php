<?php

namespace Exchange\Http\Controllers;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\DepositCreateRequest;
use Exchange\Http\Requests\DepositUpdateRequest;
use Exchange\Http\Repositories\DepositRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class DepositController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(DepositRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['deposits'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('exchange::deposit.table', $data)->render();
        }
        return view('exchange::deposit.index', $data);
    }

    public function create()
    {
        return view('exchange::deposit.create');
    }

    public function store(DepositCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('deposits.index');
    }

    public function show($id)
    {
        $deposit = $this->repository->find($id);
        if (empty($deposit)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::deposit.show', compact('deposit'));
    }

    public function edit($id)
    {
        $deposit = $this->repository->find($id);
        if (empty($deposit)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::deposit.update', compact('deposit'));
    }

    public function update(DepositUpdateRequest $request, $id)
    {
        $input = $request->all();
        $deposit = $this->repository->find($id);
        if (empty($deposit)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $deposit);
        session()->flash('success', 'update success');
        return redirect()->route('deposits.index');
    }

    public function destroy($id)
    {
        $deposit = $this->repository->find($id);
        if (empty($deposit)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
