<?php

namespace Exchange\Http\Controllers;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\BuyCreateRequest;
use Exchange\Http\Requests\BuyUpdateRequest;
use Exchange\Http\Repositories\BuyRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class BuyController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(BuyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['buys'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('exchange::buy.table', $data)->render();
        }
        return view('exchange::buy.index', $data);
    }

    public function create()
    {
        return view('exchange::buy.create');
    }

    public function store(BuyCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('buys.index');
    }

    public function show($id)
    {
        $buy = $this->repository->find($id);
        if (empty($buy)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::buy.show', compact('buy'));
    }

    public function edit($id)
    {
        $buy = $this->repository->find($id);
        if (empty($buy)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::buy.update', compact('buy'));
    }

    public function update(BuyUpdateRequest $request, $id)
    {
        $input = $request->all();
        $buy = $this->repository->find($id);
        if (empty($buy)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $buy);
        session()->flash('success', 'update success');
        return redirect()->route('buys.index');
    }

    public function destroy($id)
    {
        $buy = $this->repository->find($id);
        if (empty($buy)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
