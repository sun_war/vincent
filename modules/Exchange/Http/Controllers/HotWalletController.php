<?php

namespace Exchange\Http\Controllers;

use App\Http\Controllers\Controller;
use Exchange\Http\Requests\HotWalletCreateRequest;
use Exchange\Http\Requests\HotWalletUpdateRequest;
use Exchange\Http\Repositories\HotWalletRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class HotWalletController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(HotWalletRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['hotWallets'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('exchange::hot-wallet.table', $data)->render();
        }
        return view('exchange::hot-wallet.index', $data);
    }

    public function create()
    {
        return view('exchange::hot-wallet.create');
    }

    public function store(HotWalletCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('hot-wallets.index');
    }

    public function show($id)
    {
        $hotWallet = $this->repository->find($id);
        if (empty($hotWallet)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::hot-wallet.show', compact('hotWallet'));
    }

    public function edit($id)
    {
        $hotWallet = $this->repository->find($id);
        if (empty($hotWallet)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('exchange::hot-wallet.update', compact('hotWallet'));
    }

    public function update(HotWalletUpdateRequest $request, $id)
    {
        $input = $request->all();
        $hotWallet = $this->repository->find($id);
        if (empty($hotWallet)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $hotWallet);
        session()->flash('success', 'update success');
        return redirect()->route('hot-wallets.index');
    }

    public function destroy($id)
    {
        $hotWallet = $this->repository->find($id);
        if (empty($hotWallet)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
