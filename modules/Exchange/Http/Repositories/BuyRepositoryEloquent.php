<?php

namespace Exchange\Http\Repositories;


use Modularization\MultiInheritance\RepositoriesTrait;

use Illuminate\Support\Facades\Cache;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Exchange\Models\Buy;

/**
 * Class NewsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BuyRepositoryEloquent extends BaseRepository implements BuyRepository
{
    use RepositoriesTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Buy::class;
    }

    public function myPaginate($input)
    {
        isset($input[PER_PAGE]) ?: $input[PER_PAGE] = 10;
        return $this->makeModel()
            ->filter($input)
            ->orderBy('id', 'DESC')
            ->paginate($input[PER_PAGE]);
    }

    public function store($input)
    {
        $input[USER_ID_COL] = auth()->id();
        return $this->create($input);
    }

    public function edit($id)
    {
        $buy = $this->find($id);
        if (empty($buy)) {
            return $buy;
        }
        return compact('buy');
    }

    public function change($input, $data)
    {
        $input[USER_ID_COL] = auth()->id();
        return $this->update($input, $data->id);
    }

    public function import($file)
    {
        set_time_limit(9999);
        $path = $this->makeModel()->uploadImport($file);
        return $this->importing($path);
    }

    private function standardized($input, $data)
    {
        return $data->uploads($input);
    }

    public function destroy($data)
    {
        return $this->delete($data->id);
    }

    /**
     * Boot up the repository, ping criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
