<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/15/19
 * Time: 10:00 PM
 */
namespace Exchange\Http\Controllers;

use Exchange\Http\Repositories\BuyRepository;
use Exchange\Http\Repositories\SellRepository;
use Illuminate\Http\Request;

class ExchangeController
{
    private $buyRepo, $sellRepo;

    public function __construct(BuyRepository $buyRepo, SellRepository $sellRepo)
    {
        $this->buyRepo = $buyRepo;
        $this->sellRepo = $sellRepo;
    }

    public function index(Request $request) {
        $input = $request->all();
        $buys = $this->buyRepo->myPaginate($input);
        $sells = $this->sellRepo->myPaginate($input);
        $data = compact('buys', 'sells');
        return view('exchange::index', $data);
    }
}