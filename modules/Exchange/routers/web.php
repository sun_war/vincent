<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/15/19
 * Time: 10:12 PM
 */

Route::group(['namespace' => 'Exchange\Http\Controllers', 'middleware' => ['web', 'auth'], 'prefix' => 'exchange'], function () {
    Route::resource('buys' , 'BuyController');
    Route::resource('deposits' , 'DepositController');
    Route::resource('order-buys' , 'OrderBuyController');
    Route::resource('sells' , 'SellController');
    Route::resource('hot-wallets' , 'HotWalletController');
    Route::resource('order-sells' , 'OrderSellController');

    Route::get('/' , 'ExchangeController@index')->name('exchange');
});