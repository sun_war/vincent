<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/15/19
 * Time: 10:12 PM
 */

Route::name('api.')
    ->namespace('Exchange\Http\Controllers\API')
    ->middleware(['api', 'auth'])
    ->group(function () {
        Route::resource('buys', 'BuyApiController');
        Route::resource('deposits' , 'DepositApiController');
        Route::resource('order-buys' , 'OrderBuyApiController');
        Route::resource('hot-wallets', 'HotWalletApiController');
        Route::resource('sells', 'SellApiController');
//        Route::resource('order-sells' , 'OrderSellApiController');
    });

Route::name('frontend.')
    ->namespace('Exchange\Http\Controllers\Frontend')
    ->middleware(['web', 'auth'])
    ->group(function () {

    });
