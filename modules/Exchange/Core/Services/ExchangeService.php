<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/22/19
 * Time: 12:11 AM
 */

namespace Exchange\Core\Services;


use Exchange\Http\Repositories\BuyRepository;
use Exchange\Http\Repositories\SellRepository;

class ExchangeService
{
    private $buyRepo, $sellRepo;

    public function __construct(BuyRepository $buyRepo, SellRepository $sellRepo)
    {
        $this->buyRepo = $buyRepo;
        $this->sellRepo = $sellRepo;
    }

    public function index($input) {
        $buys = $this->buyRepo->myPaginate($input);
        $sells = $this->sellRepo->myPaginate($input);
        return compact('buys', 'sells');
    }
}