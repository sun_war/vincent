<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.user_id')}}</th>
<th>{{trans('label.price')}}</th>
<th>{{trans('label.amount')}}</th>
<th>{{trans('label.total')}}</th>
<th>{{trans('label.status')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($sells as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->user_id}}</td>
<td>{{$row->price}}</td>
<td>{{$row->amount}}</td>
<td>{{$row->total}}</td>
<td>{{$row->status}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('sells.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('sells.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('sells.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$sells->links()}}
</div>
