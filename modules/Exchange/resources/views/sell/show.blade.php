@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('sells.index')}}">{{trans('table.sells')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.price')}}</th>
<td>{!! $sell->price !!}</td>
</tr><tr><th>{{__('label.amount')}}</th>
<td>{!! $sell->amount !!}</td>
</tr><tr><th>{{__('label.total')}}</th>
<td>{!! $sell->total !!}</td>
</tr><tr><th>{{__('label.status')}}</th>
<td>{!! $sell->status !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#ExchangeMenu', '#sellMenu')
</script>
@endpush
