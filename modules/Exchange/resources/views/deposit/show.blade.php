@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('deposits.index')}}">{{trans('table.deposits')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.from')}}</th>
<td>{!! $deposit->from !!}</td>
</tr><tr><th>{{__('label.to')}}</th>
<td>{!! $deposit->to !!}</td>
</tr><tr><th>{{__('label.amount')}}</th>
<td>{!! $deposit->amount !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#ExchangeMenu', '#depositMenu')
</script>
@endpush
