<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.user_id')}}</th>
<th>{{trans('label.from')}}</th>
<th>{{trans('label.to')}}</th>
<th>{{trans('label.amount')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($deposits as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->user_id}}</td>
<td>{{$row->from}}</td>
<td>{{$row->to}}</td>
<td>{{$row->amount}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('deposits.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('deposits.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('deposits.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$deposits->links()}}
</div>
