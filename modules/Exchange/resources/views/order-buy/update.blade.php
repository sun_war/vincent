@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('order-buys.index')}}">{{trans('table.order_buys')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.update')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('order-buys.update', $orderBuy->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-12">
    <label for="buy_id">{{trans('label.buy_id')}}</label>
    <input required type="number" class="form-control" name="buy_id" id="buy_id" value="{{$orderBuy->buy_id}}">
</div>
<div class="form-group col-lg-12">
    <label for="buyer">{{trans('label.buyer')}}</label>
    <input required type="number" class="form-control" name="buyer" id="buyer" value="{{$orderBuy->buyer}}">
</div>
<div class="form-group col-lg-12">
    <label for="price">{{trans('label.price')}}</label>
    <textarea class="form-control" name="price" id="price">{{$orderBuy->price}}</textarea>
</div>
<div class="form-group col-lg-12">
    <label for="amount">{{trans('label.amount')}}</label>
    <textarea class="form-control" name="amount" id="amount">{{$orderBuy->amount}}</textarea>
</div>
<div class="form-group col-lg-12">
    <label for="total">{{trans('label.total')}}</label>
    <textarea class="form-control" name="total" id="total">{{$orderBuy->total}}</textarea>
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        Menu('#ExchangeMenu', '#orderBuyMenu')
    </script>
@endpush
