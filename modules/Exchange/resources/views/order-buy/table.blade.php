<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.buy_id')}}</th>
<th>{{trans('label.buyer')}}</th>
<th>{{trans('label.price')}}</th>
<th>{{trans('label.amount')}}</th>
<th>{{trans('label.total')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($orderBuys as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->buy_id}}</td>
<td>{{$row->buyer}}</td>
<td>{{$row->price}}</td>
<td>{{$row->amount}}</td>
<td>{{$row->total}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('order-buys.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('order-buys.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('order-buys.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$orderBuys->links()}}
</div>
