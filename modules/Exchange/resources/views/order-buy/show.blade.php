@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('order-buys.index')}}">{{trans('table.order_buys')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.buy_id')}}</th>
<td>{!! $order_buy->buy_id !!}</td>
</tr><tr><th>{{__('label.buyer')}}</th>
<td>{!! $order_buy->buyer !!}</td>
</tr><tr><th>{{__('label.price')}}</th>
<td>{!! $order_buy->price !!}</td>
</tr><tr><th>{{__('label.amount')}}</th>
<td>{!! $order_buy->amount !!}</td>
</tr><tr><th>{{__('label.total')}}</th>
<td>{!! $order_buy->total !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#ExchangeMenu', '#orderBuyMenu')
</script>
@endpush
