@extends('edu::layouts.app')
@section('content')
    <div class="row form-group">
        <h1>Exchange</h1>
        <div class="row">
            <div class="col-lg-6">
                <h2>Buy</h2>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Price</th>
                        <th>Amount</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($buys as $buy)
                        <tr>
                            <td>{{$buy->price}}</td>
                            <td>{{$buy->amount}}</td>
                            <td>{{$buy->total}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-lg-6">
                <h2>Sell</h2>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Price</th>
                        <th>Amount</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sells as $sell)
                        <tr>
                            <td>{{$sell->price}}</td>
                            <td>{{$sell->amount}}</td>
                            <td>{{$sell->total}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('frontend.buys.store')}}" class="row" method="POST">
                    {{csrf_field()}}
                    <div class="form-group col-sm-6">
                        <lable>Price</lable>
                        <input type="number" name="price" required class="form-control" step="0.000000001">
                    </div>
                    <div class="form-group col-sm-6">
                        <lable>Amount</lable>
                        <input type="number" name="amount" required class="form-control" step="0.000000001">
                    </div>
                    <div class="form-group col-sm-12">
                        <lable>Total</lable>
                        <input type="number" name="total" required class="form-control" step="0.000000001">
                    </div>
                    <div class="form-group col-sm-12">
                        <button class="btn btn-sm btn-primary">Done</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-6">

                <form action="{{route('frontend.sells.store')}}" class="row" method="POST">
                    {{csrf_field()}}
                    <div class="form-group col-sm-6">
                        <lable>Price</lable>
                        <input type="number" name="price" required class="form-control" step="0.000000001">
                    </div>
                    <div class="form-group col-sm-6">
                        <lable>Amount</lable>
                        <input type="number" name="amount" required class="form-control" step="0.000000001">
                    </div>
                    <div class="form-group col-sm-12">
                        <lable>Total</lable>
                        <input type="number" name="total" required class="form-control" step="0.000000001">
                    </div>
                    <div class="form-group col-sm-12">
                        <button class="btn btn-sm btn-primary">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript" src="{{asset('modules/BlockChain/js/app.js')}}?v={{rand(0, 9999)}}"></script>
@endpush