<li class="has-sub root-level" id="ExchangeMenu"><a>
        <i class="fa fa-file"></i>
        <span class="title">{{__('menu.Exchange')}}</span> </a>
    <ul>
        <li id="buysMenu">
            <a href="{{route('buys.index')}}">
                <span class="title">{{__('table.buys')}}</span>
            </a>
        </li>
        <li id="depositsMenu">
            <a href="{{route('deposits.index')}}">
                <span class="title">{{__('table.deposits')}}</span>
            </a>
        </li>
        <li id="sellsMenu">
            <a href="{{route('sells.index')}}">
                <span class="title">{{__('table.sells')}}</span>
            </a>
        </li>
        <li id="hotWalletsMenu">
            <a href="{{route('hot-wallets.index')}}">
                <span class="title">{{__('table.hot_wallets')}}</span>
            </a>
        </li>
        <li id="orderBuysMenu">
            <a href="{{route('order-buys.index')}}">
                <span class="title">{{__('table.order_buys')}}</span>
            </a>
        </li>
        <li id="orderSellsMenu">
            <a href="{{route('order-sells.index')}}">
                <span class="title">{{__('table.order_sells')}}</span>
            </a>
        </li>
    </ul>
</li>
