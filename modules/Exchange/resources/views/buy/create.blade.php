@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('buys.index')}}">{{trans('table.buys')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.create')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('buys.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group col-lg-12">
    <label for="price">{{trans('label.price')}}</label>
    <textarea class="form-control" name="price" id="price"></textarea>
</div>
<div class="form-group col-lg-12">
    <label for="amount">{{trans('label.amount')}}</label>
    <textarea class="form-control" name="amount" id="amount"></textarea>
</div>
<div class="form-group col-lg-12">
    <label for="total">{{trans('label.total')}}</label>
    <textarea class="form-control" name="total" id="total"></textarea>
</div>
<div class="form-group col-lg-12">
    <label for="status">{{trans('label.status')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" required checked value="1" name="status" id="status">
        </label>
    </div>
</div>


        <div class="col-lg-12">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
<script>
    Menu('#ExchangeMenu', '#adaAccountMenu')
</script>
@endpush