@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('buys.index')}}">{{trans('table.buys')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.price')}}</th>
<td>{!! $buy->price !!}</td>
</tr><tr><th>{{__('label.amount')}}</th>
<td>{!! $buy->amount !!}</td>
</tr><tr><th>{{__('label.total')}}</th>
<td>{!! $buy->total !!}</td>
</tr><tr><th>{{__('label.status')}}</th>
<td>{!! $buy->status !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#ExchangeMenu', '#buyMenu')
</script>
@endpush
