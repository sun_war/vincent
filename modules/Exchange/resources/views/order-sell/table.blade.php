<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.sell_id')}}</th>
<th>{{trans('label.seller')}}</th>
<th>{{trans('label.price')}}</th>
<th>{{trans('label.amount')}}</th>
<th>{{trans('label.total')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($orderSells as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->sell_id}}</td>
<td>{{$row->seller}}</td>
<td>{{$row->price}}</td>
<td>{{$row->amount}}</td>
<td>{{$row->total}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('order-sells.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('order-sells.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('order-sells.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$orderSells->links()}}
</div>
