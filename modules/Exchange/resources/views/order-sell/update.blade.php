@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('order-sells.index')}}">{{trans('table.order_sells')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.update')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('order-sells.update', $orderSell->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-12">
    <label for="sell_id">{{trans('label.sell_id')}}</label>
    <input required type="number" class="form-control" name="sell_id" id="sell_id" value="{{$orderSell->sell_id}}">
</div>
<div class="form-group col-lg-12">
    <label for="seller">{{trans('label.seller')}}</label>
    <input required type="number" class="form-control" name="seller" id="seller" value="{{$orderSell->seller}}">
</div>
<div class="form-group col-lg-12">
    <label for="price">{{trans('label.price')}}</label>
    <textarea class="form-control" name="price" id="price">{{$orderSell->price}}</textarea>
</div>
<div class="form-group col-lg-12">
    <label for="amount">{{trans('label.amount')}}</label>
    <textarea class="form-control" name="amount" id="amount">{{$orderSell->amount}}</textarea>
</div>
<div class="form-group col-lg-12">
    <label for="total">{{trans('label.total')}}</label>
    <textarea class="form-control" name="total" id="total">{{$orderSell->total}}</textarea>
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        Menu('#ExchangeMenu', '#orderSellMenu')
    </script>
@endpush
