<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.address')}}</th>
<th>{{trans('label.mnemonic')}}</th>
<th>{{trans('label.private_key')}}</th>
<th>{{trans('label.status')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($hotWallets as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->address}}</td>
<td>{{$row->mnemonic}}</td>
<td>{{$row->private_key}}</td>
<td>{{$row->status}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('hot-wallets.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('hot-wallets.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('hot-wallets.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$hotWallets->links()}}
</div>
