@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('hot-wallets.index')}}">{{trans('table.hot_wallets')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.address')}}</th>
<td>{!! $hot_wallet->address !!}</td>
</tr><tr><th>{{__('label.mnemonic')}}</th>
<td>{!! $hot_wallet->mnemonic !!}</td>
</tr><tr><th>{{__('label.private_key')}}</th>
<td>{!! $hot_wallet->private_key !!}</td>
</tr><tr><th>{{__('label.status')}}</th>
<td>{!! $hot_wallet->status !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#ExchangeMenu', '#hotWalletMenu')
</script>
@endpush
