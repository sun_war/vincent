@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('hot-wallets.index')}}">{{trans('table.hot_wallets')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.create')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('hot-wallets.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group col-lg-12">
    <label for="address">{{trans('label.address')}}</label>
    <input required class="form-control" name="address" id="address">
</div>

<div class="form-group col-lg-12">
    <label for="mnemonic">{{trans('label.mnemonic')}}</label>
    <input required class="form-control" name="mnemonic" id="mnemonic">
</div>

<div class="form-group col-lg-12">
    <label for="private_key">{{trans('label.private_key')}}</label>
    <input required class="form-control" name="private_key" id="private_key">
</div>

<div class="form-group col-lg-12">
    <label for="status">{{trans('label.status')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" required checked value="1" name="status" id="status">
        </label>
    </div>
</div>


        <div class="col-lg-12">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
<script>
    Menu('#ExchangeMenu', '#adaAccountMenu')
</script>
@endpush