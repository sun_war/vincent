@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('withdrawals.index')}}">{{trans('table.withdrawals')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.from')}}</th>
<td>{!! $withdrawal->from !!}</td>
</tr><tr><th>{{__('label.to')}}</th>
<td>{!! $withdrawal->to !!}</td>
</tr><tr><th>{{__('label.amount')}}</th>
<td>{!! $withdrawal->amount !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#ExchangeMenu', '#withdrawalMenu')
</script>
@endpush
