<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/23/18
 * Time: 11:51 AM
 */


/**
 * Passport
 */

const CALLBACK_URL = 'http://vincent.vn/callback/';

const CALL_BACK_PASSWORD_URL = 'http://vincent.vn/callback-password/';

const PASSPORT_DOMAIN = 'http://passport.vn/';

Route::get('/callback', function () {

    $http = new GuzzleHttp\Client;

    $response = $http->post(PASSPORT_DOMAIN . 'oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => 3,
            'client_secret' => 'ldmFu174aBDB5eE1DFXEQjWBvMeaa3ZgJrS4bXI4',
            'redirect_uri' => CALLBACK_URL,
            'code' => request('code'),
        ],
    ]);

    /**
     * get token
     */
    $body = json_decode((string)$response->getBody(), true);

    /**
     * From token get info
     */
    $response = $http->get(PASSPORT_DOMAIN . 'api/user', [
        'headers' => [
            'Authorization' => 'Bearer ' . $body['access_token'],
        ],
    ]);

    return json_decode((string)$response->getBody(), true);
});

Route::get('/call-api', function () {
    $http = new GuzzleHttp\Client;
    $auth = [
        'Authorization' => 'Bearer ' . 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQ1NzQ2MjNmZjJmYTY0MGNiNDI0NDQ3Mzk2OWNjMTlhYmM2MDYyZTgyMzk0NDk1YTgzODEzMzRkNjgwY2IzZTRkOTYwZmI1ZWI2YzlkNDU2In0.eyJhdWQiOiI0IiwianRpIjoiNDU3NDYyM2ZmMmZhNjQwY2I0MjQ0NDczOTY5Y2MxOWFiYzYwNjJlODIzOTQ0OTVhODM4MTMzNGQ2ODBjYjNlNGQ5NjBmYjVlYjZjOWQ0NTYiLCJpYXQiOjE1MzgzODI3MjIsIm5iZiI6MTUzODM4MjcyMiwiZXhwIjoxNTM5Njc4NzIyLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.k4Ioh0k6YmBiYGN3TXnfVTs3fK1AufLLs28aozOJA9dwSMe8zihlS61hTiF6Z9yMw7K_hL3PdjfJsia-fFrHd_3f4dsPLwPEplEWKzOUPrZF1-pHgt2ey4Ex6vmp2b46etpdK4vjMTKJAuQ9RtQ9tI3TzjJhIWgxIMfTz780Q2mZ_WjFUqNfQb0VotMUg6LV4SJ9qNJCfYKT4Ffu3T2KM7C74dCdWyx5Q92TI2q4CoXYqhasVIF00ulU9XFQLww6IHe7_FNvsPTGEtp9z3SDXU9zJJBYnXhtmFYosZBC7Bke68C1a5PoxtFYfcGVeqNhK6iF2FAsREbE7cVQggOdUkWpSf1rzaw5-np7cZgTPYwOy3xM0gGcnDlKZa8W3h7_1LSU_h9gMWNHLQ8g1S8rrDdLST_6B6zidt45tdtxaGANbLd9vgNNouDesmz0BaHx5diBU5Cgdl7qw6RBNhNfMDzZnElaQS3Ntx5sYnIpA6TYAVVbQaYXFzOYWxYJtKTEI_uAbim48D1UHMs92ZJct1gIP5KDN7Ui-bj4-HDcm5mD1fl1CF6q7Lt3_3_jpyuPnFuGFpQmuvn_yPuV4XY9zJVvVvhVSjYb274FwivckRrIwjMNCOiilTpAeCyNUiaMwy0mOuQk9lrc4sWeAlPaL5BaIOL8j2XKLbmqc3JiaXo',
    ];

    $response = $http->get(PASSPORT_DOMAIN . 'api/get-auth', [
        'headers' => $auth,
    ]);

    return json_decode((string)$response->getBody(), true);
});

Route::get('refresh-token', function () {
    $http = new GuzzleHttp\Client;

    $response = $http->post(PASSPORT_DOMAIN . 'oauth/token', [
        'form_params' => [
            'grant_type' => 'refresh_token',
            'refresh_token' => 'the-refresh-token',
            'client_id' => 3,
            'client_secret' => 'ldmFu174aBDB5eE1DFXEQjWBvMeaa3ZgJrS4bXI4',
            'scope' => '',
        ],
    ]);

    return json_decode((string) $response->getBody(), true);
});

Route::get('/auth/passport', function () {
    $query = http_build_query([
        'client_id' => '3',
        'redirect_uri' => CALLBACK_URL,
        'response_type' => 'code',
        'scope' => '',
    ]);

    return redirect(PASSPORT_DOMAIN . 'oauth/authorize?'.$query);
});

Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => '3',
        'redirect_uri' => CALLBACK_URL,
        'response_type' => 'token',
        'scope' => '',
    ]);

    return redirect('http://your-app.com/oauth/authorize?'.$query);
});

Route::get('/auth/callback-password', function () {
    $http = new GuzzleHttp\Client;

    $response = $http->post(PASSPORT_DOMAIN . 'oauth/token', [
        'form_params' => [
            'grant_type' => 'password',
            'client_id' => '4',
            'client_secret' => 'XIac7l8Kof9iczHehuKYFAlKK0QUFPbyIVyZooHN',
            'redirect_uri' => CALL_BACK_PASSWORD_URL,
            'code' => request('code'),
        ],
    ]);

    /**
     * get token
     */
    $body = json_decode((string)$response->getBody(), true);

    /**
     * From token get info
     */
    $response = $http->get(PASSPORT_DOMAIN . 'api/user', [
        'headers' => [
            'Authorization' => 'Bearer ' . $body['access_token'],
        ],
    ]);

    return json_decode((string)$response->getBody(), true);
});

Route::get('/auth/passport-password', function () {
    $http = new GuzzleHttp\Client;
    $response = $http->post(PASSPORT_DOMAIN . 'oauth/token', [
        'form_params' => [
            'grant_type' => 'password',
            'client_id' => '4',
            'client_secret' => 'XIac7l8Kof9iczHehuKYFAlKK0QUFPbyIVyZooHN',
            'username' => 'i.am.m.cuong@gmail.com',
            'password' => 'PMC1704bn',
            'scope' => '',
        ],
    ]);

    $body = json_decode((string)$response->getBody(), true);

    $response = $http->get(PASSPORT_DOMAIN . 'api/get-auth', [
        'headers' => [
            'Authorization' => 'Bearer ' . $body['access_token'],
        ],
    ]);

    dump($body['access_token']);

    return json_decode((string) $response->getBody(), true);
});

Route::get('create-client', function () {
    $http = new GuzzleHttp\Client;

    $response = $http->post(PASSPORT_DOMAIN . 'oauth/clients', [
        'form_params' => [
            'name' => str_random(20),
            'redirect' => CALLBACK_URL,
        ],
    ]);

    return json_decode((string)$response->getBody(), true);
});

Route::get('update-client', function ($clientId) {
    $http = new GuzzleHttp\Client;

    $response = $http->put(PASSPORT_DOMAIN . 'oauth/clients' . $clientId, [
        'form_params' => [
            'name' => str_random(20),
            'redirect' => CALLBACK_URL,
        ],
    ]);

    return json_decode((string)$response->getBody(), true);
});

Route::get('delete-client', function ($clientId) {
    $http = new GuzzleHttp\Client;

    $response = $http->delete(PASSPORT_DOMAIN . 'oauth/clients' . $clientId, [

    ]);

    return json_decode((string)$response->getBody(), true);
});