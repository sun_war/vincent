<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/redirect/{social}', 'SocialAuthController@redirect');
Route::get('/callback/{social}', 'SocialAuthController@callback');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
});

Route::group(['namespace' => 'Api', 'middleware' => ['web']], function () {
    Route::resource('lesson-comment-api', 'LessonCommentController');
});

//Route::get('queue', function () {
//    \App\Jobs\ProcessPodcast::dispatch()
//        ->delay(now()
//        ->addMinutes(1));
//    $date = now()->addMinutes(1);
//    \Illuminate\Support\Facades\Queue::later($date, new \App\Jobs\ProcessPodcast());
//    $job = (new \App\Jobs\ProcessPodcast())->delay(now()->addMinute(1));
//    dispatch($job);
//});

Route::get('ssh', function () {
    echo '<pre>';
    \SSH::into('production')->run([
        'cd sites/backend',
        'git status',
        'ls -la | grep "config" && ifconfig',
    ], function ($line) {
        print_r($line . PHP_EOL);
    });
});

Route::get('bash', function () {
    $connection = \ssh2_connect('128.199.229.218', 22);
    \ssh2_auth_password($connection, 'cuongpm', '!12345678#');

    $stream = \ssh2_exec($connection, 'ls -la');
    dd($stream);
});