#install apache
yum install -y httpd
cp ./bin/trading.conf /etc/httpd/conf.d/
cp ./bin/trading_https.conf /etc/httpd/conf.d/

#install mariadb (in order to run mysql command)
echo "[mariadb]" > /etc/yum.repos.d/MariaDB.repo
echo "name = MariaDB" >> /etc/yum.repos.d/MariaDB.repo
echo "baseurl = http://yum.mariadb.org/10.1/rhel7-amd64" >> /etc/yum.repos.d/MariaDB.repo
echo "gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB" >> /etc/yum.repos.d/MariaDB.repo
echo "gpgcheck=1" >> /etc/yum.repos.d/MariaDB.repo

yum install -y mariadb-server

yum install -y mod_ssl
mv /etc/httpd/conf.d/ssl.conf /etc/httpd/conf.d/ssl.conf.bak

#install php
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
yum -y install php71w php71w-opcache php71w-xml php71w-mcrypt php71w-gd php71w-devel php71w-mysqlnd php71w-intl php71w-mbstring php71w-bcmath

cp ./bin/php_extensions/dstk.ini /etc/php.d/
cp ./bin/php_extensions/dstk.so /usr/lib64/php/modules

#install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --install-dir=/usr/local/bin/ --filename=composer
php -r "unlink('composer-setup.php');"

#install nodejs
curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
yum -y install nodejs

#install pm2
npm install pm2 -g 

#make storage and cache writable
mkdir -p /var/www/trading/bootstrap/cache
mkdir -p /var/www/trading/storage/framework/cache
mkdir -p /var/www/trading/storage/framework/sessions
mkdir -p /var/www/trading/storage/framework/views
mkdir -p /var/www/trading/storage/logs
mkdir -p /var/www/trading/public/
mkdir -p /var/www/trading/storage/app/public/qr_codes
chown -R apache:apache /var/www/trading/storage
chown -R apache:apache /var/www/trading/bootstrap/cache
setfacl -d -m g:apache:rwx /var/www/trading/storage/logs
ln -s /var/www/trading/storage/app/public /var/www/trading/public/storage

restorecon -R /var/www
chcon -t httpd_sys_rw_content_t -R /var/www/trading/storage
chcon -t httpd_sys_rw_content_t -R /var/www/trading/bootstrap/cache

systemctl start httpd.service
systemctl enable httpd.service

# add crontab: check disk free
echo "0 * * * * cd /var/www/trading && ./bin/healthcheck/df.sh mango.alert@sotatek.com" | crontab -

#cd /root/BitGoJS && npm install && pm2 start ./bin/bitgo-express -x -- --debug --port 3080 --env prod --bind 0.0.0.0 --keypath /root/ssl/bitgo.key  --crtpath /root/ssl/bitgo.crt

#yum install supervisor
#cp trading.ini /etc/supervisor.d/
#touch /var/www/trading/storage/logs/worker.log
#chmod 777 /var/www/trading/storage/logs/worker.log
#systemctl start supervisord
#systemctl enable supervisord


#make apache be able to connect network
setsebool -P httpd_can_network_connect 1

#configuration
#env
#migration
#laravel echo server start
#