email=$1
date=`date`
time=`date +%s`

source_folder="/root/mango"
commit=`cd $source_folder && git log -n 1 | sed ':a;N;$!ba;s#\n#<br />#g;s#^<br />##g'`
healthchecks=`crontab -l | grep health | sed ':a;N;$!ba;s#\n#<br />#g;s#^<br />##g'`

subject="Mango deployment $date ($time)"
content="All servers are updated to:<br/>$commit"
content="$content<br/><br/><br/>Current health-checks:<br/>$healthchecks"
cd $source_folder && php artisan email:send $email "$subject" "$content"

