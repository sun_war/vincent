#install mariadb (in order to run mysql command)
echo "[mariadb]" > /etc/yum.repos.d/MariaDB.repo
echo "name = MariaDB" >> /etc/yum.repos.d/MariaDB.repo
echo "baseurl = http://yum.mariadb.org/10.1/rhel7-amd64" >> /etc/yum.repos.d/MariaDB.repo
echo "gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB" >> /etc/yum.repos.d/MariaDB.repo
echo "gpgcheck=1" >> /etc/yum.repos.d/MariaDB.repo

yum install -y mariadb-server

yum install -y mod_ssl
mv /etc/httpd/conf.d/ssl.conf /etc/httpd/conf.d/ssl.conf.bak

#install php
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
yum -y install php71w php71w-opcache php71w-xml php71w-mcrypt php71w-gd php71w-devel php71w-mysqlnd php71w-intl php71w-mbstring php71w-bcmath

cp ./bin/php_extensions/dstk.ini /etc/php.d/
cp ./bin/php_extensions/dstk.so /usr/lib64/php/modules

#install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --install-dir=/usr/local/bin/ --filename=composer

ln -s /usr/local/bin/composer /usr/local/sbin/composer

php -r "unlink('composer-setup.php');"

#install nodejs
curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
yum -y install nodejs

#install pm2
npm install pm2 -g 
