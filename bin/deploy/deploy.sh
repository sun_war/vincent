h=$1
deploy=$2
ssh $h "mkdir -p /var/www/trading"
if [ $deploy -eq "1" ]; then
    rsync -avhzL --delete \
            --no-perms --no-owner --no-group \
            --exclude .git \
            --exclude .idea \
            --exclude .env \
            --exclude bootstrap/cache \
            --exclude storage/logs \
            --exclude storage/framework \
            --exclude storage/app \
            --exclude public/storage \
            /root/mango/ $h:/var/www/trading/
    exit;
fi
rsync -avhzL --delete \
            --no-perms --no-owner --no-group \
            --exclude .git \
            --exclude .idea \
            --exclude .env \
            --exclude bootstrap/cache \
            --exclude storage/logs \
            --exclude storage/framework \
            --exclude storage/app \
            --exclude public/storage \
	    --dry-run \
            /root/mango/ $h:/var/www/trading/

