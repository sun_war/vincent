scp /root/mango/.env web1:/var/www/trading
ssh web1 "apachectl restart"
scp /root/mango/.env web2:/var/www/trading
ssh web2 "apachectl restart"
scp /root/mango/.env queue:/var/www/trading
ssh queue "supervisorctl restart mango-worker:mango-worker_00"
