set -x;
./deploy.sh web1 1
./deploy.sh web2 1
./deploy.sh order 1
ssh order "cd /var/www/trading && ./bin/queue/restart_all.sh"
./deploy.sh queue 1
ssh queue "supervisorctl restart mango-worker:mango-worker_00"

./send_deploy_mail.sh mango.alert@sotatek.com
