Event
```angular2html
php artisan make:event EventName
```

Redis
```angular2html
redis-cli monitor
```

Echo
```angular2html
npm i -g laravel-echo-server
laravel-echo-server init
laravel-echo-server start
```

npm
```angular2html
npm install --save-dev cross-env
```