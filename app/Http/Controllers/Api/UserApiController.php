<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['users'] = $this->repository->myPaginate($input);
        return new UserResource($data);
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(UserCreateRequest $request)
    {
        $input = $request->all();
        $user = $this->repository->store($input);
        return new UserResource($user);
    }

    public function show($id)
    {
        $user = $this->repository->find($id);
        return new UserResource($user);
    }

    public function edit($id)
    {
        $user = $this->repository->find($id);
        if (empty($user)) {
            return new UserResource($user);
        }
        return new UserResource($user);
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $input = $request->all();
        $user = $this->repository->find($id);
        if (empty($user)) {
            return new UserResource($user);
        }
        $data = $this->repository->change($input, $user);
        return new UserResource($data);
    }

    public function destroy($id)
    {
        $user = $this->repository->find($id);
        if (empty($user)) {
            return new UserResource($user);
        }
        $data = $this->repository->delete($id);
        return new UserResource($data);
    }
}