<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ScheduleCreateRequest;
use App\Http\Requests\ScheduleUpdateRequest;
use App\Repositories\ScheduleRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class ScheduleController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(ScheduleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['schedules'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('schedule.table', $data)->render();
        }
        return view('schedule.index', $data);
    }

    public function create()
    {
        return view('schedule.create');
    }

    public function store(ScheduleCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('schedules.index');
    }

    public function show($id)
    {
        $schedule = $this->repository->find($id);
        if (empty($schedule)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('schedule.show', compact('schedule'));
    }

    public function edit($id)
    {
        $schedule = $this->repository->find($id);
        if (empty($schedule)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('schedule.update', compact('schedule'));
    }

    public function update(ScheduleUpdateRequest $request, $id)
    {
        $input = $request->all();
        $schedule = $this->repository->find($id);
        if (empty($schedule)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $schedule);
        session()->flash('success', 'update success');
        return redirect()->route('schedules.index');
    }

    public function destroy($id)
    {
        $schedule = $this->repository->find($id);
        if (empty($schedule)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
