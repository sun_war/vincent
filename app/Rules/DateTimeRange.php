<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modularization\Facades\InputFa;

class DateTimeRange implements Rule
{
    private $value;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->value = $value;
        return InputFa::checkMonth($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute ' . $this->value . ' over range.';
    }
}
