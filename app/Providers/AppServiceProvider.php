<?php

namespace App\Providers;

use ACL\ACLServiceProvider;
use BlockChain\Providers\BlockChainServiceProvider;
use Bugger\BuggerServiceProvider;
use Bugger\EventBuggerServiceProvider;
use Landing\LandingServiceProvider;
use ECommerce\Providers\ECommerceServiceProvider;
use English\EnglishServiceProvider;
use Exchange\ExchangeServiceProvider;
use Games\GameServiceRepository;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use IO\IOServiceProvider;
use Laravel\Passport\Passport;
use Logger\Providers\LoggerEventServiceProvider;
use Logger\Providers\LoggerServiceProvider;
use Media\Providers\MediaServiceProvider;
use Patterns\PatternsServiceProvider;
use Profile\ProfileServiceProvider;
use Searcher\Providers\SearcherServiceProvider;
use Studio\StudioServiceProvider;
use Test\TestServiceProvider;
use Transaction\TransactionServiceProvider;
use Tutorial\Providers\TutorialServiceProvider;
use Vincent\Providers\VincentServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Passport::tokensExpireIn(now()->addDays(15));
        Passport::refreshTokensExpireIn(now()->addDays(30));
        Passport::pruneRevokedTokens();

        Resource::withoutWrapping();
//        Blade::withoutDoubleEncoding();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(ACLServiceProvider::class);
        $this->app->register(BlockChainServiceProvider::class);
        $this->app->register(EventBuggerServiceProvider::class);
        $this->app->register(BuggerServiceProvider::class);
        $this->app->register(LandingServiceProvider::class);
        $this->app->register(ECommerceServiceProvider::class);
        $this->app->register(EnglishServiceProvider::class);
        $this->app->register(ExchangeServiceProvider::class);
        $this->app->register(EventBuggerServiceProvider::class);
        $this->app->register(GameServiceRepository::class);
        $this->app->register(IOServiceProvider::class);
        $this->app->register(LoggerServiceProvider::class);
        $this->app->register(LoggerEventServiceProvider::class);
        $this->app->register(MediaServiceProvider::class);
        $this->app->register(PatternsServiceProvider::class);
        $this->app->register(ProfileServiceProvider::class);
        $this->app->register(SearcherServiceProvider::class);
        $this->app->register(StudioServiceProvider::class);
        $this->app->register(TestServiceProvider::class);
        $this->app->register(TransactionServiceProvider::class);
        $this->app->register(TutorialServiceProvider::class);
        $this->app->register(VincentServiceProvider::class);

//        if ($this->app->isLocal()) {
//            $this->app->register(TelescopeServiceProvider::class);
//        }
    }
}



