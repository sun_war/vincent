<?php

namespace App\Policies;

use App\Models\OrderBuy;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderBuyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    private $roleRepository, $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function view(User $user, OrderBuy $order_buys)
    {
        if ($this->permissionRepository->is('view_order_buy')) {
            return true;
        }
        return $user->id === $order_buys->user_id;
    }

    public function create(User $user)
    {
        if ($this->permissionRepository->is('create_order_buy')) {
            return true;
        }
        return false;
    }

    public function update(User $user, OrderBuy $order_buys)
    {
        if ($this->permissionRepository->is('update_order_buy')) {
            return true;
        }
        return $user->id === $order_buys->user_id;
    }

    public function delete($user, OrderBuy $order_buys)
    {
        if ($this->permissionRepository->is('delete_order_buy')) {
            return true;
        }
        return $user->id === $order_buys->user_id;
    }

}
