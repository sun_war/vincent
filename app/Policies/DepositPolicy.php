<?php

namespace App\Policies;

use App\Models\Deposit;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DepositPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    private $roleRepository, $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function view(User $user, Deposit $deposits)
    {
        if ($this->permissionRepository->is('view_deposit')) {
            return true;
        }
        return $user->id === $deposits->user_id;
    }

    public function create(User $user)
    {
        if ($this->permissionRepository->is('create_deposit')) {
            return true;
        }
        return false;
    }

    public function update(User $user, Deposit $deposits)
    {
        if ($this->permissionRepository->is('update_deposit')) {
            return true;
        }
        return $user->id === $deposits->user_id;
    }

    public function delete($user, Deposit $deposits)
    {
        if ($this->permissionRepository->is('delete_deposit')) {
            return true;
        }
        return $user->id === $deposits->user_id;
    }

}
