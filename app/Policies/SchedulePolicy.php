<?php

namespace App\Policies;

use App\Models\Schedule;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchedulePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    private $roleRepository, $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function view(User $user, Schedule $schedules)
    {
        if ($this->permissionRepository->is('view_schedule')) {
            return true;
        }
        return $user->id === $schedules->user_id;
    }

    public function create(User $user)
    {
        if ($this->permissionRepository->is('create_schedule')) {
            return true;
        }
        return false;
    }

    public function update(User $user, Schedule $schedules)
    {
        if ($this->permissionRepository->is('update_schedule')) {
            return true;
        }
        return $user->id === $schedules->user_id;
    }

    public function delete($user, Schedule $schedules)
    {
        if ($this->permissionRepository->is('delete_schedule')) {
            return true;
        }
        return $user->id === $schedules->user_id;
    }

}
