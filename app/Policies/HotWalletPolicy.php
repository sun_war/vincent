<?php

namespace App\Policies;

use App\Models\HotWallet;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class HotWalletPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    private $roleRepository, $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function view(User $user, HotWallet $hot_wallets)
    {
        if ($this->permissionRepository->is('view_hot_wallet')) {
            return true;
        }
        return $user->id === $hot_wallets->user_id;
    }

    public function create(User $user)
    {
        if ($this->permissionRepository->is('create_hot_wallet')) {
            return true;
        }
        return false;
    }

    public function update(User $user, HotWallet $hot_wallets)
    {
        if ($this->permissionRepository->is('update_hot_wallet')) {
            return true;
        }
        return $user->id === $hot_wallets->user_id;
    }

    public function delete($user, HotWallet $hot_wallets)
    {
        if ($this->permissionRepository->is('delete_hot_wallet')) {
            return true;
        }
        return $user->id === $hot_wallets->user_id;
    }

}
