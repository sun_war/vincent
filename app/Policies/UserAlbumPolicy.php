<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Profile\Models\UserAlbum;

class UserAlbumPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user album.
     *
     * @param  \App\User  $user
     * @param  \App\UserAlbum  $userAlbum
     * @return mixed
     */
    public function view(User $user, UserAlbum $userAlbum)
    {

    }

    /**
     * Determine whether the user can create user albums.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the user album.
     *
     * @param  \App\User  $user
     * @param  \App\UserAlbum  $userAlbum
     * @return mixed
     */
    public function update(User $user, UserAlbum $userAlbum)
    {
        return $user->id === $userAlbum->user_id;
    }

    /**
     * Determine whether the user can delete the user album.
     *
     * @param  \App\User  $user
     * @param  \App\UserAlbum  $userAlbum
     * @return mixed
     */
    public function delete(User $user, UserAlbum $userAlbum)
    {
        return $user->id === $userAlbum->user_id;
    }

    /**
     * Determine whether the user can restore the user album.
     *
     * @param  \App\User  $user
     * @param  \App\UserAlbum  $userAlbum
     * @return mixed
     */
    public function restore(User $user, UserAlbum $userAlbum)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the user album.
     *
     * @param  \App\User  $user
     * @param  \App\UserAlbum  $userAlbum
     * @return mixed
     */
    public function forceDelete(User $user, UserAlbum $userAlbum)
    {
        //
    }
}
