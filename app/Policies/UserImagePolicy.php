<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Profile\Models\UserImage;

class UserImagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user image.
     *
     * @param  \App\User  $user
     * @param  \App\UserImage  $userImage
     * @return mixed
     */
    public function view(User $user, UserImage $userImage)
    {
        //
    }

    /**
     * Determine whether the user can create user images.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the user image.
     *
     * @param  \App\User  $user
     * @param  \App\UserImage  $userImage
     * @return mixed
     */
    public function update(User $user, UserImage $userImage)
    {
        return $user->id === $userImage->user_id;
    }

    /**
     * Determine whether the user can delete the user image.
     *
     * @param  \App\User  $user
     * @param  \App\UserImage  $userImage
     * @return mixed
     */
    public function delete(User $user, UserImage $userImage)
    {
        return $user->id === $userImage->user_id;
    }

    /**
     * Determine whether the user can restore the user image.
     *
     * @param  \App\User  $user
     * @param  \App\UserImage  $userImage
     * @return mixed
     */
    public function restore(User $user, UserImage $userImage)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the user image.
     *
     * @param  \App\User  $user
     * @param  \App\UserImage  $userImage
     * @return mixed
     */
    public function forceDelete(User $user, UserImage $userImage)
    {
        return $user->id === $userImage->user_id;
    }
}
