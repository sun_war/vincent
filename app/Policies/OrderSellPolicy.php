<?php

namespace App\Policies;

use App\Models\OrderSell;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderSellPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    private $roleRepository, $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function view(User $user, OrderSell $order_sells)
    {
        if ($this->permissionRepository->is('view_order_sell')) {
            return true;
        }
        return $user->id === $order_sells->user_id;
    }

    public function create(User $user)
    {
        if ($this->permissionRepository->is('create_order_sell')) {
            return true;
        }
        return false;
    }

    public function update(User $user, OrderSell $order_sells)
    {
        if ($this->permissionRepository->is('update_order_sell')) {
            return true;
        }
        return $user->id === $order_sells->user_id;
    }

    public function delete($user, OrderSell $order_sells)
    {
        if ($this->permissionRepository->is('delete_order_sell')) {
            return true;
        }
        return $user->id === $order_sells->user_id;
    }

}
