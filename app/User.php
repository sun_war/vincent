<?php

namespace App;

use ACL\Models\Role;
use ACL\Models\VerifyUser;
use ACL\Http\Repositories\PermissionRepository;
use ACL\Http\Repositories\RoleRepository;
use BlockChain\Models\Account;
use BlockChain\Models\Crypto;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Laravel\Passport\HasApiTokens;
use Modularization\MultiInheritance\ModelsTrait;
use Profile\Models\UserProfile;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    use ModelsTrait;
//    use HasMultiAuthApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [FIRST_NAME_COL, LAST_NAME_COL, CODE_COL, EMAIL_COL, 'phone_number', 'sex', 'password', 'birthday',
        'address', AVATAR_COL, 'remember_token', 'is_active', 'last_login', 'last_logout'];


    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['full_name'];

    public function scopeFilter($query, $input)
    {
        if (isset($input[EMAIL_COL])) {
            $query->where(EMAIL_COL, 'LIKE', '%' . $input[EMAIL_COL] . '%');
        }
        return $query;
    }

    function getFullNameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function isSuperAdmin()
    {
        return $this->roles()->where('name', 'admin')->count();
    }

    public function hasRole($name, RoleRepository $repository)
    {
        return $repository->is($name);
    }

    public function hasPermission($name, PermissionRepository $repository)
    {
        return $repository->is($name);
    }

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    const OTP_CACHE_LIVE_TIME = 0.5; // 30s

    public function verifyUser()
    {
        return $this->hasOne(VerifyUser::class, USER_ID_COL);
    }

    public function verifyOtp($authenticationCode)
    {
        $googleAuthenticator = new \PHPGangsta_GoogleAuthenticator();
        $result = $googleAuthenticator->verifyCode($this->google_authentication, $authenticationCode, 0);
        if ($result) {
            $key = "OTPCode:{$this->id}:{$authenticationCode}";
            if (Cache::get($key) !== $authenticationCode) {
                Cache::put($key, $authenticationCode, User::OTP_CACHE_LIVE_TIME);
            }
        }
        return $result;
    }

    public function account()
    {
        return $this->hasOne(Account::class, 'user_id');
    }

    public function crypto()
    {
        return $this->hasOne(Crypto::class, 'user_id');
    }

    function profile()
    {
        return $this->hasOne(UserProfile::class, 'user_id');
    }

    public $fileUpload = [AVATAR_COL => 1];
    protected $pathUpload = [AVATAR_COL => '/images/users'];
    protected $thumbImage = [
        AVATAR_COL => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];

}
