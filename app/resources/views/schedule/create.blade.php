@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('schedules.index')}}">{{trans('table.schedules')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.create')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('schedules.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group col-lg-12">
    <label for="client_id">{{trans('label.client_id')}}</label>
    <textarea class="form-control" name="client_id" id="client_id"></textarea>
</div>
<div class="form-group col-lg-12">
    <label for="test_case_id">{{trans('label.test_case_id')}}</label>
    <textarea class="form-control" name="test_case_id" id="test_case_id"></textarea>
</div>
<div class="form-group col-lg-12">
    <label for="name">{{trans('label.name')}}</label>
    <input required class="form-control" name="name" id="name">
</div>

<div class="form-group col-lg-12">
    <label for="test_mode">{{trans('label.test_mode')}}</label>
    <input type="number" required class="form-control" name="test_mode" id="test_mode">
</div>
<div class="form-group col-lg-12">
    <label for="status">{{trans('label.status')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" required checked value="1" name="status" id="status">
        </label>
    </div>
</div>

<div class="form-group col-lg-12">
    <label for="from">{{trans('label.from')}}</label>
    <input type="datetime" required required class="form-control" name="from" id="from">
</div>
<div class="form-group col-lg-12">
    <label for="to">{{trans('label.to')}}</label>
    <input type="datetime" required required class="form-control" name="to" id="to">
</div>
<div class="form-group col-lg-12">
    <label for="test_interval">{{trans('label.test_interval')}}</label>
    <input type="number" required class="form-control" name="test_interval" id="test_interval">
</div>
<div class="form-group col-lg-12">
    <label for="unit">{{trans('label.unit')}}</label>
    <input type="number" required class="form-control" name="unit" id="unit">
</div>

        <div class="col-lg-12">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
<script>
    Menu('#AppMenu', '#adaAccountMenu')
</script>
@endpush