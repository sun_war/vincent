@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('schedules.index')}}">{{trans('table.schedules')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.client_id')}}</th>
<td>{!! $schedule->client_id !!}</td>
</tr><tr><th>{{__('label.test_case_id')}}</th>
<td>{!! $schedule->test_case_id !!}</td>
</tr><tr><th>{{__('label.name')}}</th>
<td>{!! $schedule->name !!}</td>
</tr><tr><th>{{__('label.test_mode')}}</th>
<td>{!! $schedule->test_mode !!}</td>
</tr><tr><th>{{__('label.status')}}</th>
<td>{!! $schedule->status !!}</td>
</tr><tr><th>{{__('label.from')}}</th>
<td>{!! $schedule->from !!}</td>
</tr><tr><th>{{__('label.to')}}</th>
<td>{!! $schedule->to !!}</td>
</tr><tr><th>{{__('label.test_interval')}}</th>
<td>{!! $schedule->test_interval !!}</td>
</tr><tr><th>{{__('label.unit')}}</th>
<td>{!! $schedule->unit !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#AppMenu', '#scheduleMenu')
</script>
@endpush
