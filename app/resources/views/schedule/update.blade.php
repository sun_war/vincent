@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('schedules.index')}}">{{trans('table.schedules')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.update')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('schedules.update', $schedule->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-12">
    <label for="client_id">{{trans('label.client_id')}}</label>
    <textarea class="form-control" name="client_id" id="client_id">{{$schedule->client_id}}</textarea>
</div>
<div class="form-group col-lg-12">
    <label for="test_case_id">{{trans('label.test_case_id')}}</label>
    <textarea class="form-control" name="test_case_id" id="test_case_id">{{$schedule->test_case_id}}</textarea>
</div>
<div class="form-group col-lg-12">
    <label for="name">{{trans('label.name')}}</label>
    <input required class="form-control" name="name" id="name" value="{{$schedule->name}}">
</div>
<div class="form-group col-lg-12">
    <label for="test_mode">{{trans('label.test_mode')}}</label>
    <input required type="number" class="form-control" name="test_mode" id="test_mode" value="{{$schedule->test_mode}}">
</div>
<div class="form-group col-lg-12">
    <label for="status">{{trans('label.status')}}</label>
    <div class="checkbox">
        <label>
            <input required type="checkbox" {{$schedule->status !== 1 ?: 'checked'}} name="status" id="status" value="1">
        </label>
    </div>
</div>
<div class="form-group col-lg-12">
    <label for="from">{{trans('label.from')}}</label>
    <input required type="datetime" class="form-control" name="from" id="from" value="{{$schedule->from}}">
</div>
<div class="form-group col-lg-12">
    <label for="to">{{trans('label.to')}}</label>
    <input required type="datetime" class="form-control" name="to" id="to" value="{{$schedule->to}}">
</div>
<div class="form-group col-lg-12">
    <label for="test_interval">{{trans('label.test_interval')}}</label>
    <input required type="number" class="form-control" name="test_interval" id="test_interval" value="{{$schedule->test_interval}}">
</div>
<div class="form-group col-lg-12">
    <label for="unit">{{trans('label.unit')}}</label>
    <input required type="number" class="form-control" name="unit" id="unit" value="{{$schedule->unit}}">
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        Menu('#AppMenu', '#scheduleMenu')
    </script>
@endpush
