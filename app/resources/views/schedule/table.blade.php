<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.client_id')}}</th>
<th>{{trans('label.test_case_id')}}</th>
<th>{{trans('label.name')}}</th>
<th>{{trans('label.test_mode')}}</th>
<th>{{trans('label.status')}}</th>
<th>{{trans('label.from')}}</th>
<th>{{trans('label.to')}}</th>
<th>{{trans('label.test_interval')}}</th>
<th>{{trans('label.unit')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($schedules as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->client_id}}</td>
<td>{{$row->test_case_id}}</td>
<td>{{$row->name}}</td>
<td>{{$row->test_mode}}</td>
<td>{{$row->status}}</td>
<td>{{$row->from}}</td>
<td>{{$row->to}}</td>
<td>{{$row->test_interval}}</td>
<td>{{$row->unit}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('schedules.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('schedules.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('schedules.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$schedules->links()}}
</div>
