<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.first_name')}}</th>
<th>{{trans('label.last_name')}}</th>
<th>{{trans('label.code')}}</th>
<th>{{trans('label.email')}}</th>
<th>{{trans('label.phone_number')}}</th>
<th>{{trans('label.sex')}}</th>
<th>{{trans('label.password')}}</th>
<th>{{trans('label.birthday')}}</th>
<th>{{trans('label.address')}}</th>
<th>{{trans('label.avatar')}}</th>
<th>{{trans('label.remember_token')}}</th>
<th>{{trans('label.is_active')}}</th>
<th>{{trans('label.last_login')}}</th>
<th>{{trans('label.last_logout')}}</th>
<th>{{trans('label.slack_webhook_url')}}</th>
<th>{{trans('label.coin')}}</th>
<th>{{trans('label.locale')}}</th>
<th>{{trans('label.group_id')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->first_name}}</td>
<td>{{$row->last_name}}</td>
<td>{{$row->code}}</td>
<td>{{$row->email}}</td>
<td>{{$row->phone_number}}</td>
<td>{{$row->sex}}</td>
<td>{{$row->password}}</td>
<td>{{$row->birthday}}</td>
<td>{{$row->address}}</td>
<td>{{$row->avatar}}</td>
<td>{{$row->remember_token}}</td>
<td>{{$row->is_active}}</td>
<td>{{$row->last_login}}</td>
<td>{{$row->last_logout}}</td>
<td>{{$row->slack_webhook_url}}</td>
<td>{{$row->coin}}</td>
<td>{{$row->locale}}</td>
<td>{{$row->group_id}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('users.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('users.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('users.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$users->links()}}
</div>
