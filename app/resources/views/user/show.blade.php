@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('users.index')}}">{{trans('table.users')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.first_name')}}</th>
<td>{!! $user->first_name !!}</td>
</tr><tr><th>{{__('label.last_name')}}</th>
<td>{!! $user->last_name !!}</td>
</tr><tr><th>{{__('label.email')}}</th>
<td>{!! $user->email !!}</td>
</tr><tr><th>{{__('label.phone_number')}}</th>
<td>{!! $user->phone_number !!}</td>
</tr><tr><th>{{__('label.sex')}}</th>
<td>{!! $user->sex !!}</td>
</tr><tr><th>{{__('label.birthday')}}</th>
<td>{!! $user->birthday !!}</td>
</tr><tr><th>{{__('label.address')}}</th>
<td>{!! $user->address !!}</td>
</tr><tr><th>{{__('label.avatar')}}</th>
<td>{!! $user->avatar !!}</td>
</tr><tr><th>{{__('label.is_active')}}</th>
<td>{!! $user->is_active !!}</td>
</tr><tr><th>{{__('label.last_login')}}</th>
<td>{!! $user->last_login !!}</td>
</tr><tr><th>{{__('label.last_logout')}}</th>
<td>{!! $user->last_logout !!}</td>
</tr><tr><th>{{__('label.slack_webhook_url')}}</th>
<td>{!! $user->slack_webhook_url !!}</td>
</tr><tr><th>{{__('label.coin')}}</th>
<td>{!! $user->coin !!}</td>
</tr><tr><th>{{__('label.locale')}}</th>
<td>{!! $user->locale !!}</td>
</tr><tr><th>{{__('label.group_id')}}</th>
<td>{!! $user->group_id !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#AppMenu', '#userMenu')
</script>
@endpush
