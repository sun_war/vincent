@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('users.index')}}">{{trans('table.users')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.update')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('users.update', $user->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-12">
    <label for="first_name">{{trans('label.first_name')}}</label>
    <input required class="form-control" name="first_name" id="first_name" value="{{$user->first_name}}">
</div>
<div class="form-group col-lg-12">
    <label for="last_name">{{trans('label.last_name')}}</label>
    <input required class="form-control" name="last_name" id="last_name" value="{{$user->last_name}}">
</div>
<div class="form-group col-lg-12">
    <label for="email">{{trans('label.email')}}</label>
    <input required class="form-control" name="email" id="email" value="{{$user->email}}">
</div>
<div class="form-group col-lg-12">
    <label for="phone_number">{{trans('label.phone_number')}}</label>
    <input required class="form-control" name="phone_number" id="phone_number" value="{{$user->phone_number}}">
</div>
<div class="form-group col-lg-12">
    <label for="sex">{{trans('label.sex')}}</label>
    <div class="checkbox">
        <label>
            <input required type="checkbox" {{$user->sex !== 1 ?: 'checked'}} name="sex" id="sex" value="1">
        </label>
    </div>
</div>
<div class="form-group col-lg-12">
    <label for="birthday">{{trans('label.birthday')}}</label>
    <input required type="datetime" class="form-control" name="birthday" id="birthday" value="{{$user->birthday}}">
</div>
<div class="form-group col-lg-12">
    <label for="address">{{trans('label.address')}}</label>
    <textarea class="form-control" name="address" id="address">{{$user->address}}</textarea>
</div>
<div class="form-group col-lg-12">
    <label for="avatar">{{trans('label.avatar')}}</label>
    <textarea class="form-control" name="avatar" id="avatar">{{$user->avatar}}</textarea>
</div>
<div class="form-group col-lg-12">
    <label for="is_active">{{trans('label.is_active')}}</label>
    <div class="checkbox">
        <label>
            <input required type="checkbox" {{$user->is_active !== 1 ?: 'checked'}} name="is_active" id="is_active" value="1">
        </label>
    </div>
</div>
<div class="form-group col-lg-12">
    <label for="last_login">{{trans('label.last_login')}}</label>
    <input required type="datetime" class="form-control" name="last_login" id="last_login" value="{{$user->last_login}}">
</div>
<div class="form-group col-lg-12">
    <label for="last_logout">{{trans('label.last_logout')}}</label>
    <input required type="datetime" class="form-control" name="last_logout" id="last_logout" value="{{$user->last_logout}}">
</div>
<div class="form-group col-lg-12">
    <label for="slack_webhook_url">{{trans('label.slack_webhook_url')}}</label>
    <input required class="form-control" name="slack_webhook_url" id="slack_webhook_url" value="{{$user->slack_webhook_url}}">
</div>
<div class="form-group col-lg-12">
    <label for="coin">{{trans('label.coin')}}</label>
    <input required type="number" class="form-control" name="coin" id="coin" value="{{$user->coin}}">
</div>
<div class="form-group col-lg-12">
    <label for="locale">{{trans('label.locale')}}</label>
    <input required class="form-control" name="locale" id="locale" value="{{$user->locale}}">
</div>
<div class="form-group col-lg-12">
    <label for="group_id">{{trans('label.group_id')}}</label>
    <input required type="number" class="form-control" name="group_id" id="group_id" value="{{$user->group_id}}">
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        Menu('#AppMenu', '#userMenu')
    </script>
@endpush
